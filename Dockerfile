FROM maven:3-openjdk-11 AS BUILD

WORKDIR /app

COPY . .

RUN mvn package -P ci -ff -q

FROM adoptopenjdk/openjdk11:alpine-slim

LABEL maintainer="Segning Lambou <stephane.segning@ssegning.com>"

WORKDIR /app

COPY --from=BUILD /app/target/*.jar ./

RUN ls -la

ENTRYPOINT java -jar *.jar
