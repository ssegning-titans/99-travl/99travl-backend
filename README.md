# 99Travl Main Backend

## Scripts

```bash
mvn dependency:sources
```

If you want to skip tests, run:
````bash
mvn clean package -P ci
````

If not, then:
````bash
mvn clean package
````

## Env

```bash
KEYCLOAK_ISSUER_URI=http://localhost:8080/auth/realms/99-travl
```
```bash
KEYCLOAK_CLIENT_ID=99travl-backend
```
```bash
KEYCLOAK_CLIENT_SECRET=7c1f6d68-c18d-459e-ac84-9820cdb5e575
```

## Links
- http://jump-pilot.sourceforge.net/pdfs/jts_secrets_foss4g2007.pdf
- https://www.freecodecamp.org/news/unit-testing-services-endpoints-and-repositories-in-spring-boot-4b7d9dc2b772/
