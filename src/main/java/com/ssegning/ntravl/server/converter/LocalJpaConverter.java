package com.ssegning.ntravl.server.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Locale;

@Converter(autoApply = true)
public class LocalJpaConverter implements AttributeConverter<Locale, String> {

    @Override
    public String convertToDatabaseColumn(Locale attribute) {
        if (attribute == null) {
            return null;
        }
        return attribute.toLanguageTag();
    }

    @Override
    public Locale convertToEntityAttribute(String dbData) {
        if (dbData == null) {
            return null;
        }
        return Locale.forLanguageTag(dbData);
    }
}
