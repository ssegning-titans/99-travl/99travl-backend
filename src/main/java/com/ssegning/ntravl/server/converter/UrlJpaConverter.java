package com.ssegning.ntravl.server.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

@Converter(autoApply = true)
public class UrlJpaConverter implements AttributeConverter<URL, String> {
    @Override
    public String convertToDatabaseColumn(URL attribute) {
        if (attribute == null) {
            return null;
        }
        return attribute.toString();
    }

    @Override
    public URL convertToEntityAttribute(String dbData) {
        try {
            return URI.create(dbData).toURL();
        } catch (MalformedURLException | NullPointerException e) {
            return null;
        }
    }
}
