package com.ssegning.ntravl.server.mapper.report;

import com.ssegning.ntravl.server.domain.media.BugMediaEntity;
import com.ssegning.ntravl.server.domain.report.ReportBugEntity;
import com.ssegning.ntravl.server.mapper.core.CoreMapper;
import com.ssegning.ntravl.server.mapper.core.CycleAvoidingMappingContext;
import com.ssegning.ntravl.server.model.BugMedia;
import com.ssegning.ntravl.server.model.ReportBug;
import com.ssegning.ntravl.server.model.ReportType;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", imports = ReportType.class)
public abstract class ReportBugMapper extends CoreMapper<ReportBug, ReportBugEntity> {

    @Mappings({
            @Mapping(target = "authorId", source = "author.id"),
            @Mapping(target = "createdAt", source = "creationDate"),
            @Mapping(target = "reportType", expression = "java( ReportType.BUG )")
    })
    public abstract ReportBug entityToModel(ReportBugEntity source, @Context CycleAvoidingMappingContext context);

    @Mappings({
            @Mapping(target = "documentId", source = "document.id"),
            @Mapping(target = "bugId", source = "bug.id"),
    })
    public abstract BugMedia entityToModel(BugMediaEntity source, @Context CycleAvoidingMappingContext context);

    @Named(value = "useMe")
    public BugMedia entityToModel(BugMediaEntity source) {
        return entityToModel(source, getContext());
    }

    @Mappings({
            @Mapping(source = "documentId", target = "document.id"),
            @Mapping(source = "bugId", target = "bug.id"),
            @Mapping(ignore = true, target = "updateDate"),
    })
    public abstract BugMediaEntity toEntity(BugMedia source);

    public abstract List<BugMediaEntity> toEntity(List<BugMedia> source);

}
