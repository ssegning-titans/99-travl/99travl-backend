package com.ssegning.ntravl.server.mapper.core;

import org.mapstruct.Context;
import org.mapstruct.IterableMapping;
import org.mapstruct.Named;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

public abstract class CoreMapper<Model, Entity> {

    private static final CycleAvoidingMappingContext CONTEXT = new CycleAvoidingMappingContext();

    public abstract Model entityToModel(Entity source, @Context CycleAvoidingMappingContext context);

    public CycleAvoidingMappingContext getContext() {
        return CONTEXT;
    }

    @Named(value = "useMe")
    public Model entityToModel(Entity source) {
        return entityToModel(source, CONTEXT);
    }

    @IterableMapping(qualifiedByName = "useMe")
    public List<Model> entityToModel(List<Entity> page) {
        return page.stream().map(this::entityToModel).collect(Collectors.toList());
    }

    protected Long map(Timestamp timestamp) {
        if (timestamp == null) {
            return null;
        }

        return timestamp.getTime();
    }

    protected Timestamp map(Long milliseconds) {
        return milliseconds != null ? Timestamp.from(Instant.ofEpochMilli(milliseconds)) : null;
    }

    protected URL map(URI uri) {
        try {
            return uri.toURL();
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    protected URI map(URL url) {
        if (url == null) {
            return null;
        }

        try {
            return url.toURI();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
}
