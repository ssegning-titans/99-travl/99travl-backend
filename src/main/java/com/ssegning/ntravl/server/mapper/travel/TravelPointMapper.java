package com.ssegning.ntravl.server.mapper.travel;

import com.ssegning.ntravl.server.domain.travel.TravelPointEntity;
import com.ssegning.ntravl.server.mapper.core.AddressMapper;
import com.ssegning.ntravl.server.mapper.core.CoreMapper;
import com.ssegning.ntravl.server.mapper.core.CycleAvoidingMappingContext;
import com.ssegning.ntravl.server.model.LatLng;
import com.ssegning.ntravl.server.model.TravelPoint;
import org.locationtech.jts.geom.Point;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class TravelPointMapper extends CoreMapper<TravelPoint, TravelPointEntity> {

    @Autowired
    private AddressMapper addressMapper;

    @Mappings({
            @Mapping(source = "travelId", target = "travel.id"),
            @Mapping(source = "region", target = "address.region"),
            @Mapping(source = "formattedName", target = "address.formattedName"),
            @Mapping(source = "street", target = "address.street"),
            @Mapping(source = "houseNumber", target = "address.houseNumber"),
            @Mapping(source = "city", target = "address.city"),
            @Mapping(source = "zip", target = "address.zip"),
            @Mapping(source = "country", target = "address.country"),
            @Mapping(source = "priority", target = "priority"),
            @Mapping(source = "location", target = "address.point"),

            @Mapping(target = "id", ignore = true),
            @Mapping(target = "creationDate", ignore = true),
            @Mapping(target = "updateDate", ignore = true)
    })
    public abstract TravelPointEntity toEntity(TravelPoint message);

    @Mappings({
            @Mapping(target = "id", source = "id"),
            @Mapping(target = "creationDate", source = "creationDate"),
            @Mapping(target = "travelId", source = "travel.id"),
            @Mapping(target = "region", source = "address.region"),
            @Mapping(target = "formattedName", source = "address.formattedName"),
            @Mapping(target = "street", source = "address.street"),
            @Mapping(target = "houseNumber", source = "address.houseNumber"),
            @Mapping(target = "city", source = "address.city"),
            @Mapping(target = "zip", source = "address.zip"),
            @Mapping(target = "country", source = "address.country"),
            @Mapping(target = "priority", source = "priority"),
            @Mapping(target = "location", source = "address.point")
    })
    public abstract TravelPoint entityToModel(
            TravelPointEntity source,
            @Context CycleAvoidingMappingContext context
    );

    public LatLng map(Point point) {
        return addressMapper.pointToAddress(point);
    }

    public Point map(LatLng point) {
        return addressMapper.latLngToPoint(point);
    }
}
