package com.ssegning.ntravl.server.mapper.favorite;

import com.ssegning.ntravl.server.domain.favorite.FavoriteTravelEntity;
import com.ssegning.ntravl.server.mapper.core.CoreMapper;
import com.ssegning.ntravl.server.mapper.core.CycleAvoidingMappingContext;
import com.ssegning.ntravl.server.model.FavoriteTravel;
import com.ssegning.ntravl.server.model.FavoriteType;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", imports = FavoriteType.class)
public abstract class FavoriteTravelMapper extends CoreMapper<FavoriteTravel, FavoriteTravelEntity> {

    @Mappings({
            @Mapping(target = "travelId", source = "travel.id"),
            @Mapping(target = "ownerId", source = "owner.id"),
            @Mapping(target = "createdAt", source = "creationDate"),
            @Mapping(target = "favoriteType", expression = "java( FavoriteType.TRAVEL )")
    })
    @Override
    public abstract FavoriteTravel entityToModel(
            FavoriteTravelEntity source,
            @Context CycleAvoidingMappingContext context
    );

}
