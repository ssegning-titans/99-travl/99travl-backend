package com.ssegning.ntravl.server.mapper.core;


import com.ssegning.ntravl.server.model.LatLng;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.PrecisionModel;
import org.springframework.stereotype.Component;

@Component
public class AddressMapper {

    final GeometryFactory gf = new GeometryFactory(new PrecisionModel(), 4326);

    public GeometryFactory getGf() {
        return gf;
    }

    public LatLng pointToAddress(Point address) {
        if (address == null) {
            return null;
        }
        var latLng = new LatLng();
        latLng.setLat(address.getCoordinate().getY());
        latLng.setLng(address.getCoordinate().getX());
        return latLng;
    }

    public Point latLngToPoint(LatLng apiLatLng) {
        if (apiLatLng == null) {
            return null;
        }
        var coord = new Coordinate(apiLatLng.getLng(), apiLatLng.getLat());
        return gf.createPoint(coord);
    }

    public Point coordsToPoint(Double lat, Double lng) {
        var coord = new Coordinate(lng, lat);
        return gf.createPoint(coord);
    }

}
