package com.ssegning.ntravl.server.mapper.account;

import com.ssegning.ntravl.server.domain.accounts.AccountAddressEntity;
import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import com.ssegning.ntravl.server.mapper.core.CoreMapper;
import com.ssegning.ntravl.server.mapper.core.CycleAvoidingMappingContext;
import com.ssegning.ntravl.server.model.Account;
import com.ssegning.ntravl.server.model.AccountAddress;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class AccountMapper extends CoreMapper<Account, AccountEntity> {

    @Autowired
    private AccountAddressMapper addressMapper;

    @Mappings({
            @Mapping(target = "id", source = "id"),
            @Mapping(target = "createdAt", source = "creationDate"),
            @Mapping(target = "updatedAt", source = "updateDate"),
            @Mapping(target = "status", source = "status"),
            @Mapping(target = "firstName", source = "name.firstName"),
            @Mapping(target = "lastName", source = "name.lastName"),
            @Mapping(target = "email", source = "contact.email"),
            @Mapping(target = "emailVerified", source = "contact.emailVerified"),
            @Mapping(target = "phoneNumber", source = "contact.phoneNumber"),
            @Mapping(target = "phoneNumberVerified", source = "contact.phoneNumberVerified"),
            @Mapping(target = "gender", source = "metaData.gender"),
            @Mapping(target = "locale", source = "metaData.locale"),
            @Mapping(target = "bio", source = "metaData.bio"),
            @Mapping(target = "avatarUrl", source = "metaData.avatarUrl"),
            @Mapping(target = "accountType", source = "metaData.accountType")
    })
    public abstract Account entityToModel(AccountEntity source, @Context CycleAvoidingMappingContext context);

    public AccountAddress map(AccountAddressEntity address) {
        return addressMapper.entityToModel(address);
    }

}
