package com.ssegning.ntravl.server.mapper.rating;

import com.ssegning.ntravl.server.domain.rating.RatingTravelEntity;
import com.ssegning.ntravl.server.mapper.core.CoreMapper;
import com.ssegning.ntravl.server.mapper.core.CycleAvoidingMappingContext;
import com.ssegning.ntravl.server.model.RatingTravel;
import com.ssegning.ntravl.server.model.RatingType;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", imports = RatingType.class)
public abstract class RatingTravelMapper extends CoreMapper<RatingTravel, RatingTravelEntity> {

    @Mappings({
            // Reportee
            @Mapping(target = "travelId", source = "travel.id"),
            // Reporter
            @Mapping(target = "authorId", source = "author.id"),
            @Mapping(target = "createdAt", source = "creationDate"),
            @Mapping(target = "type", expression = "java( RatingType.TRAVEL )")
    })
    public abstract RatingTravel entityToModel(RatingTravelEntity source, @Context CycleAvoidingMappingContext context);

    @Mappings({
            // Ratee
            @Mapping(target = "travel.id", source = "travelId"),
            // Rater
            @Mapping(target = "author.id", source = "authorId"),
            @Mapping(target = "creationDate", source = "createdAt"),
    })
    public abstract RatingTravelEntity toEntity(RatingTravel rating);

}
