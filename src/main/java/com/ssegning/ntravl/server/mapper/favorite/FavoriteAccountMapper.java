package com.ssegning.ntravl.server.mapper.favorite;

import com.ssegning.ntravl.server.domain.favorite.FavoriteAccountEntity;
import com.ssegning.ntravl.server.mapper.core.CoreMapper;
import com.ssegning.ntravl.server.mapper.core.CycleAvoidingMappingContext;
import com.ssegning.ntravl.server.model.FavoriteAccount;
import com.ssegning.ntravl.server.model.FavoriteType;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", imports = FavoriteType.class)
public abstract class FavoriteAccountMapper extends CoreMapper<FavoriteAccount, FavoriteAccountEntity> {

    @Mappings({
            @Mapping(target = "accountId", source = "person.id"),
            @Mapping(target = "ownerId", source = "owner.id"),
            @Mapping(target = "createdAt", source = "creationDate"),
            @Mapping(target = "favoriteType", expression = "java( FavoriteType.ACCOUNT )")
    })
    @Override
    public abstract FavoriteAccount entityToModel(
            FavoriteAccountEntity source,
            @Context CycleAvoidingMappingContext context
    );

}
