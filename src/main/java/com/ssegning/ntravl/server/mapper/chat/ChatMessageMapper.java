package com.ssegning.ntravl.server.mapper.chat;

import com.ssegning.ntravl.server.domain.chat.*;
import com.ssegning.ntravl.server.mapper.core.CoreMapper;
import com.ssegning.ntravl.server.mapper.core.CycleAvoidingMappingContext;
import com.ssegning.ntravl.server.model.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ChatMessageMapper extends CoreMapper<BaseChatMessage, BaseChatMessageEntity> {

    private final ContactChatMessageMapper contactChatMessageMapper;
    private final MediaChatMessageMapper mediaChatMessageMapper;
    private final PropositionChatMessageMapper propositionChatMessageMapper;
    private final TextChatMessageMapper textChatMessageMapper;

    @Override
    public BaseChatMessage entityToModel(BaseChatMessageEntity source, CycleAvoidingMappingContext context) {
        if (source instanceof ContactChatMessageEntity) {
            return contactChatMessageMapper.entityToModel((ContactChatMessageEntity) source, context);
        } else if (source instanceof MediaChatMessageEntity) {
            return mediaChatMessageMapper.entityToModel((MediaChatMessageEntity) source);
        } else if (source instanceof PropositionChatMessageEntity) {
            return propositionChatMessageMapper.entityToModel((PropositionChatMessageEntity) source);
        } else if (source instanceof TextChatMessageEntity) {
            return textChatMessageMapper.entityToModel((TextChatMessageEntity) source);
        } else {
            throw new RuntimeException("This type is not supported");
        }
    }

    public BaseChatMessageEntity toEntity(BaseChatMessage baseChatMessage) {
        if (baseChatMessage instanceof ContactChatMessage) {
            return contactChatMessageMapper.toEntity((ContactChatMessage) baseChatMessage);
        } else if (baseChatMessage instanceof MediaChatMessage) {
            return mediaChatMessageMapper.toEntity((MediaChatMessage) baseChatMessage);
        } else if (baseChatMessage instanceof PropositionChatMessage) {
            return propositionChatMessageMapper.toEntity((PropositionChatMessage) baseChatMessage);
        } else if (baseChatMessage instanceof TextChatMessage) {
            return textChatMessageMapper.toEntity((TextChatMessage) baseChatMessage);
        } else {
            throw new RuntimeException("This type is not supported");
        }
    }

}
