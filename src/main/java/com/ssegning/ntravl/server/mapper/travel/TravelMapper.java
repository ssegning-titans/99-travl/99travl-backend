package com.ssegning.ntravl.server.mapper.travel;

import com.ssegning.ntravl.server.domain.travel.TravelEntity;
import com.ssegning.ntravl.server.domain.travel.TravelPointEntity;
import com.ssegning.ntravl.server.mapper.core.CoreMapper;
import com.ssegning.ntravl.server.mapper.core.CycleAvoidingMappingContext;
import com.ssegning.ntravl.server.model.Travel;
import com.ssegning.ntravl.server.model.TravelPoint;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class TravelMapper extends CoreMapper<Travel, TravelEntity> {

    @Autowired
    private TravelPointMapper travelPointMapper;

    @Mappings({
            @Mapping(source = "providerAccountId", target = "provider.id"),

            @Mapping(source = "createdAt", target = "creationDate"),
            @Mapping(source = "updatedAt", target = "updateDate"),
    })
    public abstract TravelEntity toEntity(Travel message);

    @Mappings({
            @Mapping(target = "providerAccountId", source = "provider.id"),

            @Mapping(target = "createdAt", source = "creationDate"),
            @Mapping(target = "updatedAt", source = "updateDate"),
    })
    @Override
    public abstract Travel entityToModel(
            TravelEntity source,
            @Context CycleAvoidingMappingContext context
    );

    public TravelPointEntity map(TravelPoint point) {
        return travelPointMapper.toEntity(point);
    }

    public TravelPoint map(TravelPointEntity point) {
        return travelPointMapper.entityToModel(point);
    }
}
