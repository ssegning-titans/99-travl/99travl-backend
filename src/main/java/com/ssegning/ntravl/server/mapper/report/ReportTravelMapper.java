package com.ssegning.ntravl.server.mapper.report;

import com.ssegning.ntravl.server.domain.report.ReportTravelEntity;
import com.ssegning.ntravl.server.mapper.core.CoreMapper;
import com.ssegning.ntravl.server.mapper.core.CycleAvoidingMappingContext;
import com.ssegning.ntravl.server.model.ReportTravel;
import com.ssegning.ntravl.server.model.ReportType;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", imports = ReportType.class)
public abstract class ReportTravelMapper extends CoreMapper<ReportTravel, ReportTravelEntity> {

    @Mappings({
            // Reportee
            @Mapping(target = "travelId", source = "travel.id"),
            // Reporter
            @Mapping(target = "authorId", source = "author.id"),
            @Mapping(target = "createdAt", source = "creationDate"),
            @Mapping(target = "reportType", expression = "java( ReportType.TRAVEL )")
    })
    public abstract ReportTravel entityToModel(ReportTravelEntity source, @Context CycleAvoidingMappingContext context);

}
