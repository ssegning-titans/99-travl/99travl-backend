package com.ssegning.ntravl.server.mapper.chat;

import com.ssegning.ntravl.server.domain.chat.PropositionChatMessageEntity;
import com.ssegning.ntravl.server.mapper.core.CoreMapper;
import com.ssegning.ntravl.server.mapper.core.CycleAvoidingMappingContext;
import com.ssegning.ntravl.server.model.ChatMessageType;
import com.ssegning.ntravl.server.model.PropositionChatMessage;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", imports = {ChatMessageType.class})
public abstract class PropositionChatMessageMapper extends CoreMapper<PropositionChatMessage, PropositionChatMessageEntity> {

    @Mappings({
            @Mapping(source = "authorId", target = "author.id"),
            @Mapping(source = "chatId", target = "chat.id"),

            @Mapping(target = "id", ignore = true),
            @Mapping(target = "creationDate", ignore = true),
            @Mapping(target = "updateDate", ignore = true)
    })
    public abstract PropositionChatMessageEntity toEntity(PropositionChatMessage message);

    @Mappings({
            @Mapping(target = "authorId", source = "author.id"),
            @Mapping(target = "chatId", source = "chat.id"),
            @Mapping(target = "createdAt", source = "creationDate"),
            @Mapping(target = "messageType", expression = "java( ChatMessageType.PROPOSITION )")
    })
    @Override
    public abstract PropositionChatMessage entityToModel(
            PropositionChatMessageEntity source,
            @Context CycleAvoidingMappingContext context
    );

}
