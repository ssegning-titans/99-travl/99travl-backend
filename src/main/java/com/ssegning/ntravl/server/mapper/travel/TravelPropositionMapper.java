package com.ssegning.ntravl.server.mapper.travel;

import com.ssegning.ntravl.server.domain.travel.TravelPropositionEntity;
import com.ssegning.ntravl.server.mapper.core.CoreMapper;
import com.ssegning.ntravl.server.mapper.core.CycleAvoidingMappingContext;
import com.ssegning.ntravl.server.model.TravelProposition;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public abstract class TravelPropositionMapper extends CoreMapper<TravelProposition, TravelPropositionEntity> {

    @Mappings({
            @Mapping(source = "startingPointId", target = "startingPoint.id"),
            @Mapping(source = "endingPointId", target = "endingPoint.id"),

            @Mapping(ignore = true, target = "creationDate"),
            @Mapping(ignore = true, target = "updateDate"),
    })
    public abstract TravelPropositionEntity toEntity(TravelProposition message);

    @Mappings({
            @Mapping(target = "startingPointId", source = "startingPoint.id"),
            @Mapping(target = "endingPointId", source = "endingPoint.id"),
    })
    @Override
    public abstract TravelProposition entityToModel(
            TravelPropositionEntity source,
            @Context CycleAvoidingMappingContext context
    );


}
