package com.ssegning.ntravl.server.mapper.friend;

import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import com.ssegning.ntravl.server.domain.friend.BaseFriendEntity;
import com.ssegning.ntravl.server.domain.friend.OfflineFriendShipEntity;
import com.ssegning.ntravl.server.domain.friend.OnlineFriendShipEntity;
import com.ssegning.ntravl.server.mapper.core.CoreMapper;
import com.ssegning.ntravl.server.mapper.core.CycleAvoidingMappingContext;
import com.ssegning.ntravl.server.model.*;
import org.mapstruct.*;

import java.util.Set;
import java.util.UUID;

@Mapper(componentModel = "spring", imports = {FriendType.class, FriendProcessType.class})
public abstract class FriendMapper extends CoreMapper<BaseFriend, BaseFriendEntity> {

    @Override
    public BaseFriend entityToModel(BaseFriendEntity source, @Context CycleAvoidingMappingContext context) {
        BaseFriend target = context.getMappedInstance(source, BaseFriend.class);
        if (target != null) {
            return target;
        }

        if (source == null) {
            return null;
        }

        if (source instanceof OfflineFriendShipEntity) {
            return entityToModel((OfflineFriendShipEntity) source, context);
        } else if (source instanceof OnlineFriendShipEntity) {
            return entityToModel((OnlineFriendShipEntity) source, context);
        } else {
            return null;
        }
    }

    @Mappings({
            @Mapping(target = "processType", expression = "java( FriendProcessType.OFFLINE )")
    })
    public abstract OfflineFriend entityToModel(OfflineFriendShipEntity source, @Context CycleAvoidingMappingContext context);

    @Named(value = "useMe")
    public OfflineFriend entityToModel(OfflineFriendShipEntity source) {
        return entityToModel(source, getContext());
    }

    @Mappings({
            @Mapping(target = "accountId", expression = "java( map( source ) )"),
            @Mapping(target = "authorId", source = "author.id"),
            @Mapping(target = "processType", expression = "java( FriendProcessType.ONLINE )")
    })
    public abstract OnlineFriend entityToModel(OnlineFriendShipEntity source, @Context CycleAvoidingMappingContext context);

    @Named(value = "useMe")
    public OnlineFriend entityToModel(OnlineFriendShipEntity source) {
        return entityToModel(source, getContext());
    }

    protected UUID map(OnlineFriendShipEntity source) {
        Set<AccountEntity> members = source.getMembers();
        if (members == null || members.size() == 0) {
            return null;
        }
        return members.stream().filter(a -> !a.getId().equals(source.getAuthor().getId())).findFirst().map(AccountEntity::getId).orElse(null);
    }
}
