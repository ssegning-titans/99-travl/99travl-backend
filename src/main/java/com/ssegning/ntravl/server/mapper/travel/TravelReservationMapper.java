package com.ssegning.ntravl.server.mapper.travel;

import com.ssegning.ntravl.server.domain.travel.TravelReservationEntity;
import com.ssegning.ntravl.server.mapper.core.CoreMapper;
import com.ssegning.ntravl.server.mapper.core.CycleAvoidingMappingContext;
import com.ssegning.ntravl.server.model.TravelReservation;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public abstract class TravelReservationMapper extends CoreMapper<TravelReservation, TravelReservationEntity> {

    @Mappings({
            @Mapping(source = "travelId", target = "travel.id"),
            @Mapping(source = "startingReservationPoint", target = "startingPoint.id"),
            @Mapping(source = "endingReservationPoint", target = "endingPoint.id"),
            @Mapping(source = "reserverAccountId", target = "author.id"),
    })
    public abstract TravelReservationEntity toEntity(TravelReservation message);

    @Mappings({
            @Mapping(target = "travelId", source = "travel.id"),
            @Mapping(target = "startingReservationPoint", source = "startingPoint.id"),
            @Mapping(target = "endingReservationPoint", source = "endingPoint.id"),
            @Mapping(target = "reserverAccountId", source = "author.id"),
    })
    @Override
    public abstract TravelReservation entityToModel(
            TravelReservationEntity source,
            @Context CycleAvoidingMappingContext context
    );

}
