package com.ssegning.ntravl.server.mapper.report;

import com.ssegning.ntravl.server.domain.report.ReportAccountEntity;
import com.ssegning.ntravl.server.mapper.core.CoreMapper;
import com.ssegning.ntravl.server.mapper.core.CycleAvoidingMappingContext;
import com.ssegning.ntravl.server.model.ReportAccount;
import com.ssegning.ntravl.server.model.ReportType;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", imports = ReportType.class)
public abstract class ReportAccountMapper extends CoreMapper<ReportAccount, ReportAccountEntity> {

    @Mappings({
            // Reportee
            @Mapping(target = "accountId", source = "account.id"),
            // Reporter
            @Mapping(target = "authorId", source = "author.id"),
            @Mapping(target = "createdAt", source = "creationDate"),
            @Mapping(target = "reportType", expression = "java( ReportType.ACCOUNT )")
    })
    public abstract ReportAccount entityToModel(ReportAccountEntity source, @Context CycleAvoidingMappingContext context);

}
