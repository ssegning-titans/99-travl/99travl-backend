package com.ssegning.ntravl.server.mapper.rating;

import com.ssegning.ntravl.server.domain.rating.RatingAccountEntity;
import com.ssegning.ntravl.server.mapper.core.CoreMapper;
import com.ssegning.ntravl.server.mapper.core.CycleAvoidingMappingContext;
import com.ssegning.ntravl.server.model.RatingAccount;
import com.ssegning.ntravl.server.model.RatingType;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", imports = RatingType.class)
public abstract class RatingAccountMapper extends CoreMapper<RatingAccount, RatingAccountEntity> {

    @Mappings({
            // Reportee
            @Mapping(target = "personId", source = "person.id"),
            // Reporter
            @Mapping(target = "authorId", source = "author.id"),
            @Mapping(target = "createdAt", source = "creationDate"),
            @Mapping(target = "type", expression = "java( RatingType.ACCOUNT )")
    })
    public abstract RatingAccount entityToModel(RatingAccountEntity source, @Context CycleAvoidingMappingContext context);

    @Mappings({
            // Ratee
            @Mapping(target = "person.id", source = "personId"),
            // Rater
            @Mapping(target = "author.id", source = "authorId"),
            @Mapping(target = "creationDate", source = "createdAt"),
    })
    public abstract RatingAccountEntity toEntity(RatingAccount rating);

}
