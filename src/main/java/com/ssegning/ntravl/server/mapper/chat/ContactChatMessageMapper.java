package com.ssegning.ntravl.server.mapper.chat;

import com.ssegning.ntravl.server.domain.chat.ChatContactEntity;
import com.ssegning.ntravl.server.domain.chat.ContactChatMessageEntity;
import com.ssegning.ntravl.server.mapper.core.CoreMapper;
import com.ssegning.ntravl.server.mapper.core.CycleAvoidingMappingContext;
import com.ssegning.ntravl.server.model.ChatContact;
import com.ssegning.ntravl.server.model.ChatMessageType;
import com.ssegning.ntravl.server.model.ContactChatMessage;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", imports = {ChatMessageType.class})
public abstract class ContactChatMessageMapper extends CoreMapper<ContactChatMessage, ContactChatMessageEntity> {

    @Mappings({
            @Mapping(source = "authorId", target = "author.id"),
            @Mapping(source = "chatId", target = "chat.id"),

            @Mapping(target = "id", ignore = true),
            @Mapping(target = "creationDate", ignore = true),
            @Mapping(target = "updateDate", ignore = true)
    })
    public abstract ContactChatMessageEntity toEntity(ContactChatMessage message);

    @Mappings({
            @Mapping(source = "accountId", target = "account.id"),

            @Mapping(target = "id", ignore = true),
            @Mapping(target = "creationDate", ignore = true),
            @Mapping(target = "updateDate", ignore = true),
            @Mapping(target = "contactChatMessage", ignore = true)
    })
    public abstract ChatContactEntity toEntity(ChatContact message);

    @Mappings({
            @Mapping(target = "authorId", source = "author.id"),
            @Mapping(target = "chatId", source = "chat.id"),
            @Mapping(target = "createdAt", source = "creationDate"),
            @Mapping(target = "messageType", expression = "java( ChatMessageType.CONTACT )")

    })
    @Override
    public abstract ContactChatMessage entityToModel(
            ContactChatMessageEntity source,
            @Context CycleAvoidingMappingContext context
    );

    @Mappings({
            @Mapping(target = "accountId", source = "account.id"),
            @Mapping(target = "isAppUser", source = "isAppUser")
    })
    public abstract ChatContact entityToModel(ChatContactEntity source);

}
