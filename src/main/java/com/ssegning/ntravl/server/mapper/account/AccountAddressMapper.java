package com.ssegning.ntravl.server.mapper.account;

import com.ssegning.ntravl.server.domain.accounts.AccountAddressEntity;
import com.ssegning.ntravl.server.mapper.core.AddressMapper;
import com.ssegning.ntravl.server.mapper.core.CoreMapper;
import com.ssegning.ntravl.server.mapper.core.CycleAvoidingMappingContext;
import com.ssegning.ntravl.server.model.AccountAddress;
import com.ssegning.ntravl.server.model.LatLng;
import org.locationtech.jts.geom.Point;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class AccountAddressMapper extends CoreMapper<AccountAddress, AccountAddressEntity> {

    @Autowired
    private AddressMapper addressMapper;

    @Mappings({
            @Mapping(target = "id", source = "id"),
            @Mapping(target = "creationDate", source = "creationDate"),
            @Mapping(target = "accountId", source = "account.id"),
            @Mapping(target = "region", source = "address.region"),
            @Mapping(target = "formattedName", source = "address.formattedName"),
            @Mapping(target = "street", source = "address.street"),
            @Mapping(target = "houseNumber", source = "address.houseNumber"),
            @Mapping(target = "city", source = "address.city"),
            @Mapping(target = "zip", source = "address.zip"),
            @Mapping(target = "country", source = "address.country"),
            @Mapping(target = "location", source = "address.point")
    })
    public abstract AccountAddress entityToModel(
            AccountAddressEntity source,
            @Context CycleAvoidingMappingContext context
    );

    @Mappings({
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "creationDate", target = "creationDate"),
            @Mapping(ignore = true, target = "updateDate"),
            @Mapping(source = "accountId", target = "account.id"),
            @Mapping(source = "region", target = "address.region"),
            @Mapping(source = "formattedName", target = "address.formattedName"),
            @Mapping(source = "street", target = "address.street"),
            @Mapping(source = "houseNumber", target = "address.houseNumber"),
            @Mapping(source = "city", target = "address.city"),
            @Mapping(source = "zip", target = "address.zip"),
            @Mapping(source = "country", target = "address.country"),
            @Mapping(source = "location", target = "address.point")
    })
    public abstract AccountAddressEntity toEntity(AccountAddress source);

    public LatLng map(Point point) {
        return addressMapper.pointToAddress(point);
    }

    public Point map(LatLng latLng) {
        return addressMapper.latLngToPoint(latLng);
    }

}
