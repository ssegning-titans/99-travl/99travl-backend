package com.ssegning.ntravl.server.mapper.rating;

import com.ssegning.ntravl.server.domain.rating.BaseRatingEntity;
import com.ssegning.ntravl.server.domain.rating.RatingAccountEntity;
import com.ssegning.ntravl.server.domain.rating.RatingTravelEntity;
import com.ssegning.ntravl.server.mapper.core.CoreMapper;
import com.ssegning.ntravl.server.mapper.core.CycleAvoidingMappingContext;
import com.ssegning.ntravl.server.model.BaseRating;
import com.ssegning.ntravl.server.model.RatingAccount;
import com.ssegning.ntravl.server.model.RatingTravel;
import lombok.AllArgsConstructor;
import org.mapstruct.Context;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class RatingMapper extends CoreMapper<BaseRating, BaseRatingEntity> {

    private final RatingTravelMapper ratingTravelMapper;
    private final RatingAccountMapper ratingAccountMapper;

    public BaseRating entityToModel(BaseRatingEntity source, @Context CycleAvoidingMappingContext context) {
        if (source instanceof RatingTravelEntity) {
            return ratingTravelMapper.entityToModel((RatingTravelEntity) source, context);
        }
        if (source instanceof RatingAccountEntity) {
            return ratingAccountMapper.entityToModel((RatingAccountEntity) source, context);
        }
        return null;
    }

    public BaseRatingEntity toEntity(BaseRating rating) {
        if (rating instanceof RatingTravel) {
            return ratingTravelMapper.toEntity((RatingTravel) rating);
        }
        if (rating instanceof RatingAccount) {
            return ratingAccountMapper.toEntity((RatingAccount) rating);
        }
        return null;
    }

}
