package com.ssegning.ntravl.server.mapper.chat;

import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import com.ssegning.ntravl.server.domain.chat.ChatEntity;
import com.ssegning.ntravl.server.domain.core.AbstractEntity;
import com.ssegning.ntravl.server.mapper.core.CoreMapper;
import com.ssegning.ntravl.server.mapper.core.CycleAvoidingMappingContext;
import com.ssegning.ntravl.server.model.Chat;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class ChatMapper extends CoreMapper<Chat, ChatEntity> {

    @Mappings({
            @Mapping(source = "createdAt", target = "creationDate"),
            @Mapping(source = "travelId", target = "travel.id"),
            @Mapping(target = "updateDate", ignore = true)
    })
    public abstract ChatEntity toEntity(Chat message);

    protected Set<AccountEntity> map(List<UUID> members) {
        return members
                .stream()
                .map(item -> {
                    var account = new AccountEntity();
                    account.setId(item);
                    return account;
                })
                .collect(Collectors.toSet());
    }

    @Mappings({
            @Mapping(target = "createdAt", source = "creationDate"),
            @Mapping(target = "travelId", source = "travel.id")
    })
    @Override
    public abstract Chat entityToModel(
            ChatEntity source,
            @Context CycleAvoidingMappingContext context
    );


    public List<UUID> map(Set<AccountEntity> members) {
        return members.stream().map(AbstractEntity::getId).collect(Collectors.toList());
    }

}
