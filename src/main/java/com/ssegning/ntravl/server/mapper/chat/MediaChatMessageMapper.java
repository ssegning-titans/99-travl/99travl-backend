package com.ssegning.ntravl.server.mapper.chat;

import com.ssegning.ntravl.server.domain.chat.MediaChatMessageEntity;
import com.ssegning.ntravl.server.domain.media.MediaEntity;
import com.ssegning.ntravl.server.mapper.core.CoreMapper;
import com.ssegning.ntravl.server.mapper.core.CycleAvoidingMappingContext;
import com.ssegning.ntravl.server.model.ChatMedia;
import com.ssegning.ntravl.server.model.ChatMessageType;
import com.ssegning.ntravl.server.model.MediaChatMessage;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", imports = {ChatMessageType.class})
public abstract class MediaChatMessageMapper extends CoreMapper<MediaChatMessage, MediaChatMessageEntity> {

    @Mappings({
            @Mapping(source = "authorId", target = "author.id"),
            @Mapping(source = "chatId", target = "chat.id"),

            @Mapping(target = "id", ignore = true),
            @Mapping(target = "creationDate", ignore = true),
            @Mapping(target = "updateDate", ignore = true)
    })
    public abstract MediaChatMessageEntity toEntity(MediaChatMessage message);

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "creationDate", ignore = true),
            @Mapping(target = "updateDate", ignore = true),
            @Mapping(target = "mediaChatMessage", ignore = true),
            @Mapping(target = "document.id", source = "documentId"),
            @Mapping(target = "document.filePath", source = "path")
    })
    public abstract MediaEntity toEntity(ChatMedia message);

    @Mappings({
            @Mapping(target = "authorId", source = "author.id"),
            @Mapping(target = "chatId", source = "chat.id"),
            @Mapping(target = "createdAt", source = "creationDate"),
            @Mapping(target = "messageType", expression = "java( ChatMessageType.MEDIA )")
    })
    @Override
    public abstract MediaChatMessage entityToModel(
            MediaChatMessageEntity source,
            @Context CycleAvoidingMappingContext context
    );

    @Mappings({
            @Mapping(source = "document.id", target = "documentId"),
            @Mapping(source = "document.filePath", target = "path")
    })
    public abstract ChatMedia entityToModel(MediaEntity source);

}
