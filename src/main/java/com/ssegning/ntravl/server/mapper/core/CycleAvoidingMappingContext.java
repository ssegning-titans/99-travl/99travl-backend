package com.ssegning.ntravl.server.mapper.core;

import org.mapstruct.AfterMapping;
import org.mapstruct.BeforeMapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.TargetType;

import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Map;

public class CycleAvoidingMappingContext {

    private final Map<Object, Object> knownInstances = Collections.synchronizedMap(new IdentityHashMap<>());

    @AfterMapping
    public <S, T> void storeMappedInstance(S source, @MappingTarget T target) {
        knownInstances.put(source, target);
    }

    @BeforeMapping
    public <S, T> T getMappedInstance(S source, @TargetType Class<T> targetType) {
        var obj = knownInstances.get(source);
        return targetType.cast(obj);
    }

}
