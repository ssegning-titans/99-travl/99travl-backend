package com.ssegning.ntravl.server.domain.media;

import com.ssegning.ntravl.server.domain.core.AbstractEntity;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "NTV_DOCUMENT", indexes = @Index(name = "id_doc_file_path", columnList = "FILE_PATH"))
public class DocumentEntity extends AbstractEntity {

    @Lob
    @Column(columnDefinition = "text", nullable = false, updatable = false)
    private byte[] bytes;

    @Column(nullable = false)
    private String contentType;

    @Column(nullable = false, unique = true, name = "FILE_PATH")
    private String filePath;

    @CollectionTable(name = "NTV_DOCUMENT_META_DATA")
    @ElementCollection
    private Map<String, String> metaData = new HashMap<>();

    @CollectionTable(name = "NTV_DOCUMENT_HEADERS")
    @ElementCollection
    private Map<String, String> headers = new HashMap<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        DocumentEntity that = (DocumentEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
