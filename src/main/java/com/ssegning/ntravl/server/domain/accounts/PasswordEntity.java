package com.ssegning.ntravl.server.domain.accounts;

import com.ssegning.ntravl.server.domain.core.AbstractEntity;
import com.ssegning.ntravl.server.types.PasswordStatus;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "NTV_PASSWORD_CRED")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PasswordEntity extends AbstractEntity {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false)
    private AccountEntity account;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private PasswordStatus status = PasswordStatus.ACTIVE;

    @Column(name = "HASHED_VALUE")
    private String hashedValue;
}
