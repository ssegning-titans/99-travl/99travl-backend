package com.ssegning.ntravl.server.domain.travel;

import com.ssegning.ntravl.server.domain.core.AbstractEntity;
import com.ssegning.ntravl.server.domain.embedded.Address;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@ToString(exclude = "travel")
@Entity
@Table(
        name = "NTV_TRAVEL_POINT",
        indexes = {
                @Index(name = "id_travel_point_by_coords", columnList = "TRAVEL_ID"),
                @Index(name = "id_travel_passage_time", columnList = "PASSAGE_TIME"),
        },
        uniqueConstraints = @UniqueConstraint(name = "uc_travel_priority", columnNames = {"TRAVEL_ID", "PRIORITY"})
)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TravelPointEntity extends AbstractEntity {

    @Embedded
    private Address address;

    @ManyToOne
    @JoinColumn(name = "TRAVEL_ID", nullable = false)
    private TravelEntity travel;

    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "PRIORITY", nullable = false, updatable = false)
    private Integer priority;

    @Column(name = "PASSAGE_TIME", nullable = false, updatable = false)
    private Timestamp passageTime;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        TravelPointEntity that = (TravelPointEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
