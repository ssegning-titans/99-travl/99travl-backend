package com.ssegning.ntravl.server.domain.travel;

import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import com.ssegning.ntravl.server.domain.core.AbstractEntity;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "NTV_TRAVEL_RESERVATION")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class TravelReservationEntity extends AbstractEntity {

    @ManyToOne
    @JoinColumn(name = "AUTHOR_ID")
    private AccountEntity author;

    @ManyToOne
    @JoinColumn(name = "TRAVEL_ID")
    private TravelEntity travel;

    @Column(columnDefinition = "varchar(512)")
    @Length(max = 512)
    private String message;

    @ManyToOne
    @JoinColumn(nullable = false, name = "STARTING_POINT")
    private TravelPointEntity startingPoint;

    @ManyToOne
    @JoinColumn(nullable = false, name = "ENDING_POINT")
    private TravelPointEntity endingPoint;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        TravelReservationEntity that = (TravelReservationEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
