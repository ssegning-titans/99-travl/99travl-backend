package com.ssegning.ntravl.server.domain.report;

import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "NTV_REPORT_ACCOUNT")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ReportAccountEntity extends BaseReportEntity {

    @ManyToOne
    private AccountEntity account;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ReportAccountEntity that = (ReportAccountEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
