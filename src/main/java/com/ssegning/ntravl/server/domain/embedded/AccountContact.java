package com.ssegning.ntravl.server.domain.embedded;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class AccountContact implements Serializable {

    @Column(unique = true)
    private String email;

    private Boolean emailVerified;

    @Column(unique = true)
    private String phoneNumber;

    private Boolean phoneNumberVerified;
}
