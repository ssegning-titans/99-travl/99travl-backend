package com.ssegning.ntravl.server.domain.report;

import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import com.ssegning.ntravl.server.domain.core.AbstractEntity;
import com.ssegning.ntravl.server.model.ReportStatus;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "NTV_REPORT")
@Inheritance(strategy = InheritanceType.JOINED)
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public abstract class BaseReportEntity extends AbstractEntity {

    @Enumerated(EnumType.STRING)
    private ReportStatus status = ReportStatus.PENDING;

    @ManyToOne
    private AccountEntity author;

    private String description;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        BaseReportEntity that = (BaseReportEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
