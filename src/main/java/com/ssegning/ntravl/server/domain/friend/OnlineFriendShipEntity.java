package com.ssegning.ntravl.server.domain.friend;

import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "NTV_ONLINE_FRIENDSHIP")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class OnlineFriendShipEntity extends BaseFriendEntity {

    @NotNull
    @ManyToOne
    @JoinColumn(nullable = false)
    private AccountEntity author;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        OnlineFriendShipEntity that = (OnlineFriendShipEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
