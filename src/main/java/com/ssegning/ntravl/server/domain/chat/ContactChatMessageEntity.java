package com.ssegning.ntravl.server.domain.chat;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "NTV_CONTACT_CHAT_MESSAGE")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ContactChatMessageEntity extends BaseChatMessageEntity {

    @Size(min = 1, max = 6)
    @OneToMany(
            cascade = {CascadeType.REMOVE, CascadeType.PERSIST},
            mappedBy = "contactChatMessage",
            orphanRemoval = true
    )
    @ToString.Exclude
    private Set<ChatContactEntity> contacts;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ContactChatMessageEntity that = (ContactChatMessageEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
