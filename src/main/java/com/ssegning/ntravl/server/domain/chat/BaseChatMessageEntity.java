package com.ssegning.ntravl.server.domain.chat;

import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import com.ssegning.ntravl.server.domain.core.AbstractEntity;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "NTV_CHAT_MESSAGE")
@Inheritance(strategy = InheritanceType.JOINED)
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public abstract class BaseChatMessageEntity extends AbstractEntity {

    @ManyToOne
    @JoinColumn(name = "AUTHOR_ID")
    private AccountEntity author;

    @ManyToOne
    @JoinColumn(name = "CHAT_ID")
    private ChatEntity chat;

    @Column(name = "IS_SEEN")
    private Boolean seen;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        BaseChatMessageEntity that = (BaseChatMessageEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
