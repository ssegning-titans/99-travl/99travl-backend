package com.ssegning.ntravl.server.domain.accounts;

import com.ssegning.ntravl.server.domain.core.AbstractEntity;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "NTV_PHONE_NUMBER_VALIDATION")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PhoneNumberValidationEntity extends AbstractEntity {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false)
    private AccountEntity account;

    private boolean used = false;

    @lombok.NonNull
    @Column(nullable = false)
    private String code;

    @lombok.NonNull
    @Column(nullable = false)
    private String phoneNumber;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        PhoneNumberValidationEntity that = (PhoneNumberValidationEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
