package com.ssegning.ntravl.server.domain.chat;

import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "NTV_TEXT_CHAT_MESSAGE")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class TextChatMessageEntity extends BaseChatMessageEntity {

    @Length(max = 512)
    @Column(columnDefinition = "varchar(512)")
    private String message;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        TextChatMessageEntity that = (TextChatMessageEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
