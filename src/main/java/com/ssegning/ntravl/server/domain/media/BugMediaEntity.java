package com.ssegning.ntravl.server.domain.media;

import com.ssegning.ntravl.server.domain.core.AbstractEntity;
import com.ssegning.ntravl.server.domain.report.ReportBugEntity;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "NTV_BUG_MEDIA")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BugMediaEntity extends AbstractEntity {

    @OneToOne(
            cascade = {CascadeType.REMOVE, CascadeType.PERSIST},
            fetch = FetchType.LAZY
    )
    @ToString.Exclude
    private DocumentEntity document;

    @ManyToOne
    @JoinColumn(name = "BUG_MEDIA_CHAT_MESSAGE_ID")
    private ReportBugEntity bug;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        BugMediaEntity that = (BugMediaEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
