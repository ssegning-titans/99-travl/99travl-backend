package com.ssegning.ntravl.server.domain.accounts;

import com.ssegning.ntravl.server.domain.chat.ChatEntity;
import com.ssegning.ntravl.server.domain.core.AbstractEntity;
import com.ssegning.ntravl.server.domain.embedded.AccountContact;
import com.ssegning.ntravl.server.domain.embedded.AccountMetaData;
import com.ssegning.ntravl.server.domain.embedded.AccountName;
import com.ssegning.ntravl.server.domain.friend.BaseFriendEntity;
import com.ssegning.ntravl.server.types.AccountStatus;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "NTV_ACCOUNT")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class AccountEntity extends AbstractEntity {

    public AccountEntity(UUID id) {
        super();
        this.setId(id);
    }

    @Embedded
    private AccountName name;

    @Embedded
    private AccountContact contact;

    @Embedded
    private AccountMetaData metaData;

    @Enumerated(EnumType.STRING)
    private AccountStatus status = AccountStatus.ALLOWED;

    @BatchSize(size = 5)
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "account", orphanRemoval = true)
    @ToString.Exclude
    private Set<AccountAddressEntity> addresses;

    @BatchSize(size = 5)
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "account", orphanRemoval = true)
    @ToString.Exclude
    private Set<PasswordEntity> passwords;

    @BatchSize(size = 5)
    @ManyToMany(cascade = CascadeType.REMOVE, mappedBy = "members")
    @ToString.Exclude
    private Set<BaseFriendEntity> friendShips;

    @BatchSize(size = 5)
    @ManyToMany(mappedBy = "members")
    @ToString.Exclude
    private Set<ChatEntity> chats;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        AccountEntity that = (AccountEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
