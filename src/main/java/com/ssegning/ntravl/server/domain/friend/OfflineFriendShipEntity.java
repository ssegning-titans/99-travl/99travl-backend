package com.ssegning.ntravl.server.domain.friend;

import com.ssegning.ntravl.server.converter.UrlJpaConverter;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.net.URL;
import java.util.Objects;

@Entity
@Table(name = "NTV_OFFLINE_FRIENDSHIP")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class OfflineFriendShipEntity extends BaseFriendEntity {

    /**
     * This can also be null, as he can be an app user
     * <p>
     * TODO create a cron job to know if this user is an app user
     */
    @Column(nullable = false, updatable = false)
    private String phoneNumber;

    @OneToOne(cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, mappedBy = "owner")
    private OfflineFriendShipRequestEntity request;

    private String description;

    private String firstName;

    private String lastName;

    @Column(length = 512)
    @Convert(converter = UrlJpaConverter.class)
    private URL avatar;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        OfflineFriendShipEntity that = (OfflineFriendShipEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
