package com.ssegning.ntravl.server.domain.payment;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "NTV_PAYMENT_PROPOSITION")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PropositionPaymentEntity extends BasePaymentEntity {

    @ManyToOne
    @JoinColumn(name = "PROPOSITION_ID")
    private BasePaymentEntity proposition;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        PropositionPaymentEntity that = (PropositionPaymentEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
