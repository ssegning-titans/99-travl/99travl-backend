package com.ssegning.ntravl.server.domain.report;

import com.ssegning.ntravl.server.domain.travel.TravelEntity;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "NTV_REPORT_TRAVEL")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ReportTravelEntity extends BaseReportEntity {

    @ManyToOne
    private TravelEntity travel;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ReportTravelEntity that = (ReportTravelEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
