package com.ssegning.ntravl.server.domain.report;

import com.ssegning.ntravl.server.domain.media.BugMediaEntity;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Entity
@Table(name = "NTV_REPORT_BUG")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ReportBugEntity extends BaseReportEntity {

    private String title;

    @CollectionTable(name = "NTV_REPORT_DEVICE_INFO")
    @ElementCollection
    private Map<String, String> deviceInfo;

    @Size(min = 1, max = 6)
    @OneToMany(
            cascade = {CascadeType.REMOVE, CascadeType.PERSIST},
            mappedBy = "bug",
            orphanRemoval = true)
    @ToString.Exclude
    private List<BugMediaEntity> screenshot;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ReportBugEntity that = (ReportBugEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
