package com.ssegning.ntravl.server.domain.rating;

import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import com.ssegning.ntravl.server.domain.core.AbstractEntity;
import com.ssegning.ntravl.server.types.RatingValue;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "NTV_RATING")
@Inheritance(strategy = InheritanceType.JOINED)
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public abstract class BaseRatingEntity extends AbstractEntity {

    @NotNull
    @Enumerated(EnumType.STRING)
    private RatingValue value = RatingValue.THREE;

    private String description;

    @ManyToOne
    @JoinColumn(name = "AUTHOR_ID")
    private AccountEntity author;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        BaseRatingEntity that = (BaseRatingEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
