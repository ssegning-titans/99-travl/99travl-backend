package com.ssegning.ntravl.server.domain.favorite;

import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import com.ssegning.ntravl.server.domain.core.AbstractEntity;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "NTV_FAVORITE")
@Inheritance(strategy = InheritanceType.JOINED)
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public abstract class BaseFavoriteEntity extends AbstractEntity {

    @ManyToOne
    @JoinColumn(name = "OWNER_ID")
    private AccountEntity owner;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        BaseFavoriteEntity that = (BaseFavoriteEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
