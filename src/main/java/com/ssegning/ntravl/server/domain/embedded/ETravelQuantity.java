package com.ssegning.ntravl.server.domain.embedded;

import com.ssegning.ntravl.server.model.TravelWeightUnit;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;

@SuperBuilder
@Data
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class ETravelQuantity implements Serializable {

    private Integer weight;

    @Enumerated(EnumType.STRING)
    private TravelWeightUnit unit;

    @Builder.Default
    private Integer pricePerUnit = 1;

}
