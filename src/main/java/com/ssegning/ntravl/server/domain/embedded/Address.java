package com.ssegning.ntravl.server.domain.embedded;

import lombok.*;
import org.locationtech.jts.geom.Point;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.PostLoad;
import javax.persistence.Transient;
import java.io.Serializable;

@Data
@Embeddable
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"formattedName", "point"})
public class Address implements Serializable {

    @Column(nullable = false, columnDefinition = "Geometry(Point,4326)")
    private Point point;

    @Transient
    private String formattedName;

    @Column
    private String street;

    @Column
    private String houseNumber;

    @Column
    private String region;

    @Column(nullable = false)
    private String city;

    @Column
    private String zip;

    @Column(nullable = false)
    private String country;

    @PostLoad
    private void postLoad() {
        formattedName = String.format("%s %s, %s %s, %s", street, houseNumber, zip, city, country);
        formattedName = formattedName.replace("null", "");
        formattedName = formattedName.trim();
        if (formattedName.startsWith(",")) {
            formattedName = formattedName.substring(1);
            formattedName = formattedName.trim();
        }
    }
}
