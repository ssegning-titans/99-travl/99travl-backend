package com.ssegning.ntravl.server.domain.travel;

import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import com.ssegning.ntravl.server.domain.core.AbstractEntity;
import com.ssegning.ntravl.server.domain.embedded.ETravelQuantity;
import com.ssegning.ntravl.server.model.TravelLuggageDimension;
import com.ssegning.ntravl.server.model.TravelModeUnit;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "NTV_TRAVEL")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TravelEntity extends AbstractEntity {

    public TravelEntity(UUID id) {
        super();
        this.setId(id);
    }

    @ManyToOne
    @JoinColumn(nullable = false)
    private AccountEntity provider;

    @Embedded
    private ETravelQuantity maxQuantity;

    @Enumerated(EnumType.STRING)
    private TravelModeUnit transportMode;

    @Enumerated(EnumType.STRING)
    private TravelLuggageDimension luggageDimension;

    private String description;

    @OneToMany(
            cascade = {CascadeType.REMOVE, CascadeType.PERSIST},
            mappedBy = "travel",
            orphanRemoval = true
    )
    @ToString.Exclude
    private Set<TravelPointEntity> passagePoints;

    @OneToMany(
            cascade = CascadeType.REMOVE,
            mappedBy = "travel",
            orphanRemoval = true
    )
    @ToString.Exclude
    private Set<TravelReservationEntity> reservations;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        TravelEntity that = (TravelEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
