package com.ssegning.ntravl.server.domain.media;

import com.ssegning.ntravl.server.domain.chat.MediaChatMessageEntity;
import com.ssegning.ntravl.server.domain.core.AbstractEntity;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Map;
import java.util.Objects;

@Entity
@Table(name = "NTV_MEDIA")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class MediaEntity extends AbstractEntity {

    @OneToOne(
            cascade = {CascadeType.REMOVE, CascadeType.PERSIST},
            fetch = FetchType.LAZY
    )
    @ToString.Exclude
    private DocumentEntity document;

    @Column
    private Integer width = 1;

    @Column
    private Integer height = 1;

    @Column
    private String name;

    @Column
    private String description;

    @CollectionTable(name = "NTV_MEDIA_META_DATA")
    @ElementCollection
    private Map<String, String> metaData;

    @Column(nullable = false, updatable = false)
    private Integer priority;

    @ManyToOne
    @JoinColumn(name = "MEDIA_CHAT_MESSAGE_ID")
    private MediaChatMessageEntity mediaChatMessage;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        MediaEntity that = (MediaEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
