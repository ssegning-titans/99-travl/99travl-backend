package com.ssegning.ntravl.server.domain.favorite;

import com.ssegning.ntravl.server.domain.travel.TravelEntity;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "NTV_FAVORITE_TRAVEL")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class FavoriteTravelEntity extends BaseFavoriteEntity {

    @ManyToOne
    @JoinColumn(name = "TRAVEL_ID")
    private TravelEntity travel;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        FavoriteTravelEntity that = (FavoriteTravelEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
