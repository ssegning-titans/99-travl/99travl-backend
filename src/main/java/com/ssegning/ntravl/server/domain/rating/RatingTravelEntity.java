package com.ssegning.ntravl.server.domain.rating;

import com.ssegning.ntravl.server.domain.travel.TravelEntity;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "NTV_RATING_TRAVEL")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class RatingTravelEntity extends BaseRatingEntity {

    @ManyToOne
    @JoinColumn(name = "TRAVEL_ID")
    private TravelEntity travel;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        RatingTravelEntity that = (RatingTravelEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
