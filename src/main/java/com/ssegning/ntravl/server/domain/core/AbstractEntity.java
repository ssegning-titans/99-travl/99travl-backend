package com.ssegning.ntravl.server.domain.core;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
public class AbstractEntity implements Serializable {

    protected static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid")
    @Column(nullable = false, updatable = false, columnDefinition = "uuid")
    private UUID id;

    @Column(nullable = false)
    @CreationTimestamp
    private Timestamp creationDate;

    @Column(nullable = false)
    @UpdateTimestamp
    private Timestamp updateDate;

}
