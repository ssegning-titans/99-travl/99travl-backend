package com.ssegning.ntravl.server.domain.chat;

import com.ssegning.ntravl.server.domain.travel.TravelPropositionEntity;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "NTV_PROPOSITION_CHAT_MESSAGE")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PropositionChatMessageEntity extends BaseChatMessageEntity {

    @ManyToOne
    @JoinColumn(name = "PROPOSITION_ID")
    private TravelPropositionEntity proposition;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        PropositionChatMessageEntity that = (PropositionChatMessageEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
