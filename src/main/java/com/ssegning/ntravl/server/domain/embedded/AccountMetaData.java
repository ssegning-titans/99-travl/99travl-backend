package com.ssegning.ntravl.server.domain.embedded;

import com.ssegning.ntravl.server.converter.UrlJpaConverter;
import com.ssegning.ntravl.server.model.AccountGender;
import com.ssegning.ntravl.server.model.AccountType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.net.URL;
import java.util.Locale;

@Data
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class AccountMetaData implements Serializable {

    @Enumerated(EnumType.STRING)
    private AccountGender gender;

    @Column
    private Locale locale;

    @Column
    private String bio;

    @Column(length = 512)
    @Convert(converter = UrlJpaConverter.class)
    private URL avatarUrl;

    @Enumerated(EnumType.STRING)
    private AccountType accountType = AccountType.C2C;

}
