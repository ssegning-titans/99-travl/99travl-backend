package com.ssegning.ntravl.server.domain.payment;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "NTV_FEE_PAYMENT")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class FeePaymentEntity extends BasePaymentEntity {

    @Column
    private String reason;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        FeePaymentEntity that = (FeePaymentEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
