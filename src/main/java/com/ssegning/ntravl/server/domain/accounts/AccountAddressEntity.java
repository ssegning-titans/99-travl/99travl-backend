package com.ssegning.ntravl.server.domain.accounts;

import com.ssegning.ntravl.server.domain.core.AbstractEntity;
import com.ssegning.ntravl.server.domain.embedded.Address;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "NTV_ACCOUNT_ADDRESS")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class AccountAddressEntity extends AbstractEntity {

    @Embedded
    private Address address;

    @ManyToOne
    @JoinColumn(name = "ACCOUNT_ID", updatable = false)
    private AccountEntity account;

    @Column(nullable = false, name = "PHONE_NUMBER")
    private String phoneNumber;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        AccountAddressEntity that = (AccountAddressEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
