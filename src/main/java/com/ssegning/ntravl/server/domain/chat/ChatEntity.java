package com.ssegning.ntravl.server.domain.chat;

import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import com.ssegning.ntravl.server.domain.core.AbstractEntity;
import com.ssegning.ntravl.server.domain.travel.TravelEntity;
import com.ssegning.ntravl.server.model.ChatStatus;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "NTV_CHAT")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ChatEntity extends AbstractEntity {

    @ManyToMany
    @Size(min = 2, max = 3)
    @ToString.Exclude
    @JoinTable(
            name = "NTV_CHAT_MEMBERS",
            joinColumns = @JoinColumn(name = "chat_id"),
            inverseJoinColumns = @JoinColumn(name = "account_id")
    )
    private Set<AccountEntity> members;

    @ManyToOne
    @JoinColumn(name = "TRAVEL_ID")
    private TravelEntity travel;

    @Enumerated(EnumType.ORDINAL)
    private ChatStatus status = ChatStatus.ACTIVE;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ChatEntity that = (ChatEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
