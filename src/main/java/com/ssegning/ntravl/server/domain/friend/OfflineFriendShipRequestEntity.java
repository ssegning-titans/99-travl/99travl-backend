package com.ssegning.ntravl.server.domain.friend;

import com.ssegning.ntravl.server.domain.core.AbstractEntity;
import com.ssegning.ntravl.server.types.FriendShipRequestStatus;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "NTV_OFFLINE_FRIENDSHIP_REQUEST")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class OfflineFriendShipRequestEntity extends AbstractEntity {

    @NotNull
    @Column(nullable = false)
    private String code;

    @OneToOne
    @JoinColumn(unique = true, nullable = false)
    private OfflineFriendShipEntity owner;

    @Enumerated(EnumType.STRING)
    private FriendShipRequestStatus status = FriendShipRequestStatus.PENDING;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        OfflineFriendShipRequestEntity that = (OfflineFriendShipRequestEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
