package com.ssegning.ntravl.server.domain.favorite;

import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "NTV_FAVORITE_ACCOUNT")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class FavoriteAccountEntity extends BaseFavoriteEntity {

    @ManyToOne
    @JoinColumn(name = "PERSON_ID")
    private AccountEntity person;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        FavoriteAccountEntity that = (FavoriteAccountEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
