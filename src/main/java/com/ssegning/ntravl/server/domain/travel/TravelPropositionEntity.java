package com.ssegning.ntravl.server.domain.travel;

import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import com.ssegning.ntravl.server.domain.chat.PropositionChatMessageEntity;
import com.ssegning.ntravl.server.domain.core.AbstractEntity;
import com.ssegning.ntravl.server.types.PropositionStatus;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.BatchSize;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "NTV_TRAVEL_PROPOSITION")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class TravelPropositionEntity extends AbstractEntity {

    @ManyToOne
    @JoinColumn(name = "AUTHOR_ID")
    private AccountEntity author;

    @ManyToOne
    @JoinColumn(name = "TRAVEL_ID")
    private TravelEntity travel;

    @Column(columnDefinition = "varchar(512)")
    @Length(max = 512)
    private String message;

    @ManyToOne
    @JoinColumn(nullable = false, name = "STARTING_POINT")
    private TravelPointEntity startingPoint;

    @ManyToOne
    @JoinColumn(nullable = false, name = "ENDING_POINT")
    private TravelPointEntity endingPoint;

    @BatchSize(size = 5)
    @OneToMany(cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, mappedBy = "proposition")
    @ToString.Exclude
    private Set<PropositionChatMessageEntity> propositions;

    @Enumerated(EnumType.STRING)
    private PropositionStatus status = PropositionStatus.PENDING;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        TravelPropositionEntity that = (TravelPropositionEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
