package com.ssegning.ntravl.server.domain.chat;

import com.ssegning.ntravl.server.annotation.AppPhoneNumber;
import com.ssegning.ntravl.server.converter.UrlJpaConverter;
import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import com.ssegning.ntravl.server.domain.core.AbstractEntity;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.net.URL;
import java.util.Objects;

@Entity
@Table(name = "NTV_CHAT_CONTACT")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ChatContactEntity extends AbstractEntity {

    @ManyToOne
    @JoinColumn(name = "REFERENCE_ACCOUNT_ID", nullable = true, updatable = false)
    private AccountEntity account;

    @Column
    private Boolean isAppUser;

    @AppPhoneNumber
    @Column
    private String phoneNumber;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private String description;

    @org.hibernate.validator.constraints.URL
    @Column(length = 512)
    @Convert(converter = UrlJpaConverter.class)
    private URL avatar;

    @ManyToOne
    @JoinColumn(name = "CONTACT_CHAT_MESSAGE_ID")
    private ContactChatMessageEntity contactChatMessage;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ChatContactEntity that = (ChatContactEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
