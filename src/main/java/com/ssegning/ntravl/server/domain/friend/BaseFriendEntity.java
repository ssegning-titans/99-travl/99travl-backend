package com.ssegning.ntravl.server.domain.friend;

import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import com.ssegning.ntravl.server.domain.core.AbstractEntity;
import com.ssegning.ntravl.server.model.FriendShipStatus;
import com.ssegning.ntravl.server.model.FriendType;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "NTV_FRIENDSHIP")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class BaseFriendEntity extends AbstractEntity {

    @ManyToMany
    @Size(min = 1, max = 2)
    @ToString.Exclude
    private Set<AccountEntity> members;

    @Enumerated(EnumType.STRING)
    private FriendShipStatus status = FriendShipStatus.PENDING;

    @Enumerated(EnumType.STRING)
    private FriendType type = FriendType.RECEIVER_ONLY;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        BaseFriendEntity that = (BaseFriendEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
