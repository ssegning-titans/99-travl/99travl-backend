package com.ssegning.ntravl.server.domain.payment;

import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import com.ssegning.ntravl.server.domain.core.AbstractEntity;
import com.ssegning.ntravl.server.types.PaymentStatus;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "NTV_PAYMENT")
@Inheritance(strategy = InheritanceType.JOINED)
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public abstract class BasePaymentEntity extends AbstractEntity {

    @Enumerated(EnumType.STRING)
    private PaymentStatus value = PaymentStatus.FINISHED;

    @ManyToOne
    @JoinColumn(name = "AUTHOR_ID")
    private AccountEntity author;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        BasePaymentEntity that = (BasePaymentEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
