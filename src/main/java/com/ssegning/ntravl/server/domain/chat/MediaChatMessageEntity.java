package com.ssegning.ntravl.server.domain.chat;

import com.ssegning.ntravl.server.domain.media.MediaEntity;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "NTV_MEDIA_CHAT_MESSAGE")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class MediaChatMessageEntity extends BaseChatMessageEntity {

    @Size(min = 1, max = 6)
    @OneToMany(
            cascade = {CascadeType.REMOVE, CascadeType.PERSIST},
            mappedBy = "mediaChatMessage",
            orphanRemoval = true)
    @ToString.Exclude
    private Set<MediaEntity> medias;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        MediaChatMessageEntity that = (MediaChatMessageEntity) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }
}
