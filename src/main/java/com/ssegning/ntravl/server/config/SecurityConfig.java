package com.ssegning.ntravl.server.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

@Slf4j
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true, jsr250Enabled = true)
@RequiredArgsConstructor
public class SecurityConfig {

    private static final String[] AUTH_PERMIT_LIST = {
            "/actuator/**",
    };
    private final Environment environment;

    @Bean
    public SecurityFilterChain securityWebFilterChain(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers(AUTH_PERMIT_LIST).permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/travels/**", "/api/v1/maps/**", "/api/v1/accounts/**").permitAll()
                .anyRequest().authenticated()
                .and()
                //
                .oauth2ResourceServer()
                .jwt(Customizer.withDefaults())
                .and()
                //
                .csrf()
                .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
                //
        ;

        for (String activeProfile : this.environment.getActiveProfiles()) {
            if (activeProfile.equals("dev")) {
                http.headers().frameOptions().disable();
                break;
            }
        }

        return http.build();
    }
}
