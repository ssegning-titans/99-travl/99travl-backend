package com.ssegning.ntravl.server.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "com.ssegning.ntravl.server.repo.*")
public class JpaConfig {
}
