package com.ssegning.ntravl.server.config;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;

@Slf4j
@Configuration
public class FcmConfig {

    @Bean
    public FirebaseMessaging firebaseMessaging(@Value("${app.firebase.config-file}") String firebaseConfigPath) throws IOException {
        var credentials = GoogleCredentials
                .fromStream(new ClassPathResource(firebaseConfigPath)
                .getInputStream());

        var options = FirebaseOptions.builder()
                .setCredentials(credentials)
                .build();

        if (FirebaseApp.getApps().isEmpty()) {
            FirebaseApp.initializeApp(options);
            log.info("Firebase application has been initialized");
        }
        return FirebaseMessaging.getInstance();
    }
}
