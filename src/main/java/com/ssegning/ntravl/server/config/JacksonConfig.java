package com.ssegning.ntravl.server.config;

import com.ssegning.ntravl.server.mapper.core.AddressMapper;
import lombok.RequiredArgsConstructor;
import org.n52.jackson.datatype.jts.JtsModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class JacksonConfig {

    private final AddressMapper addressMapper;

    @Bean
    public JtsModule jtsModule() {
        return new JtsModule(addressMapper.getGf());
    }

}
