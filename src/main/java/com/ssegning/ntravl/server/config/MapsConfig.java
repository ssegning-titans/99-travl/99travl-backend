package com.ssegning.ntravl.server.config;

import com.google.maps.GeoApiContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapsConfig {

    @Bean
    public GeoApiContext geoApiContext(@Value("${api.mapsApiKey}") String mapsApiKey) {
        return new GeoApiContext.Builder().apiKey(mapsApiKey).build();
    }
}
