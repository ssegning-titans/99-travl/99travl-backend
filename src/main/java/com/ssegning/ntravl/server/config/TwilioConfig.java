package com.ssegning.ntravl.server.config;

import com.twilio.Twilio;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class TwilioConfig {

    @Value("${api.twilio.sid}")
    private String twilioSid;

    @Value("${api.twilio.token}")
    private String twilioToken;

    @PostConstruct
    public void postConstruct() {
        Twilio.init(twilioSid, twilioToken);
    }

}
