package com.ssegning.ntravl.server.repo.account;

import com.ssegning.ntravl.server.domain.accounts.AccountAddressEntity;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;
import java.util.UUID;

public interface AccountAddressRepo extends AbstractRepository<AccountAddressEntity> {

    Optional<AccountAddressEntity> findByIdAndAccountId(UUID id, UUID account_id);

    Optional<AccountAddressEntity> findFirstByAccountIdOrderByCreationDate(UUID account_id);

    Page<AccountAddressEntity> findAllByAccountIdOrderByCreationDate(UUID account_id, Pageable pageable);

}
