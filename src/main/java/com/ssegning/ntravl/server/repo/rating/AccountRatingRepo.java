package com.ssegning.ntravl.server.repo.rating;

import com.ssegning.ntravl.server.domain.rating.RatingAccountEntity;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface AccountRatingRepo extends AbstractRepository<RatingAccountEntity> {

    Optional<RatingAccountEntity> findByPersonIdAndAuthorId(UUID person_id, UUID author_id);

    List<RatingAccountEntity> findAllByPersonId(UUID person_id);

    Page<RatingAccountEntity> findAllByPersonId(UUID person_id, Pageable pageable);

}
