package com.ssegning.ntravl.server.repo.account;

import com.ssegning.ntravl.server.domain.accounts.PhoneNumberValidationEntity;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;
import lombok.NonNull;

import java.util.Optional;
import java.util.UUID;

public interface PhoneNumberValidationRepo extends AbstractRepository<PhoneNumberValidationEntity> {

    Optional<PhoneNumberValidationEntity> findByCodeAndIdAndAccountIdAndPhoneNumber(@NonNull String code, UUID id, UUID account_id, @NonNull String phoneNumber);

}
