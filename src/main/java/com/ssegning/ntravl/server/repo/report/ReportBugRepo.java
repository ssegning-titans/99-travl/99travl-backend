package com.ssegning.ntravl.server.repo.report;

import com.ssegning.ntravl.server.domain.report.ReportBugEntity;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface ReportBugRepo extends AbstractRepository<ReportBugEntity> {

    Page<ReportBugEntity> findAllByAuthorId(UUID authorId, Pageable pageable);

}
