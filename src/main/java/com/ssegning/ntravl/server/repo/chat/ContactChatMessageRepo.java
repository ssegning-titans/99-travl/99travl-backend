package com.ssegning.ntravl.server.repo.chat;

import com.ssegning.ntravl.server.domain.chat.ContactChatMessageEntity;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;

public interface ContactChatMessageRepo extends AbstractRepository<ContactChatMessageEntity> {
}
