package com.ssegning.ntravl.server.repo.chat;

import com.ssegning.ntravl.server.domain.chat.PropositionChatMessageEntity;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;

public interface PropositionChatMessageRepo extends AbstractRepository<PropositionChatMessageEntity> {
}
