package com.ssegning.ntravl.server.repo.report;

import com.ssegning.ntravl.server.domain.report.ReportAccountEntity;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface ReportAccountRepo extends AbstractRepository<ReportAccountEntity> {

    Page<ReportAccountEntity> findAllByAccountId(UUID accountId, Pageable pageable);

}
