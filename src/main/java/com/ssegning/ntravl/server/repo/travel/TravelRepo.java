package com.ssegning.ntravl.server.repo.travel;

import com.ssegning.ntravl.server.domain.travel.TravelEntity;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface TravelRepo extends AbstractRepository<TravelEntity> {

    Page<TravelEntity> findAllByProviderId(UUID provider_id, Pageable pageable);

}
