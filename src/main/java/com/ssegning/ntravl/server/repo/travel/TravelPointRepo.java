package com.ssegning.ntravl.server.repo.travel;

import com.ssegning.ntravl.server.domain.travel.TravelPointEntity;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.UUID;

@Repository
public interface TravelPointRepo extends AbstractRepository<TravelPointEntity> {

    @Query("SELECT a, b " +
            "FROM TravelPointEntity a JOIN TravelPointEntity b ON (a.travel.id = b.travel.id AND a.priority < b.priority ) " +
            "WHERE ST_DistanceSphere(a.address.point, ST_GeomFromGeoJSON(:startPoint)) <= :startRadius " +
            "AND ST_DistanceSphere(b.address.point, ST_GeomFromGeoJSON(:endPoint)) <= :endRadius " +
            "AND (a.passageTime BETWEEN :startDate AND :endDate)")
    Page<Object[]> findByStartAndEnd(
            @Param("startPoint") String startPoint,
            @Param("endPoint") String endPoint,
            @Param("startRadius") Double startRadius,
            @Param("endRadius") Double endRadius,
            @Param("startDate") Date startDate,
            @Param("endDate") Date endDate,
            Pageable pageable
    );

    @Query("SELECT a FROM TravelPointEntity a " +
            "WHERE ST_DistanceSphere(a.address.point, ST_GeomFromGeoJSON(:point)) <= :radius")
    Page<TravelPointEntity> findRandomlyNearPoint(
            @Param("point") String point,
            @Param("radius") Double radius,
            Pageable pageable
    );

    Page<TravelPointEntity> findAllByTravelId(UUID travelId, Pageable pageable);

}
