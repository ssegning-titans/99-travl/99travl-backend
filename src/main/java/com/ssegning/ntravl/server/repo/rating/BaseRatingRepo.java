package com.ssegning.ntravl.server.repo.rating;

import com.ssegning.ntravl.server.domain.rating.BaseRatingEntity;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface BaseRatingRepo extends AbstractRepository<BaseRatingEntity> {

    Page<BaseRatingEntity> findAllByAuthorId(UUID authorId, Pageable pageable);

}
