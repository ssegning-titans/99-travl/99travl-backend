package com.ssegning.ntravl.server.repo.report;

import com.ssegning.ntravl.server.domain.report.BaseReportEntity;
import com.ssegning.ntravl.server.model.ReportStatus;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface BaseReportRepo extends AbstractRepository<BaseReportEntity> {

    Page<BaseReportEntity> findAllByStatus(ReportStatus status, Pageable pageable);

    Page<BaseReportEntity> findAllByAuthorIdAndStatus(UUID authorId, ReportStatus status, Pageable pageable);

}
