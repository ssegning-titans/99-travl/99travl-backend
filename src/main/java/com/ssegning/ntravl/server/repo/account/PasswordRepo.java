package com.ssegning.ntravl.server.repo.account;

import com.ssegning.ntravl.server.domain.accounts.PasswordEntity;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;
import com.ssegning.ntravl.server.types.PasswordStatus;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.UUID;

public interface PasswordRepo extends AbstractRepository<PasswordEntity> {

    PasswordEntity findTopByAccountIdAndStatus(UUID accountId, PasswordStatus status);

    @Transactional
    @Modifying
    @Query("UPDATE PasswordEntity pr SET pr.status = :status WHERE pr.account.id = :accountId AND pr.status = 'ACTIVE'")
    void deactivatePrevious(
            @Param("accountId") UUID accountId,
            @Param("status") PasswordStatus status
    );
}
