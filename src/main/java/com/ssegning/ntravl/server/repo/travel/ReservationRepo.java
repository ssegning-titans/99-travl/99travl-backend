package com.ssegning.ntravl.server.repo.travel;

import com.ssegning.ntravl.server.domain.travel.TravelReservationEntity;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;
import java.util.UUID;

public interface ReservationRepo extends AbstractRepository<TravelReservationEntity> {

    Page<TravelReservationEntity> findAllByTravelId(UUID travelId, Pageable pageable);

    Long countAllByTravelId(UUID travelId);

    Optional<TravelReservationEntity> findByTravelIdAndId(UUID travelId, UUID id);

}
