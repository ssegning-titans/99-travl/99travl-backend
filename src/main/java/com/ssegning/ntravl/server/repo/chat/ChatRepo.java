package com.ssegning.ntravl.server.repo.chat;

import com.ssegning.ntravl.server.domain.chat.ChatEntity;
import com.ssegning.ntravl.server.model.ChatStatus;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

public interface ChatRepo extends AbstractRepository<ChatEntity> {

    @Transactional
    @Modifying
    @Query("UPDATE ChatEntity bc SET bc.status = :status WHERE bc.id = :id")
    void changeChatStatus(@Param("id") UUID id, @Param("status") ChatStatus seen);

    Page<ChatEntity> findByMembers_IdAndStatus(UUID members_id, ChatStatus status, Pageable pageable);

    @Query("SELECT c FROM ChatEntity c " +
            "LEFT JOIN c.members m ON m.id = :m1 " +
            "WHERE :m2 IN (SELECT mp.id FROM c.members mp WHERE mp.id <> :m1)")
    List<ChatEntity> findByMembers(@Param("m1") UUID member1, @Param("m1") UUID member2);

}
