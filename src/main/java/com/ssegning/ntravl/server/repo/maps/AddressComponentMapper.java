package com.ssegning.ntravl.server.repo.maps;

import com.google.maps.model.AddressComponent;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AddressComponentMapper {

    com.ssegning.ntravl.server.model.AddressComponent toModel(AddressComponent addressComponent);

    List<com.ssegning.ntravl.server.model.AddressComponent> toModel(List<AddressComponent> addressComponents);

}

