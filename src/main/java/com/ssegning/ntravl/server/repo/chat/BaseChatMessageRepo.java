package com.ssegning.ntravl.server.repo.chat;

import com.ssegning.ntravl.server.domain.chat.BaseChatMessageEntity;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public interface BaseChatMessageRepo extends AbstractRepository<BaseChatMessageEntity> {

    Page<BaseChatMessageEntity> findAllByChatId(UUID chatId, Pageable pageable);

    Page<BaseChatMessageEntity> findAllByChatIdAndSeen(UUID chatId, Boolean seen, Pageable pageable);

    Page<BaseChatMessageEntity> findAllByAuthorIdNotInAndSeen(
            Collection<UUID> authorId,
            Boolean seen,
            Pageable pageable
    );

    @Transactional
    @Modifying
    @Query("UPDATE BaseChatMessageEntity bc SET bc.seen = :seen WHERE bc.id = :id")
    void markAsRead(@Param("id") UUID id, @Param("seen") Boolean seen);

}
