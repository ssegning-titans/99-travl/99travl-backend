package com.ssegning.ntravl.server.repo.travel;

import com.ssegning.ntravl.server.domain.travel.TravelPropositionEntity;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface PropositionRepo extends AbstractRepository<TravelPropositionEntity> {

    Page<TravelPropositionEntity> getAllByTravelId(UUID travelId, Pageable pageable);

}
