package com.ssegning.ntravl.server.repo.chat;

import com.ssegning.ntravl.server.domain.chat.TextChatMessageEntity;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;

public interface TextChatMessageRepo extends AbstractRepository<TextChatMessageEntity> {
}
