package com.ssegning.ntravl.server.repo.rating;

import com.ssegning.ntravl.server.domain.rating.RatingTravelEntity;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TravelRatingRepo extends AbstractRepository<RatingTravelEntity> {

    Optional<RatingTravelEntity> findByTravelIdAndAuthorId(UUID travel_id, UUID author_id);

    List<RatingTravelEntity> findAllByTravelId(UUID travel_id);

    Page<RatingTravelEntity> findAllByTravelId(UUID travel_id, Pageable pageable);

}
