package com.ssegning.ntravl.server.repo.friend;

import com.ssegning.ntravl.server.domain.friend.OfflineFriendShipEntity;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;

public interface OfflineFriendRepo extends AbstractRepository<OfflineFriendShipEntity> {
}
