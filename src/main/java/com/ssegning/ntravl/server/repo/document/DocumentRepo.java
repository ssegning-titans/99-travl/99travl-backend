package com.ssegning.ntravl.server.repo.document;

import com.ssegning.ntravl.server.domain.media.DocumentEntity;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;

import java.util.Optional;

public interface DocumentRepo extends AbstractRepository<DocumentEntity> {

    Optional<DocumentEntity> getByFilePath(String filePath);

}
