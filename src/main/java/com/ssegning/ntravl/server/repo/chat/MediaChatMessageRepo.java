package com.ssegning.ntravl.server.repo.chat;

import com.ssegning.ntravl.server.domain.chat.MediaChatMessageEntity;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;

public interface MediaChatMessageRepo extends AbstractRepository<MediaChatMessageEntity> {
}
