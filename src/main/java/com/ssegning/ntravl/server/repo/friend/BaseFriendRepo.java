package com.ssegning.ntravl.server.repo.friend;

import com.ssegning.ntravl.server.domain.friend.BaseFriendEntity;
import com.ssegning.ntravl.server.model.FriendShipStatus;
import com.ssegning.ntravl.server.model.FriendType;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface BaseFriendRepo extends AbstractRepository<BaseFriendEntity> {

    Page<BaseFriendEntity> findAllByMembers_Id(UUID memberId, Pageable pageable);

    Page<BaseFriendEntity> findAllByMembers_IdAndStatus(UUID members_id, FriendShipStatus status, Pageable pageable);

    Page<BaseFriendEntity> findAllByMembers_IdAndType(UUID members_id, FriendType type, Pageable pageable);

    Page<BaseFriendEntity> findAllByMembers_IdAndTypeAndStatus(UUID members_id, FriendType type, FriendShipStatus status, Pageable pageable);

}
