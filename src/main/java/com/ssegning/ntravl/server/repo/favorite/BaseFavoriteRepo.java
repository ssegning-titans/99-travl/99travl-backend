package com.ssegning.ntravl.server.repo.favorite;

import com.ssegning.ntravl.server.domain.favorite.BaseFavoriteEntity;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface BaseFavoriteRepo extends AbstractRepository<BaseFavoriteEntity> {

    Page<BaseFavoriteEntity> getAllByOwnerId(UUID ownerId, Pageable pageable);

}
