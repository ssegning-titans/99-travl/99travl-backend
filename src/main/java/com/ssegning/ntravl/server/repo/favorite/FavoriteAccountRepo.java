package com.ssegning.ntravl.server.repo.favorite;

import com.ssegning.ntravl.server.domain.favorite.FavoriteAccountEntity;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;

import java.util.UUID;

public interface FavoriteAccountRepo extends AbstractRepository<FavoriteAccountEntity> {

    FavoriteAccountEntity findByOwnerIdAndPersonId(UUID ownerId, UUID personId);

}
