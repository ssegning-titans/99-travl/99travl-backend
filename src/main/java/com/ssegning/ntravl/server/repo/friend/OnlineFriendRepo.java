package com.ssegning.ntravl.server.repo.friend;

import com.ssegning.ntravl.server.domain.friend.OnlineFriendShipEntity;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;

public interface OnlineFriendRepo extends AbstractRepository<OnlineFriendShipEntity> {
}
