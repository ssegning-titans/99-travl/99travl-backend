package com.ssegning.ntravl.server.repo.favorite;

import com.ssegning.ntravl.server.domain.favorite.FavoriteTravelEntity;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;

import java.util.UUID;

public interface FavoriteTravelRepo extends AbstractRepository<FavoriteTravelEntity> {

    FavoriteTravelEntity findByOwnerIdAndTravelId(UUID ownerId, UUID travelId);

}
