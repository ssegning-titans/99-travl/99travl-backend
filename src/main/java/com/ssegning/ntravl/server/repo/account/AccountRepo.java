package com.ssegning.ntravl.server.repo.account;

import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.validation.constraints.Email;
import java.util.Optional;

public interface AccountRepo extends AbstractRepository<AccountEntity> {

    @Query("SELECT a FROM AccountEntity a WHERE LOWER(a.contact.email) LIKE LOWER(:email)")
    Optional<AccountEntity> findByContactEmail(@Param("email") @Email String email);

    Optional<AccountEntity> findByContactPhoneNumber(String phoneNumber);

    @Query("SELECT a FROM AccountEntity a WHERE LOWER(a.name.firstName) LIKE :query OR LOWER(a.name.lastName) LIKE :query OR LOWER(a.metaData.bio) LIKE :query OR LOWER(a.contact.email) LIKE :query OR LOWER(a.contact.phoneNumber) LIKE :query")
    Page<AccountEntity> findAllByName(String query, Pageable pageable);

    @Query("SELECT COUNT (*) FROM AccountEntity a WHERE LOWER(a.name.firstName) LIKE :query OR LOWER(a.name.lastName) LIKE :query OR LOWER(a.metaData.bio) LIKE :query OR LOWER(a.contact.email) LIKE :query OR LOWER(a.contact.phoneNumber) LIKE :query")
    Long countAccountsBy(String query);

}
