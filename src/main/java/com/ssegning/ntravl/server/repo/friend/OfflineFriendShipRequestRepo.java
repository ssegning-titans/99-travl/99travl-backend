package com.ssegning.ntravl.server.repo.friend;

import com.ssegning.ntravl.server.domain.friend.OfflineFriendShipRequestEntity;
import com.ssegning.ntravl.server.repo.core.AbstractRepository;

public interface OfflineFriendShipRequestRepo extends AbstractRepository<OfflineFriendShipRequestEntity> {
}
