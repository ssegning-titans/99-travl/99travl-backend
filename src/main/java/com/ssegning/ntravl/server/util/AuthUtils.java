package com.ssegning.ntravl.server.util;

import com.ssegning.ntravl.server.exceptions.AuthException;
import com.ssegning.ntravl.server.model.Account;
import com.ssegning.ntravl.server.service.AccountService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;

import java.util.UUID;

public class AuthUtils {

    private AuthUtils() {
    }

    public static UUID getActualUserId() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            throw new AuthException();
        }
        var principal = (Jwt) authentication.getPrincipal();
        var subArr = principal.getSubject().split(":");
        return UUID.fromString(subArr[subArr.length - 1]);
    }

    public static Account getActualUserId(AccountService accountService) {
        final UUID actualUserId = getActualUserId();
        return accountService.findById(actualUserId);
    }
}
