package com.ssegning.ntravl.server.util;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class AppPageRequest extends PageRequest {

    public AppPageRequest(int page, int size, List<String> sort) {
        super(page, size, AppPageRequest.mapSort(sort));
    }

    private static Sort mapSort(List<String> sort) {
        if (sort == null || sort.isEmpty() || sort.size() % 2 != 0) {
            return Sort.unsorted();
        }

        var couples = new HashSet<String[]>();
        for (int i = 0; i < sort.size(); i = i + 2) {
            var couple = new String[]{sort.get(i), sort.get(i + 1)};
            couples.add(couple);
        }

        var orders = couples
                .stream()
                .map(s -> {
                    var property = s[0];
                    var directionStr = s[1];
                    var direction = Sort.Direction.fromString(directionStr);
                    return new Sort.Order(direction, property);
                })
                .collect(Collectors.toList());

        return Sort.by(orders);
    }

    public static AppPageRequest of() {
        return AppPageRequest.of(0);
    }

    public static AppPageRequest of(int page) {
        return AppPageRequest.of(page, 20);
    }

    public static AppPageRequest of(int page, int size) {
        return AppPageRequest.of(page, size, List.of());
    }

    public static AppPageRequest of(int page, int size, List<String> sort) {
        return new AppPageRequest(page, size, sort);
    }
}
