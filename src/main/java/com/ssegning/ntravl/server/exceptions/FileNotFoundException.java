package com.ssegning.ntravl.server.exceptions;

public class FileNotFoundException extends NotFoundException {
    public FileNotFoundException(String path) {
        super("${file.not.found}");
    }

    public FileNotFoundException() {
        super();
    }
}
