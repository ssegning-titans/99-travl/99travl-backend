package com.ssegning.ntravl.server.exceptions;

import java.util.UUID;

public class ReportNotFoundException extends NotFoundException {
    public ReportNotFoundException(UUID id) {
        super(id);
    }
}
