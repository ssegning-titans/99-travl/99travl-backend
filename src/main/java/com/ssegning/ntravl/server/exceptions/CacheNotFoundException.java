package com.ssegning.ntravl.server.exceptions;

import java.io.Serializable;

public class CacheNotFoundException extends NotFoundException {
    public CacheNotFoundException(Serializable key) {
        super(key.toString());
    }

    public CacheNotFoundException() {
        this("cache");
    }
}
