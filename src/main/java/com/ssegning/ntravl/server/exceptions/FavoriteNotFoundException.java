package com.ssegning.ntravl.server.exceptions;

import java.io.Serializable;

public class FavoriteNotFoundException extends NotFoundException {

    public FavoriteNotFoundException(Serializable key) {
        super(key.toString());
    }

    public FavoriteNotFoundException() {
        super("favorite");
    }
}
