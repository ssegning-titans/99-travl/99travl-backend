package com.ssegning.ntravl.server.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN, reason = "${forbidden.error}")
public class ForbiddenRequestException extends BaseAppException {

    public ForbiddenRequestException() {
        super("forbidden-request");
    }

    public ForbiddenRequestException(String message) {
        super(message);
    }

    public ForbiddenRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public ForbiddenRequestException(Throwable cause) {
        super(cause);
    }

    public ForbiddenRequestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
