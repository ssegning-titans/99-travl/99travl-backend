package com.ssegning.ntravl.server.exceptions;

import java.io.Serializable;

public class PhoneTokenNotFoundException extends NotFoundException {

    public PhoneTokenNotFoundException(Serializable key) {
        super(key.toString());
    }

    public PhoneTokenNotFoundException() {
        super("phoneNumber");
    }
}
