package com.ssegning.ntravl.server.exceptions;

import java.io.Serializable;

public class ChatNotFoundException extends NotFoundException {

    public ChatNotFoundException(Serializable key) {
        super(key.toString());
    }

    public ChatNotFoundException() {
        super("chat");
    }
}
