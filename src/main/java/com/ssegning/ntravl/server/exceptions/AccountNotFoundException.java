package com.ssegning.ntravl.server.exceptions;

import java.io.Serializable;

public class AccountNotFoundException extends NotFoundException {
    public AccountNotFoundException(Serializable identifier) {
        super(identifier.toString());
    }
}
