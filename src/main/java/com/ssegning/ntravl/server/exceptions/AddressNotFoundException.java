package com.ssegning.ntravl.server.exceptions;

import java.util.UUID;

public class AddressNotFoundException extends NotFoundException {
    public AddressNotFoundException(String typeOfAddress, UUID id) {
        super(String.format("%s-address.not.found:%s", typeOfAddress, id));
    }
}
