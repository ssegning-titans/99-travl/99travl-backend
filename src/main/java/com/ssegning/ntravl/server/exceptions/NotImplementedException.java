package com.ssegning.ntravl.server.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serializable;

@ResponseStatus(value = HttpStatus.NOT_IMPLEMENTED, reason = "${not.implemented.error}")
public class NotImplementedException extends BaseAppException {
    public NotImplementedException() {
    }

    public NotImplementedException(Serializable key) {
        super(key.toString());
    }

    public NotImplementedException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotImplementedException(Throwable cause) {
        super(cause);
    }

    public NotImplementedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
