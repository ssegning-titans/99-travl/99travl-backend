package com.ssegning.ntravl.server.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serializable;


@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "${not.found}")
public class NotFoundException extends BaseAppException {
    public NotFoundException() {
    }

    public NotFoundException(Serializable key) {
        super(key.toString());
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotFoundException(Throwable cause) {
        super(cause);
    }

    public NotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
