package com.ssegning.ntravl.server.exceptions;

import java.io.Serializable;

public class EmailTokenNotFoundException extends NotFoundException {

    public EmailTokenNotFoundException(Serializable key) {
        super(key.toString());
    }

    public EmailTokenNotFoundException() {
        super("email");
    }
}
