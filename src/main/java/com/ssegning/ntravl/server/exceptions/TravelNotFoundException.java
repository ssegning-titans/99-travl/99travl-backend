package com.ssegning.ntravl.server.exceptions;

import java.util.UUID;

public class TravelNotFoundException extends NotFoundException {
    public TravelNotFoundException(UUID id) {
        super(id);
    }
}
