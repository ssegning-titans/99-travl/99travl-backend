package com.ssegning.ntravl.server.exceptions;

import java.io.Serializable;

public class ChatMessageNotFoundException extends NotFoundException {

    public ChatMessageNotFoundException(Serializable key) {
        super(key.toString());
    }

    public ChatMessageNotFoundException() {
        super("chatMessage");
    }
}
