package com.ssegning.ntravl.server.events;

import com.ssegning.ntravl.server.model.BaseChatMessage;
import lombok.Getter;

@Getter
public class ChatMessageCreatedEvent extends AbstractEvent {

    private static final String NAME = "chat_message_created";

    private final BaseChatMessage chatMessage;

    /**
     * Create a new {@code ApplicationEvent}.
     *
     * @param chatMessage the object on which the event initially occurred or with
     *                    which the event is associated (never {@code null})
     */
    public ChatMessageCreatedEvent(BaseChatMessage chatMessage) {
        super(chatMessage);
        this.chatMessage = chatMessage;
    }

    @Override
    public String getName() {
        return NAME;
    }
}
