package com.ssegning.ntravl.server.events;

import com.ssegning.ntravl.server.model.OnlineFriend;
import lombok.Getter;

@Getter
public class OnlineFriendCreatedEvent extends AbstractEvent {

    private static final String NAME = "online_friend_created";

    private final OnlineFriend friend;

    /**
     * Create a new {@code ApplicationEvent}.
     *
     * @param friend the object on which the event initially occurred or with
     *               which the event is associated (never {@code null})
     */
    public OnlineFriendCreatedEvent(OnlineFriend friend) {
        super(friend);
        this.friend = friend;
    }

    @Override
    public String getName() {
        return NAME;
    }
}
