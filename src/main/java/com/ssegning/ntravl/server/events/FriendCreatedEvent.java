package com.ssegning.ntravl.server.events;

import com.ssegning.ntravl.server.model.BaseFriend;
import lombok.Getter;

@Getter
public class FriendCreatedEvent extends AbstractEvent {

    private static final String NAME = "friend_created";

    private final BaseFriend friend;

    /**
     * Create a new {@code ApplicationEvent}.
     *
     * @param friend the object on which the event initially occurred or with
     *               which the event is associated (never {@code null})
     */
    public FriendCreatedEvent(BaseFriend friend) {
        super(friend);
        this.friend = friend;
    }

    @Override
    public String getName() {
        return NAME;
    }
}
