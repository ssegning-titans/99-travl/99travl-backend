package com.ssegning.ntravl.server.events;

import org.springframework.context.ApplicationEvent;

public abstract class AbstractEvent extends ApplicationEvent {
    /**
     * Create a new {@code ApplicationEvent}.
     *
     * @param source the object on which the event initially occurred or with
     *               which the event is associated (never {@code null})
     */
    public AbstractEvent(Object source) {
        super(source);
    }

    public abstract String getName();
}
