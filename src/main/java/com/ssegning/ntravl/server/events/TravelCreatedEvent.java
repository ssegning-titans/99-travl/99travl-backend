package com.ssegning.ntravl.server.events;

import com.ssegning.ntravl.server.model.Travel;
import lombok.Getter;

@Getter
public class TravelCreatedEvent extends AbstractEvent {

    private static final String NAME = "travel_created";

    private final Travel travel;

    /**
     * Create a new {@code ApplicationEvent}.
     *
     * @param travel the object on which the event initially occurred or with
     *               which the event is associated (never {@code null})
     */
    public TravelCreatedEvent(Travel travel) {
        super(travel);
        this.travel = travel;
    }

    @Override
    public String getName() {
        return NAME;
    }
}
