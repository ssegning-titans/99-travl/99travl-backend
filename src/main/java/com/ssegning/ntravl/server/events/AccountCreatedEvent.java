package com.ssegning.ntravl.server.events;

import com.ssegning.ntravl.server.model.Account;
import lombok.Getter;

@Getter
public class AccountCreatedEvent extends AbstractEvent {

    private static final String NAME = "account_created";

    private final Account account;

    /**
     * Create a new {@code ApplicationEvent}.
     *
     * @param account the object on which the event initially occurred or with
     *                which the event is associated (never {@code null})
     */
    public AccountCreatedEvent(Account account) {
        super(account);
        this.account = account;
    }

    @Override
    public String getName() {
        return NAME;
    }
}
