package com.ssegning.ntravl.server.events;

import com.ssegning.ntravl.server.model.Chat;
import lombok.Getter;

@Getter
public class ChatCreatedEvent extends AbstractEvent {

    private static final String NAME = "chat_created";

    private final Chat chat;

    /**
     * Create a new {@code ApplicationEvent}.
     *
     * @param chat the object on which the event initially occurred or with
     *             which the event is associated (never {@code null})
     */
    public ChatCreatedEvent(Chat chat) {
        super(chat);
        this.chat = chat;
    }

    @Override
    public String getName() {
        return NAME;
    }
}
