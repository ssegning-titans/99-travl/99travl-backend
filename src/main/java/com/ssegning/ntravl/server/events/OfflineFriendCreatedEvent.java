package com.ssegning.ntravl.server.events;

import com.ssegning.ntravl.server.model.OfflineFriend;
import lombok.Getter;

@Getter
public class OfflineFriendCreatedEvent extends AbstractEvent {

    private static final String NAME = "offline_friend_created";

    private final OfflineFriend friend;

    /**
     * Create a new {@code ApplicationEvent}.
     *
     * @param friend the object on which the event initially occurred or with
     *               which the event is associated (never {@code null})
     */
    public OfflineFriendCreatedEvent(OfflineFriend friend) {
        super(friend);
        this.friend = friend;
    }

    @Override
    public String getName() {
        return NAME;
    }
}
