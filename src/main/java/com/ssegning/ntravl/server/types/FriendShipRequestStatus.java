package com.ssegning.ntravl.server.types;

public enum FriendShipRequestStatus {
    PENDING, ACCEPTED, REFUSED
}
