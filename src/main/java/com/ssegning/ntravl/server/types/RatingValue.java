package com.ssegning.ntravl.server.types;

public enum RatingValue {
    ONE,
    TWO,
    THREE,
    FOUR,
    FIVE
}
