package com.ssegning.ntravl.server.types;

public enum PasswordRecoverStatus {
    INIT, USED, DEACTIVATED
}
