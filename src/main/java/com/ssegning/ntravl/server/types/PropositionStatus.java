package com.ssegning.ntravl.server.types;

public enum PropositionStatus {
    PENDING, ACCEPTED, REFUSED, CLOSED
}
