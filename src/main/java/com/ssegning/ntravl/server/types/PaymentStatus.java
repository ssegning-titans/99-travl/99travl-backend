package com.ssegning.ntravl.server.types;

public enum PaymentStatus {
    INITIALIZED,
    PENDING,
    FINISHED,
    CLOSED,
}
