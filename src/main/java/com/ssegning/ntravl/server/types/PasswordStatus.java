package com.ssegning.ntravl.server.types;

public enum PasswordStatus {
    ACTIVE, INACTIVE
}
