package com.ssegning.ntravl.server.types;

public enum AccountStatus {
    ALLOWED, BLOCKED;

    public static AccountStatus fromValue(String value) {
        for (AccountStatus b : AccountStatus.values()) {
            if (b.name().equals(value)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
}
