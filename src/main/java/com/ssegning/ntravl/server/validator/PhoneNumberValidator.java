package com.ssegning.ntravl.server.validator;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.ssegning.ntravl.server.annotation.AppPhoneNumber;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class PhoneNumberValidator implements ConstraintValidator<AppPhoneNumber, String> {
    private static final PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
    private String region;

    static boolean validatePhoneNumber(String value, String region) {
        if (value == null) return true;
        else try {
            var phoneNumber = phoneUtil.parse(value, region);
            return phoneUtil.isValidNumber(phoneNumber);
        } catch (NumberParseException e) {
            return false;
        }
    }

    @Override
    public void initialize(AppPhoneNumber constraintAnnotation) {
        region = constraintAnnotation.region();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || validatePhoneNumber(value, region);
    }
}
