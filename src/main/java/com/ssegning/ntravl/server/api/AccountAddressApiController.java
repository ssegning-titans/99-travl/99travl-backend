package com.ssegning.ntravl.server.api;

import com.ssegning.ntravl.server.model.AccountAddress;
import com.ssegning.ntravl.server.service.AccountAddressService;
import com.ssegning.ntravl.server.service.AccountService;
import com.ssegning.ntravl.server.util.AppPageRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
public class AccountAddressApiController implements AccountAddressApi {

    private final AccountAddressService addressService;
    private final AccountService accountService;

    @Override
    public ResponseEntity<AccountAddress> createAddress(UUID accountId, AccountAddress accountAddress) {
        accountAddress.setAccountId(accountId);
        var done = addressService.save(accountAddress);
        return ResponseEntity.status(201).body(done);
    }

    @Override
    public ResponseEntity<Void> deleteAddressById(UUID accountId, UUID addressId) {
        addressService.deleteById(accountId, addressId);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<AccountAddress> findAddressById(UUID accountId, UUID addressId) {
        var found = addressService.findById(accountId, addressId);
        return ResponseEntity.ok(found);
    }

    @Override
    public ResponseEntity<List<AccountAddress>> findAddressesByAccountId(UUID accountId, Optional<Integer> size, Optional<Integer> page, Optional<List<String>> sort) {
        Pageable pageable = AppPageRequest.of(page.orElse(0), size.orElse(20), sort.orElse(List.of()));
        var result = addressService.findAllByAccountId(accountId, pageable);
        return ResponseEntity.ok(result.getContent());
    }

    @Override
    public ResponseEntity<AccountAddress> findLastAddressByAccountId(UUID accountId) {
        var found = addressService.firstByAccountId(accountId);
        return ResponseEntity.ok(found);
    }

    @Override
    public ResponseEntity<AccountAddress> updateAddress(UUID accountId, AccountAddress accountAddress) {
        var found = addressService.updateAddress(accountId, accountAddress);
        return ResponseEntity.ok(found);
    }

}
