package com.ssegning.ntravl.server.api;

import com.ssegning.ntravl.server.model.Document;
import com.ssegning.ntravl.server.model.UploadedResponse;
import com.ssegning.ntravl.server.service.DocumentService;
import com.ssegning.ntravl.server.service.DownloadService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
public class DocumentApiController implements DocumentApi {

    private final DocumentService documentService;
    private final DownloadService downloadService;

    @GetMapping("api/f/media/{name}")
    @ResponseBody
    public ResponseEntity<byte[]> getFile(@PathVariable String name) {
        String path = "media/" + name;

        var doc = downloadService.getByPath(path);

        var mHeaders = new LinkedMultiValueMap<String, String>();
        doc.getHeaders().forEach((n, v) -> mHeaders.put(n, Collections.singletonList(v)));
        doc.getMetaData().forEach((n, v) -> mHeaders.put("x-amz-meta-" + n, Collections.singletonList(v)));

        return new ResponseEntity<>(doc.getBytes(), mHeaders, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Document> getOnDocument(UUID documentId) {
        var result = documentService.getById(documentId);
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<UploadedResponse> uploadOne(MultipartFile file) {
        var cleanFileName = Objects.requireNonNull(file.getOriginalFilename()).replaceAll("[^0-9-.a-zA-Z]", "-");
        var filePath = String.format("media/%s-%s", Instant.now().getEpochSecond(), cleanFileName);

        String contentType = Objects
                .requireNonNullElse(file.getContentType(), MediaType.APPLICATION_OCTET_STREAM)
                .toString();

        var headers = new HashMap<String, String>();
        headers.put(HttpHeaders.CONTENT_TYPE, contentType);
        headers.put(HttpHeaders.CONTENT_DISPOSITION, String.format("inline; filename=\"%s\"", cleanFileName));
        headers.put(HttpHeaders.CACHE_CONTROL, "max-age=31536000");

        var userMetadata = new HashMap<String, String>();
        userMetadata.put("ntravl-app", "killer");
        var key = UUID.randomUUID();
        userMetadata.put("key", key.toString());

        UploadedResponse result;
        try {
            result = documentService.uploadFile(file.getBytes(), filePath, contentType, headers, userMetadata).key(key);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return ResponseEntity.ok(result);
    }

}
