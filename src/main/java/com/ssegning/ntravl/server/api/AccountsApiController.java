package com.ssegning.ntravl.server.api;


import com.ssegning.ntravl.server.model.Account;
import com.ssegning.ntravl.server.model.ChangePersonalDataRequest;
import com.ssegning.ntravl.server.model.CreateAccountInput;
import com.ssegning.ntravl.server.service.AccountService;
import com.ssegning.ntravl.server.util.AppPageRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
public class AccountsApiController implements AccountsApi {

    private final AccountService accountService;

    @Override
    public ResponseEntity<Account> createAccount(String acceptLanguage, CreateAccountInput createAccountInput) {
        var result = accountService.createAccount(createAccountInput, Locale.forLanguageTag(acceptLanguage));
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<Account> getAccountByIdentifier(String id) {
        var result = accountService.findByIdentifier(id);
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<List<Account>> getAccounts(Optional<String> query, Optional<Integer> size, Optional<Integer> page, Optional<List<String>> sort) {
        Pageable pageable = AppPageRequest.of(page.orElse(0), size.orElse(20), sort.orElse(List.of()));
        var result = accountService.getAccounts(pageable);
        return ResponseEntity.ok(result.getContent());
    }

    @Override
    public ResponseEntity<Account> updatePersonalData(UUID id, ChangePersonalDataRequest changePersonalDataRequest) {
        var result = accountService.updatePersonalData(id, changePersonalDataRequest);
        return ResponseEntity.ok(result);
    }

}
