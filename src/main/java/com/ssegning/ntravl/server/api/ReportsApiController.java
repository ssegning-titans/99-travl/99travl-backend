package com.ssegning.ntravl.server.api;

import com.ssegning.ntravl.server.model.*;
import com.ssegning.ntravl.server.service.ReportService;
import com.ssegning.ntravl.server.util.AppPageRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
public class ReportsApiController implements ReportsApi {

    private final ReportService reportService;

    @Override
    public ResponseEntity<List<ReportBug>> getAllBugReports(Optional<UUID> accountId, Optional<Integer> size, Optional<Integer> page, Optional<List<String>> sort) {
        Pageable pageable = AppPageRequest.of(page.orElse(0), size.orElse(20), sort.orElse(List.of()));
        var result = reportService.getAllBugReports(accountId.orElse(null), pageable);
        return ResponseEntity.ok(result.getContent());
    }

    @Override
    public ResponseEntity<List<ReportAccount>> getAllReportForAccount(UUID accountId, Optional<Integer> size, Optional<Integer> page, Optional<List<String>> sort) {
        Pageable pageable = AppPageRequest.of(page.orElse(0), size.orElse(20), sort.orElse(List.of()));
        var result = reportService.getAllReportForAccount(accountId, pageable);
        return ResponseEntity.ok(result.getContent());
    }

    @Override
    public ResponseEntity<List<ReportTravel>> getAllReportForTravel(UUID travelId, Optional<Integer> size, Optional<Integer> page, Optional<List<String>> sort) {
        Pageable pageable = AppPageRequest.of(page.orElse(0), size.orElse(20), sort.orElse(List.of()));
        var result = reportService.getAllReportForTravel(travelId, pageable);
        return ResponseEntity.ok(result.getContent());
    }

    @Override
    public ResponseEntity<List<BaseReport>> getAllReports(ReportStatus status, Optional<Integer> size, Optional<Integer> page, Optional<List<String>> sort) {
        Pageable pageable = AppPageRequest.of(page.orElse(0), size.orElse(20), sort.orElse(List.of()));
        var result = reportService.getAllReports(status, pageable);
        return ResponseEntity.ok(result.getContent());
    }

    @Override
    public ResponseEntity<List<BaseReport>> getOwnersReports(UUID ownerId, ReportStatus status, Optional<Integer> size, Optional<Integer> page, Optional<List<String>> sort) {
        Pageable pageable = AppPageRequest.of(page.orElse(0), size.orElse(20), sort.orElse(List.of()));
        var result = reportService.getOwnerReports(ownerId, status, pageable);
        return ResponseEntity.ok(result.getContent());
    }

    @Override
    public ResponseEntity<BaseReport> getReportById(UUID reportId) {
        var result = reportService.findOneById(reportId);
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<ReportAccount> reportAccount(CreateReportAccount createReportAccount) {
        var result = reportService.createAccountReport(createReportAccount);
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<ReportBug> reportBug(CreateReportBug createReportBug, Optional<UUID> accountId) {
        var result = reportService.createBugReport(createReportBug, accountId.orElse(null));
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<ReportTravel> reportTravel(CreateReportTravel createReportTravel) {
        var result = reportService.createTravelReport(createReportTravel);
        return ResponseEntity.ok(result);
    }

}
