package com.ssegning.ntravl.server.api;


import com.ssegning.ntravl.server.model.*;
import com.ssegning.ntravl.server.service.AccountService;
import com.ssegning.ntravl.server.service.FriendService;
import com.ssegning.ntravl.server.util.AppPageRequest;
import com.ssegning.ntravl.server.util.AuthUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
public class FriendsApiController implements FriendsApi {

    private final FriendService friendService;
    private final AccountService accountService;

    @Override
    public ResponseEntity<OfflineFriend> createOfflineFriend(RequestOfflineFriendShip requestOfflineFriendShip) {
        var account = AuthUtils.getActualUserId(accountService);
        var result = friendService.createOfflineFriend(requestOfflineFriendShip, account);
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<OnlineFriend> createOnlineFriend(RequestOnlineFriendShip requestOnlineFriendShip) {
        var account = AuthUtils.getActualUserId(accountService);
        var result = friendService.createOnlineFriend(requestOnlineFriendShip, account);
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<Void> deleteFriendById(UUID friendId) {
        var account = AuthUtils.getActualUserId(accountService);
        friendService.deleteFriendById(friendId, account);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<BaseFriend> getFriendById(UUID friendId) {
        var account = AuthUtils.getActualUserId(accountService);
        var result = friendService.getFriendById(friendId, account);
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<List<BaseFriend>> getFriends(Optional<FriendType> type, Optional<FriendShipStatus> status, Optional<Integer> size, Optional<Integer> page, Optional<List<String>> sort) {
        Pageable pageable = AppPageRequest.of(page.orElse(0), size.orElse(20), sort.orElse(List.of()));
        var account = AuthUtils.getActualUserId(accountService);
        var result = friendService.getFriends(account.getId(), type.orElse(null), status.orElse(null), pageable);
        return ResponseEntity.ok(result.getContent());
    }

    @Override
    public ResponseEntity<BaseFriend> updateFriend(UUID friendId, UpdateFriend updateFriend) {
        var account = AuthUtils.getActualUserId(accountService);
        var result = friendService.updateFriend(friendId, account.getId(), updateFriend);
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<BaseFriend> updateFriendStatus(UUID friendId, UpdateFriendStatus updateFriendStatus) {
        var account = AuthUtils.getActualUserId(accountService);
        var result = friendService.updateFriendStatus(friendId, account.getId(), updateFriendStatus);
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<BaseFriend> validateFriendShip(UUID friendId, ValidateFriendShipCode validateFriendShipCode) {
        var account = AuthUtils.getActualUserId(accountService);
        var result = friendService.validateFriendShip(friendId, account.getId(), validateFriendShipCode);
        return ResponseEntity.ok(result);
    }
}
