package com.ssegning.ntravl.server.api;

import com.ssegning.ntravl.server.model.*;
import com.ssegning.ntravl.server.service.ChatMessageService;
import com.ssegning.ntravl.server.util.AppPageRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
public class ChatMessagesApiController implements ChatMessagesApi {

    private final ChatMessageService chatMessageService;

    @Override
    public ResponseEntity<BaseChatMessage> createContactChatMessage(ContactChatMessage message) {
        var result = chatMessageService.addMessage(message);
        return ResponseEntity.status(201).body(result);
    }

    @Override
    public ResponseEntity<BaseChatMessage> createMediaChatMessage(MediaChatMessage message) {
        var result = chatMessageService.addMessage(message);
        return ResponseEntity.status(201).body(result);
    }

    @Override
    public ResponseEntity<BaseChatMessage> createPropositionChatMessage(PropositionChatMessage message) {
        var result = chatMessageService.addMessage(message);
        return ResponseEntity.status(201).body(result);
    }

    @Override
    public ResponseEntity<BaseChatMessage> createTextChatMessage(TextChatMessage message) {
        var result = chatMessageService.addMessage(message);
        return ResponseEntity.status(201).body(result);
    }

    @Override
    public ResponseEntity<Void> deleteOneChatMessages(UUID messageId) {
        chatMessageService.deleteById(messageId);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<List<BaseChatMessage>> getAllChatMessages(UUID chatId, Optional<Integer> size, Optional<Integer> page, Optional<List<String>> sort) {
        Pageable pageable = AppPageRequest.of(page.orElse(0), size.orElse(20), sort.orElse(List.of()));
        var result = chatMessageService.getChatMessages(chatId, pageable);
        return ResponseEntity.ok(result.getContent());
    }

    @Override
    public ResponseEntity<BaseChatMessage> getOneChatMessage(UUID messageId) {
        var result = chatMessageService.findOneById(messageId);
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<Void> markMessageAsRead(UUID messageId) {
        chatMessageService.markAsRead(messageId);
        return ResponseEntity.noContent().build();
    }

}
