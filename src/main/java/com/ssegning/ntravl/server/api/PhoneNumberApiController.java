package com.ssegning.ntravl.server.api;

import com.ssegning.ntravl.server.model.PhoneTanRequest;
import com.ssegning.ntravl.server.model.PhoneTanResponse;
import com.ssegning.ntravl.server.model.PhoneTanStatus;
import com.ssegning.ntravl.server.model.PhoneTanValidateRequest;
import com.ssegning.ntravl.server.service.AccountService;
import com.ssegning.ntravl.server.service.PhoneNumberService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
public class PhoneNumberApiController implements PhoneNumberApi {

    private final AccountService accountService;
    private final PhoneNumberService phoneNumberService;

    @Override
    public ResponseEntity<PhoneTanResponse> getTan(UUID id, PhoneTanRequest phoneTanRequest) {
        var account = accountService.findById(id);
        var result = phoneNumberService.createPhoneNumberTan(account, phoneTanRequest.getPhoneNumber());
        return ResponseEntity.ok(new PhoneTanResponse().secret(result));
    }

    @Override
    public ResponseEntity<PhoneTanStatus> validateTan(UUID id, PhoneTanValidateRequest input) {
        var account = accountService.findById(id);
        var result = phoneNumberService.validatePhoneNumberTan(account, input.getPhoneNumber(), input.getCode(), input.getSecret());
        return ResponseEntity.ok(new PhoneTanStatus().status(result));
    }

}
