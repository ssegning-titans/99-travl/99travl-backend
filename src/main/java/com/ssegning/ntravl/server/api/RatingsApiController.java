package com.ssegning.ntravl.server.api;

import com.ssegning.ntravl.server.model.*;
import com.ssegning.ntravl.server.service.BaseRatingService;
import com.ssegning.ntravl.server.util.AppPageRequest;
import com.ssegning.ntravl.server.util.AuthUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
public class RatingsApiController implements RatingsApi {
    private final BaseRatingService baseRatingService;

    @Override
    public ResponseEntity<BaseRating> createPersonRating(RatingAccount rating) {
        var result = baseRatingService.createRating(rating);
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<BaseRating> createTravelRating(RatingTravel rating) {
        var result = baseRatingService.createRating(rating);
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<Void> deletePersonRating(UUID id) {
        var accountId = AuthUtils.getActualUserId();
        baseRatingService.deletePersonRatingById(accountId, id);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<Void> deleteTravelRating(UUID id) {
        var accountId = AuthUtils.getActualUserId();
        baseRatingService.deleteTravelRatingById(accountId, id);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<AverageRatingPerson> getAveragePersonRating(UUID personId) {
        var result = baseRatingService.getAveragePersonRating(personId);
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<AverageRatingTravel> getAverageTravelRating(UUID travelId) {
        var result = baseRatingService.getAverageTravelRating(travelId);
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<RatingAccount> getCurrentAccountPersonRating(UUID personId) {
        var accountId = AuthUtils.getActualUserId();
        var result = baseRatingService.getCurrentAccountPersonRating(accountId, personId);
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<RatingTravel> getCurrentAccountTravelRating(UUID travelId) {
        var accountId = AuthUtils.getActualUserId();
        var result = baseRatingService.getCurrentAccountTravelRating(accountId, travelId);
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<List<BaseRating>> getOwnerRatings(UUID accountId, Optional<Integer> size, Optional<Integer> page, Optional<List<String>> sort) {
        Pageable pageable = AppPageRequest.of(page.orElse(0), size.orElse(20), sort.orElse(List.of()));
        var result = baseRatingService.findAllByAuthor(accountId, pageable);
        return ResponseEntity.ok(result.getContent());
    }

    @Override
    public ResponseEntity<List<RatingAccount>> listPersonRating(UUID personId, Optional<Integer> size, Optional<Integer> page, Optional<List<String>> sort) {
        Pageable pageable = AppPageRequest.of(page.orElse(0), size.orElse(20), sort.orElse(List.of()));
        var result = baseRatingService.getAllPersonRatings(personId, pageable);
        return ResponseEntity.ok(result.getContent());
    }

    @Override
    public ResponseEntity<List<RatingTravel>> listTravelRating(UUID travelId, Optional<Integer> size, Optional<Integer> page, Optional<List<String>> sort) {
        Pageable pageable = AppPageRequest.of(page.orElse(0), size.orElse(20), sort.orElse(List.of()));
        var result = baseRatingService.getAllTravelRatings(travelId, pageable);
        return ResponseEntity.ok(result.getContent());
    }

}
