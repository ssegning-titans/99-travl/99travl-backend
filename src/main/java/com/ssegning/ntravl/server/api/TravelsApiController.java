package com.ssegning.ntravl.server.api;

import com.ssegning.ntravl.server.model.*;
import com.ssegning.ntravl.server.service.AccountService;
import com.ssegning.ntravl.server.service.TravelService;
import com.ssegning.ntravl.server.util.AppPageRequest;
import com.ssegning.ntravl.server.util.AuthUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
public class TravelsApiController implements TravelsApi {

    private final TravelService travelService;
    private final AccountService accountService;

    @Override
    public ResponseEntity<Travel> createTravel(CreateTravel createTravel) {
        var accountId = AuthUtils.getActualUserId();
        var result = travelService.addTravel(createTravel, accountId);
        return ResponseEntity.status(201).body(result);
    }

    @Override
    public ResponseEntity<Void> deleteTravelById(UUID travelId) {
        travelService.deleteTravel(travelId);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<List<Travel>> getAccountTravels(UUID accountId, Optional<Integer> size, Optional<Integer> page, Optional<List<String>> sort) {
        Pageable pageable = AppPageRequest.of(page.orElse(0), size.orElse(20), sort.orElse(List.of()));
        var result = travelService.accountTravels(accountId, pageable);
        return ResponseEntity.ok(result.getContent());
    }

    @Override
    public ResponseEntity<List<TravelPoint>> getRandomTravel(List<Double> coord, Double radius, Optional<Integer> size, Optional<Integer> page, Optional<List<String>> sort) {
        var point = new LatLng().lat(coord.get(0)).lng(coord.get(1));
        var result = travelService.findRandomlyNearPoint(point, radius, AppPageRequest.of(page.orElse(0), size.orElse(20), sort.orElse(List.of())));
        return ResponseEntity.ok(result.getContent());
    }

    @Override
    public ResponseEntity<Travel> getTravelById(UUID travelId) {
        var result = travelService.getById(travelId);
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<List<SearchTravel>> searchTravel(List<Double> startCoord, List<Double> endCoord, Double startRadius, Double endRadius, Long startDate, Long endDate, Optional<Integer> size, Optional<Integer> page, Optional<List<String>> sort) {
        var start = new LatLng().lat(startCoord.get(0)).lng(startCoord.get(1));
        var end = new LatLng().lat(endCoord.get(0)).lng(endCoord.get(1));
        var result = travelService.findByStartAndEnd(start, end, startRadius, endRadius, startDate, endDate, AppPageRequest.of(page.orElse(0), size.orElse(20), sort.orElse(List.of())));
        var searchTravels = result.map(points -> new SearchTravel().points(points));
        return ResponseEntity.ok(searchTravels.getContent());
    }

    @Override
    public ResponseEntity<Travel> updateTravelById(Travel travel) {
        var result = travelService.updateTravel(travel);
        return ResponseEntity.ok(result);
    }

}
