package com.ssegning.ntravl.server.api;

import com.ssegning.ntravl.server.model.TravelProposition;
import com.ssegning.ntravl.server.service.PropositionsService;
import com.ssegning.ntravl.server.util.AppPageRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
public class PropositionsApiController implements PropositionsApi {

    private final PropositionsService propositionsService;

    @Override
    public ResponseEntity<TravelProposition> createChatAndTravelProposition(UUID travelId, TravelProposition travelProposition) {
        var result = propositionsService.create(travelProposition);
        return ResponseEntity.status(200).body(result);
    }

    @Override
    public ResponseEntity<TravelProposition> getOneTravelProposition(UUID propositionId) {
        var result = propositionsService.getOne(propositionId);
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<List<TravelProposition>> getTravelPropositions(UUID travelId, Optional<Integer> size, Optional<Integer> page, Optional<List<String>> sort) {
        Pageable pageable = AppPageRequest.of(page.orElse(0), size.orElse(20), sort.orElse(List.of()));
        var result = propositionsService.getPropositions(travelId, pageable);
        return ResponseEntity.ok(result.getContent());
    }

    @Override
    public ResponseEntity<TravelProposition> updateTravelProposition(UUID propositionId, TravelProposition travelProposition) {
        var result = propositionsService.update(propositionId, travelProposition);
        return ResponseEntity.ok(result);
    }
}
