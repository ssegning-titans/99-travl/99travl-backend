package com.ssegning.ntravl.server.api;


import com.ssegning.ntravl.server.model.*;
import com.ssegning.ntravl.server.service.AccountService;
import com.ssegning.ntravl.server.service.PasswordService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
public class CredentialsApiController implements CredentialsApi {

    private final PasswordService passwordService;
    private final AccountService accountService;

    @Override
    public ResponseEntity<CreateCredentialResponse> createCredential(UUID id, CredentialType type, CreateCredentialInput createCredentialInput) {
        var account = accountService.findById(id);
        passwordService.createUserPassword(createCredentialInput.getChallenge(), account);
        return ResponseEntity.ok(new CreateCredentialResponse().state(true));
    }

    @Override
    public ResponseEntity<DisableCredentialResponse> disableCredential(UUID id, CredentialType type, Map<String, String> requestBody) {
        var account = accountService.findById(id);
        passwordService.deactivatePreviousUserPassword(account);
        return ResponseEntity.ok(new DisableCredentialResponse().state(true));
    }

    @Override
    public ResponseEntity<HasAccountPassword> hasCredential(UUID id, CredentialType type) {
        var account = accountService.findById(id);
        var result = passwordService.hasPassword(account);
        return ResponseEntity.ok(new HasAccountPassword().state(result));
    }

    @Override
    public ResponseEntity<ValidatePasswordResponse> validateCredential(UUID id, CredentialType type, ValidatePasswordInput validatePasswordInput) {
        var account = accountService.findById(id);
        var result = passwordService.validatePassword(account, validatePasswordInput.getChallenge());
        return ResponseEntity.ok(new ValidatePasswordResponse().valid(result));
    }

}
