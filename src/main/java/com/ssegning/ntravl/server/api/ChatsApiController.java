package com.ssegning.ntravl.server.api;

import com.ssegning.ntravl.server.model.Chat;
import com.ssegning.ntravl.server.model.ChatStatus;
import com.ssegning.ntravl.server.service.ChatService;
import com.ssegning.ntravl.server.util.AppPageRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
public class ChatsApiController implements ChatsApi {

    private final ChatService chatService;

    @Override
    public ResponseEntity<Void> archiveChat(UUID chatId) {
        chatService.markAsArchived(chatId);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<Void> deleteChat(UUID chatId) {
        chatService.deleteOne(chatId);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<List<Chat>> getAllChats(UUID accountId, ChatStatus status, Optional<Integer> size, Optional<Integer> page, Optional<List<String>> sort) {
        Pageable pageable = AppPageRequest.of(page.orElse(0), size.orElse(20), sort.orElse(List.of()));
        var result = chatService.getChatsByAccountId(accountId, status, pageable);
        return ResponseEntity.ok(result.getContent());
    }

    @Override
    public ResponseEntity<Chat> getChat(UUID chatId) {
        var result = chatService.findById(chatId);
        return ResponseEntity.ok(result);
    }

}
