package com.ssegning.ntravl.server.api;


import com.google.maps.model.LatLng;
import com.ssegning.ntravl.server.model.MapPlace;
import com.ssegning.ntravl.server.model.MapsSession;
import com.ssegning.ntravl.server.model.RoutingDirection;
import com.ssegning.ntravl.server.model.RoutingMapPlace;
import com.ssegning.ntravl.server.service.MapsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.CacheControl;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Slf4j
@RestController
@RequiredArgsConstructor
public class MapsApiController implements MapsApi {

    private final MapsService mapsService;

    @Override
    public ResponseEntity<List<MapPlace>> autocomplete(String formattedAddress, String acceptLanguage, UUID sessionToken) {
        var results = mapsService.autocomplete(formattedAddress, acceptLanguage, sessionToken);
        return ResponseEntity.ok(results);
    }

    @Override
    public ResponseEntity<List<MapPlace>> findPlaces(String formattedAddress, String acceptLanguage) {
        var results = mapsService.getPlacesWithFormattedAddressesLike(formattedAddress, acceptLanguage);
        return ResponseEntity.ok(results);
    }

    @Override
    public ResponseEntity<MapPlace> getAddressByCoordinate(Double latitude, Double longitude, String acceptLanguage) {
        var results = mapsService.findAddressByCoordinate(latitude, longitude, acceptLanguage);
        return ResponseEntity.ok(results);
    }

    @Override
    public ResponseEntity<MapPlace> getAddressByPlaceId(String placeId, String acceptLanguage) {
        var results = mapsService.findAddressByPlaceId(placeId, acceptLanguage);
        return ResponseEntity.ok(results);
    }

    @Override
    public ResponseEntity<List<RoutingDirection>> routing(List<Double> startCoord, List<Double> endCoord, String acceptLanguage) {
        var result = mapsService.routing(acceptLanguage, new LatLng(startCoord.get(0), startCoord.get(1)), new LatLng(endCoord.get(0), endCoord.get(1)));
        var direction = new RoutingDirection();
        for (int i = 0; i < result.size(); i++) {
            var item = result.get(i);
            direction.addRoutesItem(new RoutingMapPlace()
                    .placeId(item.getPlaceId())
                    .latitude(item.getLatitude())
                    .longitude(item.getLongitude())
                    .formattedAddress(item.getFormattedAddress())
                    .addressComponent(item.getAddressComponent())
                    .order((double) i)
            );
        }
        return ResponseEntity.ok()
                .cacheControl(CacheControl.maxAge(1, TimeUnit.DAYS))
                .contentType(MediaType.APPLICATION_JSON)
                .body(List.of(direction));

    }

    @Override
    public ResponseEntity<MapsSession> sessionToken() {
        var uuid = UUID.randomUUID();
        return ResponseEntity.ok()
                .cacheControl(CacheControl.maxAge(1, TimeUnit.DAYS))
                .contentType(MediaType.APPLICATION_JSON)
                .body(new MapsSession().sessionToken(uuid));
    }

}
