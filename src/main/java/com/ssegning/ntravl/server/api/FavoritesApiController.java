package com.ssegning.ntravl.server.api;

import com.ssegning.ntravl.server.model.BaseFavorite;
import com.ssegning.ntravl.server.model.FavoriteAccount;
import com.ssegning.ntravl.server.model.FavoriteTravel;
import com.ssegning.ntravl.server.service.FavoriteService;
import com.ssegning.ntravl.server.util.AppPageRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
public class FavoritesApiController implements FavoritesApi {

    private final FavoriteService favoriteService;

    @Override
    public ResponseEntity<FavoriteAccount> createFavoriteAccount(UUID accountId, UUID personId) {
        var result = favoriteService.createFavoriteAccount(accountId, personId);
        return ResponseEntity.status(201).body(result);
    }

    @Override
    public ResponseEntity<FavoriteTravel> createTravelFavorite(UUID accountId, UUID travelId) {
        var result = favoriteService.createFavoriteTravel(accountId, travelId);
        return ResponseEntity.status(201).body(result);
    }

    @Override
    public ResponseEntity<Void> deleteFavoriteAccount(UUID accountId, UUID personId) {
        favoriteService.deleteFavoriteAccount(accountId, personId);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<Void> deleteTravelFavorite(UUID accountId, UUID travelId) {
        favoriteService.deleteFavoriteTravel(accountId, travelId);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<FavoriteAccount> getFavoriteAccount(UUID accountId, UUID personId) {
        var result = favoriteService.getFavoriteAccount(accountId, personId);
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<List<BaseFavorite>> getOwnerFavorites(UUID accountId, Optional<Integer> size, Optional<Integer> page, Optional<List<String>> sort) {
        Pageable pageable = AppPageRequest.of(page.orElse(0), size.orElse(20), sort.orElse(List.of()));
        var result = favoriteService.getAllByAccountId(accountId, pageable);
        return ResponseEntity.ok(result.getContent());
    }

    @Override
    public ResponseEntity<FavoriteTravel> getTravelFavorite(UUID accountId, UUID travelId) {
        var result = favoriteService.getFavoriteTravel(accountId, travelId);
        return ResponseEntity.ok(result);
    }

}
