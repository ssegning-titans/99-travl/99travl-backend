package com.ssegning.ntravl.server.api;

import com.ssegning.ntravl.server.model.TravelReservation;
import com.ssegning.ntravl.server.model.TravelReservationCount;
import com.ssegning.ntravl.server.service.ReservationService;
import com.ssegning.ntravl.server.service.TravelService;
import com.ssegning.ntravl.server.util.AppPageRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
public class ReservationsApiController implements ReservationsApi {

    private final ReservationService service;
    private final TravelService travelService;

    @Override
    public ResponseEntity<TravelReservationCount> countTravelReservation(UUID travelId) {
        var result = service.countByTravel(travelId);
        return ResponseEntity.ok(new TravelReservationCount().count(result));
    }

    @Override
    public ResponseEntity<TravelReservation> createTravelReservation(UUID travelId, TravelReservation travelReservation) {
        var travel = travelService.getById(travelId);
        var result = service.create(travel, travelReservation);
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<Void> deleteReservationById(UUID travelId, UUID reservationId) {
        var travel = travelService.getById(travelId);
        service.deleteById(travel, reservationId);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<TravelReservation> getReservationById(UUID travelId, UUID reservationId) {
        var result = service.findById(travelId, reservationId);
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<List<TravelReservation>> getReservationForTravel(UUID travelId, Optional<Integer> size, Optional<Integer> page, Optional<List<String>> sort) {
        Pageable pageable = AppPageRequest.of(page.orElse(0), size.orElse(20), sort.orElse(List.of()));
        var travel = travelService.getById(travelId);
        var result = service.getReservation(travel, pageable);
        return ResponseEntity.ok(result.getContent());
    }

    @Override
    public ResponseEntity<TravelReservation> updateReservationById(UUID travelId, TravelReservation reservation) {
        var travel = travelService.getById(travelId);
        var result = service.update(travel, reservation);
        return ResponseEntity.ok(result);
    }
}
