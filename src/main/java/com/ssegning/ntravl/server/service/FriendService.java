package com.ssegning.ntravl.server.service;

import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import com.ssegning.ntravl.server.domain.friend.BaseFriendEntity;
import com.ssegning.ntravl.server.domain.friend.OfflineFriendShipEntity;
import com.ssegning.ntravl.server.domain.friend.OfflineFriendShipRequestEntity;
import com.ssegning.ntravl.server.domain.friend.OnlineFriendShipEntity;
import com.ssegning.ntravl.server.exceptions.AuthException;
import com.ssegning.ntravl.server.exceptions.BadRequestException;
import com.ssegning.ntravl.server.exceptions.ForbiddenRequestException;
import com.ssegning.ntravl.server.exceptions.NotFoundException;
import com.ssegning.ntravl.server.mapper.friend.FriendMapper;
import com.ssegning.ntravl.server.model.*;
import com.ssegning.ntravl.server.repo.friend.BaseFriendRepo;
import com.ssegning.ntravl.server.repo.friend.OfflineFriendShipRequestRepo;
import com.ssegning.ntravl.server.types.FriendShipRequestStatus;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashSet;
import java.util.UUID;

@Slf4j
@Service
@AllArgsConstructor
public class FriendService {

    private final BaseFriendRepo baseFriendRepo;
    private final OfflineFriendShipRequestRepo offlineFriendShipRequestRepo;

    private final FriendMapper friendMapper;
    private final AccountService accountService;

    private final SmsService smsService;

    @Transactional(rollbackOn = {IOException.class, RuntimeException.class})
    public OfflineFriend createOfflineFriend(RequestOfflineFriendShip input, Account currentAccount) {
        var member = new HashSet<AccountEntity>();
        member.add(new AccountEntity(currentAccount.getId()));

        var toCreate = new OfflineFriendShipEntity();
        toCreate.setMembers(member);
        toCreate.setPhoneNumber(input.getPhoneNumber());
        toCreate.setDescription(input.getDescription());
        toCreate.setFirstName(input.getFirstName());
        toCreate.setLastName(input.getLastName());
        try {
            toCreate.setAvatar(input.getAvatar() != null ? input.getAvatar().toURL() : null);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }

        var request = new OfflineFriendShipRequestEntity();
        request.setOwner(toCreate);
        request.setCode(CryptoService.generateCode(6));

        toCreate.setRequest(request);
        var saved = baseFriendRepo.save(toCreate);

        smsService.sendSms(input.getPhoneNumber(), "friend.tan", currentAccount.getLocale(), currentAccount.getFirstName(), request.getCode());

        return friendMapper.entityToModel(saved);
    }

    public OnlineFriend createOnlineFriend(RequestOnlineFriendShip input, Account currentAccount) {
        final AccountEntity currentAccountEntity = new AccountEntity(currentAccount.getId());

        var otherAccount = accountService.findById(input.getAccountId());
        var member = new HashSet<AccountEntity>();
        member.add(currentAccountEntity);
        member.add(new AccountEntity(otherAccount.getId()));

        var toCreate = new OnlineFriendShipEntity();
        toCreate.setMembers(member);
        toCreate.setAuthor(currentAccountEntity);

        var saved = baseFriendRepo.save(toCreate);
        return friendMapper.entityToModel(saved);
    }

    public void deleteFriendById(UUID friendId, Account currentAccount) {
        var found = findById(friendId, currentAccount.getId());
        baseFriendRepo.delete(found);
    }

    public BaseFriend getFriendById(UUID friendId, Account currentAccount) {
        var found = findById(friendId, currentAccount.getId());
        return friendMapper.entityToModel(found);
    }

    public Page<BaseFriend> getFriends(UUID currentAccountId, FriendType type, FriendShipStatus status, Pageable pageable) {
        Page<BaseFriendEntity> page;
        if (type != null && status != null) {
            page = baseFriendRepo.findAllByMembers_IdAndTypeAndStatus(currentAccountId, type, status, pageable);
        } else if (type != null) {
            page = baseFriendRepo.findAllByMembers_IdAndType(currentAccountId, type, pageable);
        } else if (status != null) {
            page = baseFriendRepo.findAllByMembers_IdAndStatus(currentAccountId, status, pageable);
        } else {
            page = baseFriendRepo.findAllByMembers_Id(currentAccountId, pageable);
        }
        return page.map(friendMapper::entityToModel);
    }

    public BaseFriend updateFriend(UUID friendId, UUID currentAccountId, UpdateFriend input) {
        var found = findById(friendId, currentAccountId);
        found.setType(input.getType());
        var saved = baseFriendRepo.save(found);
        return friendMapper.entityToModel(saved);
    }

    public BaseFriend updateFriendStatus(UUID friendId, UUID currentAccountId, UpdateFriendStatus input) {
        var found = findById(friendId, currentAccountId);
        found.setStatus(input.getStatus());
        var saved = baseFriendRepo.save(found);
        return friendMapper.entityToModel(saved);
    }

    public BaseFriend validateFriendShip(UUID friendId, UUID currentAccountId, ValidateFriendShipCode input) {
        var found = findById(friendId, currentAccountId);
        if (!(found instanceof OfflineFriendShipEntity)) {
            throw new BadRequestException("Cannot valid this type of friendship");
        }

        var offlineFriendEntity = (OfflineFriendShipEntity) found;
        var request = offlineFriendEntity.getRequest();
        if (!input.getCode().equals(request.getCode())) {
            throw new ForbiddenRequestException("Wrong code provided");
        }
        request.setStatus(FriendShipRequestStatus.ACCEPTED);
        found.setStatus(FriendShipStatus.ACCEPTED);

        offlineFriendShipRequestRepo.save(request);
        var saved = baseFriendRepo.save(found);

        return friendMapper.entityToModel(saved);
    }

    private BaseFriendEntity findById(UUID id, UUID memberId) {
        var found = baseFriendRepo.findById(id);
        if (found.isEmpty()) {
            throw new NotFoundException("Friend not found");
        }
        var got = found.get();
        if (got.getMembers().stream().noneMatch(i -> i.getId().equals(memberId))) {
            throw new AuthException("not authorized to get this");
        }
        return found.get();
    }
}
