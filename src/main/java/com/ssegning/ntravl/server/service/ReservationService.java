package com.ssegning.ntravl.server.service;

import com.ssegning.ntravl.server.exceptions.NotFoundException;
import com.ssegning.ntravl.server.mapper.travel.TravelReservationMapper;
import com.ssegning.ntravl.server.model.Travel;
import com.ssegning.ntravl.server.model.TravelReservation;
import com.ssegning.ntravl.server.repo.travel.ReservationRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ReservationService {

    private final ReservationRepo repo;
    private final TravelReservationMapper mapper;

    public long countByTravel(UUID travelId) {
        return repo.countAllByTravelId(travelId);
    }

    public TravelReservation findById(UUID travelId, UUID id) {
        return repo
                .findByTravelIdAndId(travelId, id)
                .map(mapper::entityToModel)
                .orElseThrow(NotFoundException::new);
    }

    public TravelReservation create(Travel travel, TravelReservation travelReservation) {
        travelReservation.setTravelId(travel.getId());
        var toSave = mapper.toEntity(travelReservation);
        var saved = repo.save(toSave);
        return mapper.entityToModel(saved);
    }

    public TravelReservation update(Travel travel, TravelReservation reservation) {
        var found = findById(travel.getId(), reservation.getId());
        reservation.setTravelId(travel.getId());
        reservation.setId(found.getId());

        var toSave = mapper.toEntity(reservation);
        var saved = repo.save(toSave);
        return mapper.entityToModel(saved);
    }

    public Page<TravelReservation> getReservation(Travel travel, Pageable pageable) {
        return repo
                .findAllByTravelId(travel.getId(), pageable)
                .map(mapper::entityToModel);
    }

    public void deleteById(Travel travel, UUID id) {
        var found = findById(travel.getId(), id);
        repo.deleteById(found.getId());
    }
}
