package com.ssegning.ntravl.server.service;

import com.ssegning.ntravl.server.domain.rating.RatingAccountEntity;
import com.ssegning.ntravl.server.domain.rating.RatingTravelEntity;
import com.ssegning.ntravl.server.mapper.rating.RatingAccountMapper;
import com.ssegning.ntravl.server.mapper.rating.RatingMapper;
import com.ssegning.ntravl.server.mapper.rating.RatingTravelMapper;
import com.ssegning.ntravl.server.model.*;
import com.ssegning.ntravl.server.repo.rating.AccountRatingRepo;
import com.ssegning.ntravl.server.repo.rating.BaseRatingRepo;
import com.ssegning.ntravl.server.repo.rating.TravelRatingRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BaseRatingService {

    private final BaseRatingRepo baseRatingRepo;
    private final AccountRatingRepo accountRatingRepo;
    private final TravelRatingRepo travelRatingRepo;

    private final RatingMapper ratingMapper;
    private final RatingAccountMapper ratingAccountMapper;
    private final RatingTravelMapper ratingTravelMapper;

    public Page<BaseRating> findAllByAuthor(UUID accountId, Pageable pageable) {
        var page = baseRatingRepo.findAllByAuthorId(accountId, pageable);
        return page.map(ratingMapper::entityToModel);
    }

    public BaseRating createRating(BaseRating entity) {
        var toSave = ratingMapper.toEntity(entity);
        var saved = baseRatingRepo.save(toSave);
        return ratingMapper.entityToModel(saved);
    }

    private RatingAccountEntity findByPersonIdAndAuthorId(UUID person_id, UUID author_id) {
        return accountRatingRepo.findByPersonIdAndAuthorId(person_id, author_id).orElseThrow();
    }

    private RatingTravelEntity findByTravelIdAndAuthorId(UUID travel_id, UUID author_id) {
        return travelRatingRepo.findByTravelIdAndAuthorId(travel_id, author_id).orElseThrow();
    }

    public void deletePersonRatingById(UUID author, UUID personId) {
        final var found = findByPersonIdAndAuthorId(personId, author);
        baseRatingRepo.delete(found);
    }

    public void deleteTravelRatingById(UUID author, UUID travelId) {
        final var found = findByTravelIdAndAuthorId(travelId, author);
        baseRatingRepo.delete(found);
    }

    public RatingAccount getCurrentAccountPersonRating(UUID author, UUID personId) {
        var found = findByPersonIdAndAuthorId(personId, author);
        return ratingAccountMapper.entityToModel(found);
    }

    public RatingTravel getCurrentAccountTravelRating(UUID author, UUID travelId) {
        var found = findByTravelIdAndAuthorId(travelId, author);
        return ratingTravelMapper.entityToModel(found);
    }

    public AverageRatingPerson getAveragePersonRating(UUID personId) {
        var all = accountRatingRepo.findAllByPersonId(personId);
        var averageValue = all.parallelStream().mapToDouble(rating -> rating.getValue().ordinal()).average();
        return new AverageRatingPerson().personId(personId).count((long) all.size()).value(averageValue.orElse(2.5));
    }

    public AverageRatingTravel getAverageTravelRating(UUID travelId) {
        var all = travelRatingRepo.findAllByTravelId(travelId);
        var averageValue = all.parallelStream().mapToDouble(rating -> rating.getValue().ordinal()).average();
        return new AverageRatingTravel().travelId(travelId).count((long) all.size()).value(averageValue.orElse(2.5));
    }

    public Page<RatingAccount> getAllPersonRatings(UUID personId, Pageable pageable) {
        var page = accountRatingRepo.findAllByPersonId(personId, pageable);
        return page.map(ratingAccountMapper::entityToModel);
    }

    public Page<RatingTravel> getAllTravelRatings(UUID travelId, Pageable pageable) {
        var page = travelRatingRepo.findAllByTravelId(travelId, pageable);
        return page.map(ratingTravelMapper::entityToModel);
    }


}
