package com.ssegning.ntravl.server.service;

import com.google.maps.*;
import com.google.maps.model.*;
import com.ssegning.ntravl.server.model.MapPlace;
import com.ssegning.ntravl.server.repo.maps.AddressComponentMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor
public class MapsService {

    private final GeoApiContext context;
    private final AddressComponentMapper addressComponentMapper;

    public List<MapPlace> routing(String language, LatLng... startingPoint) {
        try {
            return Arrays
                    .stream(RoadsApi
                            .snapToRoads(context, true, startingPoint)
                            .await())
                    .map(snappedPoint -> findAddressByPlaceId(snappedPoint.placeId, language))
                    .collect(Collectors.toList());
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public List<MapPlace> autocomplete(String input, String language, UUID sessionToken) {
        try {
            return Arrays.stream(PlacesApi
                    .placeAutocomplete(context, input, new PlaceAutocompleteRequest.SessionToken(sessionToken))
                    .types(PlaceAutocompleteType.CITIES)
                    .language(language)
                    .await()).map(this::extract).collect(Collectors.toList());
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public List<MapPlace> getPlacesWithFormattedAddressesLike(String formattedAddress, String language) {
        try {
            return Arrays.stream(GeocodingApi
                    .geocode(context, formattedAddress)
                    .resultType(AddressType.LOCALITY, AddressType.POLITICAL)
                    .language(language)
                    .await()).map(this::extract).collect(Collectors.toList());
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public MapPlace findAddressByPlaceId(String placeId, String language) {
        try {
            var geocodingResults = GeocodingApi
                    .newRequest(context)
                    .place(placeId)
                    .language(language)
                    .await();
            return extract(geocodingResults[0]);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public MapPlace findAddressByCoordinate(Double lat, Double lng, String language) {
        try {
            var geocodingResults = GeocodingApi
                    .newRequest(context)
                    .latlng(new LatLng(lat, lng))
                    .resultType(AddressType.LOCALITY, AddressType.POLITICAL)
                    .language(language)
                    .await();
            return extract(geocodingResults[0]);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    private MapPlace extract(GeocodingResult result) {
        var apiMapPlace = new MapPlace()
                .addressComponent(addressComponentMapper.toModel(Arrays.asList(result.addressComponents)))
                .formattedAddress(result.formattedAddress)
                .placeId(result.placeId);

        if (result.geometry != null && result.geometry.location != null) {
            apiMapPlace.setLongitude(result.geometry.location.lng);
            apiMapPlace.setLatitude(result.geometry.location.lat);
        }

        return apiMapPlace;
    }

    private MapPlace extract(AutocompletePrediction prediction) {
        return new MapPlace().formattedAddress(prediction.description).placeId(prediction.placeId);
    }
}
