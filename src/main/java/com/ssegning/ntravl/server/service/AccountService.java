package com.ssegning.ntravl.server.service;

import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import com.ssegning.ntravl.server.domain.embedded.AccountContact;
import com.ssegning.ntravl.server.domain.embedded.AccountMetaData;
import com.ssegning.ntravl.server.domain.embedded.AccountName;
import com.ssegning.ntravl.server.events.AccountCreatedEvent;
import com.ssegning.ntravl.server.exceptions.AccountNotFoundException;
import com.ssegning.ntravl.server.exceptions.BadRequestException;
import com.ssegning.ntravl.server.mapper.account.AccountMapper;
import com.ssegning.ntravl.server.model.Account;
import com.ssegning.ntravl.server.model.AccountType;
import com.ssegning.ntravl.server.model.ChangePersonalDataRequest;
import com.ssegning.ntravl.server.model.CreateAccountInput;
import com.ssegning.ntravl.server.repo.account.AccountRepo;
import com.ssegning.ntravl.server.types.AccountStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Locale;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AccountService {

    private final SmsService smsService;
    private final ApplicationEventPublisher eventPublisher;

    private final AccountRepo accountRepo;
    private final AccountMapper accountMapper;

    private AccountEntity findOne(UUID id) {
        return accountRepo.findById(id).orElseThrow(() -> new AccountNotFoundException(id));
    }

    public Account findById(UUID id) {
        return accountRepo
                .findById(id)
                .map(accountMapper::entityToModel)
                .orElseThrow(() -> new AccountNotFoundException(id));
    }

    public Account findByEmail(String email) {
        return accountRepo
                .findByContactEmail(email)
                .map(accountMapper::entityToModel)
                .orElseThrow(() -> new AccountNotFoundException(email));
    }

    @Transactional(rollbackOn = {IOException.class, RuntimeException.class})
    public Account createAccount(CreateAccountInput input, Locale locale) {
        var name = new AccountName(input.getFirstName(), input.getLastName());

        var contact = new AccountContact(input.getEmail(), false, input.getPhoneNumber(), false);

        AccountMetaData metaData;
        try {
            metaData = new AccountMetaData(
                    input.getGender(),
                    locale,
                    input.getBio(),
                    input.getAvatarUrl() != null ? input.getAvatarUrl().toURL() : null,
                    input.getAccountType() != null ? input.getAccountType() : AccountType.C2C
            );
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }

        var toSave = new AccountEntity();
        toSave.setName(name);
        toSave.setContact(contact);
        toSave.setMetaData(metaData);

        var saved = accountRepo.save(toSave);
        var created = accountMapper.entityToModel(saved);
        eventPublisher.publishEvent(new AccountCreatedEvent(created));
        return created;
    }

    public AccountEntity updateAccount(AccountEntity account) {
        return accountRepo.save(account);
    }

    public AccountEntity secureUpdatePhone(AccountEntity account, String phoneNumber) {
        var found = findOne(account.getId());
        found.getContact().setPhoneNumber(phoneNumber);
        found.getContact().setPhoneNumberVerified(true);
        return accountRepo.save(found);
    }


    public void deleteAccount(UUID accountId) {
    }

    public Page<Account> getAccounts(Pageable pageable) {
        var page = accountRepo.findAll(pageable);
        return page.map(accountMapper::entityToModel);
    }

    public Page<Account> getAccounts(String query, Pageable pageable) {
        var page = accountRepo.findAllByName("%" + query + "%", pageable);
        return page.map(accountMapper::entityToModel);
    }

    public Long countAccounts(String query) {
        return query == null ? accountRepo.count() : accountRepo.countAccountsBy("%" + query + "%");
    }

    public Account findByIdentifier(String id) {
        Account by;
        try {
            var accountId = UUID.fromString(id);
            by = findById(accountId);
        } catch (IllegalArgumentException e) {
            by = id.contains("@") ? findByEmail(id) : findByPhoneNumber(id);
        }
        return by;
    }

    public Account findByPhoneNumber(String phoneNumber) {
        var foundOptional = accountRepo.findByContactPhoneNumber(phoneNumber).map(accountMapper::entityToModel);
        if (foundOptional.isEmpty())
            throw new AccountNotFoundException("account not found by phone number " + phoneNumber);
        return foundOptional.get();
    }

    public Account updatePersonalData(UUID id, ChangePersonalDataRequest changePersonalDataRequest) {
        var found = findOne(id);
        var update = mapToAccount(found, changePersonalDataRequest);
        var updated = updateAccount(update);
        return accountMapper.entityToModel(updated);
    }

    private AccountEntity mapToAccount(AccountEntity account, ChangePersonalDataRequest changePersonalDataRequest) {
        if (changePersonalDataRequest.getStatus() != null) {
            var value = AccountStatus.fromValue(changePersonalDataRequest.getStatus().getValue());
            account.setStatus(value);
        }

        account.setContact(account.getContact() != null ? account.getContact() : new AccountContact());
        if (changePersonalDataRequest.getPhoneNumberValid() != null) {
            account.getContact().setPhoneNumberVerified(changePersonalDataRequest.getPhoneNumberValid());
        }
        if (changePersonalDataRequest.getPhoneNumber() != null) {
            account.getContact().setPhoneNumber(changePersonalDataRequest.getPhoneNumber());
            account.getContact().setPhoneNumberVerified(false);
        }
        if (changePersonalDataRequest.getEmail() != null) {
            account.getContact().setEmail(changePersonalDataRequest.getEmail());
            account.getContact().setEmailVerified(false);
        }
        if (changePersonalDataRequest.getEmailVerified() != null) {
            account.getContact().setEmailVerified(changePersonalDataRequest.getEmailVerified());
        }

        account.setName(account.getName() != null ? account.getName() : new AccountName());
        if (changePersonalDataRequest.getFirstName() != null) {
            account.getName().setFirstName(changePersonalDataRequest.getFirstName());
        }
        if (changePersonalDataRequest.getLastName() != null) {
            account.getName().setLastName(changePersonalDataRequest.getLastName());
        }

        account.setMetaData(account.getMetaData() != null ? account.getMetaData() : new AccountMetaData());
        if (changePersonalDataRequest.getBio() != null) {
            account.getMetaData().setBio(changePersonalDataRequest.getBio());
        }
        if (changePersonalDataRequest.getAvatarUrl() != null) {
            try {
                account.getMetaData().setAvatarUrl(changePersonalDataRequest.getAvatarUrl().toURL());
            } catch (MalformedURLException e) {
                throw new BadRequestException(e);
            }
        }
        if (changePersonalDataRequest.getLocale() != null) {
            account.getMetaData().setLocale(changePersonalDataRequest.getLocale());
        }
        if (changePersonalDataRequest.getGender() != null) {
            account.getMetaData().setGender(changePersonalDataRequest.getGender());
        }
        if (changePersonalDataRequest.getAccountType() != null) {
            account.getMetaData().setAccountType(changePersonalDataRequest.getAccountType());
        }
        return account;
    }
}
