package com.ssegning.ntravl.server.service;

import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import com.ssegning.ntravl.server.domain.accounts.PhoneNumberValidationEntity;
import com.ssegning.ntravl.server.model.Account;
import com.ssegning.ntravl.server.repo.account.PhoneNumberValidationRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class PhoneNumberService {

    private final SmsService smsService;
    private final PhoneNumberValidationRepo phoneNumberValidationRepo;

    public UUID createPhoneNumberTan(Account account, String phoneNumber) {
        var code = CryptoService.generateCode(6);
        PhoneNumberValidationEntity validationEntity = new PhoneNumberValidationEntity();
        validationEntity.setCode(code);
        validationEntity.setPhoneNumber(phoneNumber);
        validationEntity.setAccount(new AccountEntity(account.getId()));
        smsService.sendSms(phoneNumber, "validation.tan", account.getLocale(), code);
        return phoneNumberValidationRepo.save(validationEntity).getId();
    }

    public Boolean validatePhoneNumberTan(Account account, String phoneNumber, int code, UUID secret) {
        var found$ = phoneNumberValidationRepo.findByCodeAndIdAndAccountIdAndPhoneNumber(code + "", secret, account.getId(), phoneNumber);
        if (found$.isPresent()) {
            var entity = found$.get();
            entity.setUsed(true);
            phoneNumberValidationRepo.save(entity);
            return true;
        }
        return false;
    }

}
