package com.ssegning.ntravl.server.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class CryptoService {

    private final PasswordEncoder passwordEncoder;

    public static String generateCode(int length) {
        return RandomStringUtils.randomNumeric(length);
    }

    public String encode(String value) {
        return passwordEncoder.encode(value);
    }

    public Boolean verifyEncoded(String hashed, String challenge) {
        return passwordEncoder.matches(challenge, hashed);
    }

}
