package com.ssegning.ntravl.server.service;

import com.ssegning.ntravl.server.exceptions.NotFoundException;
import com.ssegning.ntravl.server.mapper.travel.TravelPropositionMapper;
import com.ssegning.ntravl.server.model.PropositionChatMessage;
import com.ssegning.ntravl.server.model.TravelProposition;
import com.ssegning.ntravl.server.repo.travel.PropositionRepo;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@AllArgsConstructor
public class PropositionsService {

    private final ChatMessageService chatMessageService;

    private final PropositionRepo propositionRepo;
    private final TravelPropositionMapper propositionMapper;

    @Transactional
    public TravelProposition create(TravelProposition proposition) {
        var message = new PropositionChatMessage().proposition(proposition);
        var saved = chatMessageService.addMessage(message);
        return saved.getProposition();
    }

    public TravelProposition getOne(UUID propositionId) {
        var found = propositionRepo.findById(propositionId).orElseThrow(NotFoundException::new);
        return propositionMapper.entityToModel(found);
    }

    public TravelProposition update(UUID id, TravelProposition proposition) {
        var found = getOne(id);
        proposition.setId(found.getId());

        var toCreate = propositionMapper.toEntity(proposition);
        var saved = propositionRepo.save(toCreate);

        return propositionMapper.entityToModel(saved);
    }

    public Page<TravelProposition> getPropositions(UUID travelId, Pageable pageable) {
        return propositionRepo
                .getAllByTravelId(travelId, pageable)
                .map(propositionMapper::entityToModel);
    }
}
