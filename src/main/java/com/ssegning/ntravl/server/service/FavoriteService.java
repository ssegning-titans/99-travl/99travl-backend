package com.ssegning.ntravl.server.service;

import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import com.ssegning.ntravl.server.domain.favorite.BaseFavoriteEntity;
import com.ssegning.ntravl.server.domain.favorite.FavoriteAccountEntity;
import com.ssegning.ntravl.server.domain.favorite.FavoriteTravelEntity;
import com.ssegning.ntravl.server.domain.travel.TravelEntity;
import com.ssegning.ntravl.server.exceptions.FavoriteNotFoundException;
import com.ssegning.ntravl.server.mapper.favorite.FavoriteAccountMapper;
import com.ssegning.ntravl.server.mapper.favorite.FavoriteTravelMapper;
import com.ssegning.ntravl.server.model.BaseFavorite;
import com.ssegning.ntravl.server.model.FavoriteAccount;
import com.ssegning.ntravl.server.model.FavoriteTravel;
import com.ssegning.ntravl.server.repo.favorite.BaseFavoriteRepo;
import com.ssegning.ntravl.server.repo.favorite.FavoriteAccountRepo;
import com.ssegning.ntravl.server.repo.favorite.FavoriteTravelRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class FavoriteService {


    private final FavoriteAccountMapper favoriteAccountMapper;
    private final FavoriteTravelMapper favoriteTravelMapper;

    private final FavoriteAccountRepo favoriteAccountRepo;
    private final FavoriteTravelRepo favoriteTravelRepo;

    private final BaseFavoriteRepo baseFavoriteRepo;

    public FavoriteAccount createFavoriteAccount(UUID accountId, UUID personId) {
        var found = findPreviousFavoriteAccount(accountId, personId);
        if (found == null) {
            var toCreate = new FavoriteAccountEntity();
            var person = new AccountEntity();
            person.setId(personId);
            toCreate.setPerson(person);

            var owner = new AccountEntity();
            owner.setId(accountId);
            toCreate.setOwner(owner);

            found = favoriteAccountRepo.save(toCreate);
        }
        return favoriteAccountMapper.entityToModel(found);
    }

    public void deleteFavoriteAccount(UUID accountId, UUID personId) {
        var found = findPreviousFavoriteAccount(accountId, personId);
        if (found == null) {
            throw new FavoriteNotFoundException(String.format("%s:account:%s", accountId, personId));
        }
        favoriteAccountRepo.delete(found);
    }

    public FavoriteAccount getFavoriteAccount(UUID accountId, UUID personId) {
        var found = findPreviousFavoriteAccount(accountId, personId);
        if (found == null) {
            throw new FavoriteNotFoundException(String.format("%s:account:%s", accountId, personId));
        }
        return favoriteAccountMapper.entityToModel(found);
    }

    public FavoriteAccountEntity findPreviousFavoriteAccount(UUID accountId, UUID personId) {
        return favoriteAccountRepo.findByOwnerIdAndPersonId(accountId, personId);
    }

    public FavoriteTravel createFavoriteTravel(UUID accountId, UUID travelId) {
        var found = findPreviousFavoriteTravel(accountId, travelId);
        if (found == null) {
            var toCreate = new FavoriteTravelEntity();
            var travel = new TravelEntity();
            travel.setId(travelId);
            toCreate.setTravel(travel);

            var owner = new AccountEntity();
            owner.setId(accountId);
            toCreate.setOwner(owner);
            found = favoriteTravelRepo.save(toCreate);
        }

        return favoriteTravelMapper.entityToModel(found);
    }

    public void deleteFavoriteTravel(UUID accountId, UUID travelId) {
        var found = findPreviousFavoriteTravel(accountId, travelId);
        if (found == null) {
            throw new FavoriteNotFoundException(String.format("%s:travel:%s", accountId, travelId));
        }
        favoriteTravelRepo.delete(found);
    }

    public FavoriteTravel getFavoriteTravel(UUID accountId, UUID travelId) {
        var found = findPreviousFavoriteTravel(accountId, travelId);
        if (found == null) {
            throw new FavoriteNotFoundException(String.format("%s:travel:%s", accountId, travelId));
        }
        return favoriteTravelMapper.entityToModel(found);
    }

    public FavoriteTravelEntity findPreviousFavoriteTravel(UUID accountId, UUID travelId) {
        return favoriteTravelRepo.findByOwnerIdAndTravelId(accountId, travelId);
    }

    public Page<BaseFavorite> getAllByAccountId(UUID accountId, Pageable pageable) {
        var page = baseFavoriteRepo
                .getAllByOwnerId(accountId, pageable);
        return page.map(this::map);
    }

    private BaseFavorite map(BaseFavoriteEntity entity) {
        if (entity instanceof FavoriteTravelEntity) {
            return favoriteTravelMapper.entityToModel((FavoriteTravelEntity) entity);
        } else if (entity instanceof FavoriteAccountEntity) {
            return favoriteAccountMapper.entityToModel((FavoriteAccountEntity) entity);
        } else {
            throw new RuntimeException("This type is not supported");
        }
    }
}
