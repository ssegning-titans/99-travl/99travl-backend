package com.ssegning.ntravl.server.service;

import com.ssegning.ntravl.server.domain.media.DocumentEntity;
import com.ssegning.ntravl.server.exceptions.FileNotFoundException;
import com.ssegning.ntravl.server.repo.document.DocumentRepo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
public class DownloadService {

    private final DocumentRepo documentRepo;

    @Transactional
    public DocumentEntity getByPath(String path) {
        return documentRepo.getByFilePath(path).orElseThrow(() -> new FileNotFoundException(path));
    }
}
