package com.ssegning.ntravl.server.service;

import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Slf4j
@Service
@RequiredArgsConstructor
public class SmsService {

    private final MessageSource messageSource;

    @Value("${api.twilio.from}")
    private String fromSms;

    public void sendSms(String phoneNumber, String message, Locale locale, String... args) {

        if (locale == null) {
            locale = Locale.ENGLISH;
        }

        var messageFormatted = messageSource.getMessage(message, args, locale);

        var result = Message.creator(
                new PhoneNumber(phoneNumber),
                new PhoneNumber(fromSms),
                messageFormatted
        ).create();

        log.info(result.getMessagingServiceSid());

    }
}
