package com.ssegning.ntravl.server.service;

import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import com.ssegning.ntravl.server.domain.accounts.PasswordEntity;
import com.ssegning.ntravl.server.model.Account;
import com.ssegning.ntravl.server.repo.account.PasswordRepo;
import com.ssegning.ntravl.server.types.PasswordStatus;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class PasswordService {

    private final CryptoService cryptoService;
    private final PasswordRepo passwordRepo;

    public void createUserPassword(String challenge, Account account) {
        var hashed = cryptoService.encode(challenge);
        passwordRepo.save(new PasswordEntity(new AccountEntity(account.getId()), PasswordStatus.ACTIVE, hashed));
    }

    public void deactivatePreviousUserPassword(Account account) {
        passwordRepo.deactivatePrevious(account.getId(), PasswordStatus.ACTIVE);
    }

    private PasswordEntity getUserLastPassword(Account account) {
        return passwordRepo.findTopByAccountIdAndStatus(account.getId(), PasswordStatus.ACTIVE);
    }

    public boolean hasPassword(Account account) {
        return getUserLastPassword(account) != null;
    }

    public boolean validatePassword(Account account, String challenge) {
        var found = getUserLastPassword(account);
        return cryptoService.verifyEncoded(found.getHashedValue(), challenge);
    }

}
