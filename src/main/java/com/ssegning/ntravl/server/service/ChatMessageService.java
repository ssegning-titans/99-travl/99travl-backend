package com.ssegning.ntravl.server.service;

import com.ssegning.ntravl.server.events.ChatMessageCreatedEvent;
import com.ssegning.ntravl.server.exceptions.ChatMessageNotFoundException;
import com.ssegning.ntravl.server.mapper.chat.ChatMessageMapper;
import com.ssegning.ntravl.server.model.BaseChatMessage;
import com.ssegning.ntravl.server.repo.chat.BaseChatMessageRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ChatMessageService {

    private final ChatMessageMapper chatMessageMapper;
    private final BaseChatMessageRepo baseChatMessageRepo;
    private final ApplicationEventPublisher eventPublisher;

    @Transactional
    public <T extends BaseChatMessage> T addMessage(T baseChatMessage) {
        var entity = chatMessageMapper.toEntity(baseChatMessage);
        var saved = baseChatMessageRepo.save(entity);
        var created = chatMessageMapper.entityToModel(saved);
        eventPublisher.publishEvent(new ChatMessageCreatedEvent(created));
        return (T) created;
    }

    public Page<BaseChatMessage> getChatMessages(UUID chatId, Pageable pageable) {
        var page = baseChatMessageRepo.findAllByChatId(chatId, pageable);
        return page.map(chatMessageMapper::entityToModel);
    }

    public Page<BaseChatMessage> getAllUnreadChatMessages(UUID chatId, Pageable pageable) {
        var page = baseChatMessageRepo.findAllByChatIdAndSeen(chatId, false, pageable);
        return page.map(chatMessageMapper::entityToModel);
    }

    public Page<BaseChatMessage> getUnreadChatMessages(UUID authorId, Pageable pageable) {
        var page = baseChatMessageRepo.findAllByAuthorIdNotInAndSeen(List.of(authorId), false, pageable);
        return page.map(chatMessageMapper::entityToModel);
    }

    public void deleteById(UUID messageId) {
        baseChatMessageRepo.deleteById(messageId);
    }

    public void markAsRead(UUID messageId) {
        baseChatMessageRepo.markAsRead(messageId, true);
    }

    public BaseChatMessage findOneById(UUID id) {
        var found = baseChatMessageRepo.findById(id).orElseThrow(() -> new ChatMessageNotFoundException(id));
        return chatMessageMapper.entityToModel(found);
    }

}
