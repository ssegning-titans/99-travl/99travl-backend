package com.ssegning.ntravl.server.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import com.ssegning.ntravl.server.domain.embedded.ETravelQuantity;
import com.ssegning.ntravl.server.domain.travel.TravelEntity;
import com.ssegning.ntravl.server.domain.travel.TravelPointEntity;
import com.ssegning.ntravl.server.events.TravelCreatedEvent;
import com.ssegning.ntravl.server.exceptions.NotFoundException;
import com.ssegning.ntravl.server.exceptions.TravelNotFoundException;
import com.ssegning.ntravl.server.mapper.core.AddressMapper;
import com.ssegning.ntravl.server.mapper.travel.TravelMapper;
import com.ssegning.ntravl.server.mapper.travel.TravelPointMapper;
import com.ssegning.ntravl.server.model.CreateTravel;
import com.ssegning.ntravl.server.model.LatLng;
import com.ssegning.ntravl.server.model.Travel;
import com.ssegning.ntravl.server.model.TravelPoint;
import com.ssegning.ntravl.server.repo.travel.TravelPointRepo;
import com.ssegning.ntravl.server.repo.travel.TravelRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class TravelService {

    private final TravelRepo travelRepo;
    private final TravelMapper travelMapper;
    private final ObjectMapper objectMapper;
    private final AddressMapper addressMapper;
    private final TravelPointRepo travelPointRepo;
    private final TravelPointMapper travelPointMapper;
    private final ApplicationEventPublisher eventPublisher;

    @Transactional
    public Travel addTravel(CreateTravel createTravel, UUID providerId) {
        var passagePoints = new HashSet<TravelPointEntity>(createTravel.getPassagePoints().size() * 2 / 3);
        var travel = new TravelEntity();
        travel.setProvider(new AccountEntity(providerId));
        travel.setPassagePoints(passagePoints);
        travel.setMaxQuantity(ETravelQuantity.builder()
                .unit(createTravel.getMaxQuantity().getUnit())
                .weight(createTravel.getMaxQuantity().getWeight())
                .pricePerUnit(createTravel.getMaxQuantity().getPricePerUnit())
                .build());
        travel.setTransportMode(createTravel.getTransportMode());
        travel.setLuggageDimension(createTravel.getLuggageDimension());
        travel.setDescription(createTravel.getDescription());

        for (TravelPoint passagePoint : createTravel.getPassagePoints()) {
            var entity = travelPointMapper.toEntity(passagePoint);
            entity.setTravel(travel);
            passagePoints.add(entity);
        }

        var saved = travelRepo.save(travel);
        var created = travelMapper.entityToModel(saved);
        eventPublisher.publishEvent(new TravelCreatedEvent(created));
        return created;
    }

    public void deleteTravel(UUID travelId) {
        var found = findTravelById(travelId);
        travelRepo.delete(found);
    }

    public Travel getById(UUID travelId) {
        var found = findTravelById(travelId);
        return travelMapper.entityToModel(found);
    }

    public Travel updateTravel(Travel travel) {
        var found = findTravelById(travel.getId());
        var update = travelMapper.toEntity(travel);
        var updated = updateOne(found, update);
        return travelMapper.entityToModel(updated);
    }

    public Page<List<TravelPoint>> findByStartAndEnd(LatLng start, LatLng end, Double startRadius, Double endRadius, long startDate, long endDate, Pageable pageable) {
        var page = travelPointRepo
                .findByStartAndEnd(
                        coordAsString(start),
                        coordAsString(end),
                        startRadius,
                        endRadius,
                        new Date(startDate),
                        new Date(endDate),
                        pageable
                );

        return page.map(this::parseData);
    }

    public Page<TravelPoint> findRandomlyNearPoint(LatLng point, Double radius, Pageable pageable) {
        var coords = coordAsString(point);
        var page = travelPointRepo.findRandomlyNearPoint(coords, radius, pageable);
        return page.map(travelPointMapper::entityToModel);
    }

    private String coordAsString(LatLng latLng) {
        try {
            var point = addressMapper.latLngToPoint(latLng);
            return objectMapper.writeValueAsString(point);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private List<TravelPoint> parseData(Object[] arrayOfSelection) {
        return List.of(toPoint(arrayOfSelection[0]), toPoint(arrayOfSelection[1]));
    }

    private TravelPoint toPoint(Object any) {
        var travelPoint = (TravelPointEntity) any;
        return travelPointMapper.entityToModel(travelPoint);
    }

    private TravelEntity findTravelById(UUID travelId) {
        var travel = travelRepo.findById(travelId);
        return travel.orElseThrow(() -> new TravelNotFoundException(travelId));
    }

    private TravelPointEntity findTravelPointById(UUID travelPointId) {
        var point = travelPointRepo.findById(travelPointId);
        return point.orElseThrow(() -> new NotFoundException(travelPointId));
    }

    private TravelEntity updateOne(TravelEntity oldVersion, TravelEntity newVersion) {
        // TODO What are we updating?

        return travelRepo.save(newVersion);
    }

    public Page<Travel> accountTravels(UUID accountId, Pageable pageable) {
        var result = travelRepo.findAllByProviderId(accountId, pageable);
        return result.map(travelMapper::entityToModel);
    }
}
