package com.ssegning.ntravl.server.service;

import com.ssegning.ntravl.server.domain.accounts.AccountAddressEntity;
import com.ssegning.ntravl.server.exceptions.AddressNotFoundException;
import com.ssegning.ntravl.server.mapper.account.AccountAddressMapper;
import com.ssegning.ntravl.server.model.AccountAddress;
import com.ssegning.ntravl.server.repo.account.AccountAddressRepo;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@AllArgsConstructor
public class AccountAddressService {

    private final AccountAddressRepo repo;
    private final AccountAddressMapper addressMapper;

    public AccountAddress save(AccountAddress entity) {
        var toSave = addressMapper.toEntity(entity);
        var saved = repo.save(toSave);
        return addressMapper.entityToModel(saved);
    }

    public void deleteById(UUID accountId, UUID addressId) {
        var found = repo.findByIdAndAccountId(addressId, accountId);
        if (found.isPresent()) {
            var value = found.get();
            repo.delete(value);
        }
    }

    public AccountAddress findById(UUID accountId, UUID addressId) {
        var found = repo.findByIdAndAccountId(addressId, accountId).orElseThrow(() -> new AddressNotFoundException("account", addressId));
        return addressMapper.entityToModel(found);
    }

    public Page<AccountAddress> findAllByAccountId(UUID accountId, Pageable pageable) {
        var page = repo.findAllByAccountIdOrderByCreationDate(accountId, pageable);
        return page.map(addressMapper::entityToModel);
    }

    public AccountAddress firstByAccountId(UUID accountId) {
        var found = repo.findFirstByAccountIdOrderByCreationDate(accountId).orElseThrow(() -> new AddressNotFoundException("account", null));
        return addressMapper.entityToModel(found);
    }

    public AccountAddress updateAddress(UUID id, AccountAddress address) {
        var entity = addressMapper.toEntity(address);
        var found = repo.findByIdAndAccountId(address.getId(), id).orElseThrow(() -> new AddressNotFoundException("account", address.getId()));
        found.setAddress(entity.getAddress());
        found.setPhoneNumber(address.getPhoneNumber());
        var saved = repo.save(found);
        return addressMapper.entityToModel(saved);
    }
}
