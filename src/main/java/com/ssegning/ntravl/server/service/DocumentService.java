package com.ssegning.ntravl.server.service;

import com.ssegning.ntravl.server.domain.media.DocumentEntity;
import com.ssegning.ntravl.server.exceptions.NotImplementedException;
import com.ssegning.ntravl.server.model.Document;
import com.ssegning.ntravl.server.model.UploadedResponse;
import com.ssegning.ntravl.server.repo.document.DocumentRepo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.UUID;

@Service
@AllArgsConstructor
public class DocumentService {

    private final DocumentRepo documentRepo;

    public UploadedResponse uploadFile(byte[] bytes, String filePath, String contentType, Map<String, String> headers, Map<String, String> userMetadata) {
        var document = new DocumentEntity();
        document.setBytes(bytes);
        document.setContentType(contentType);
        document.setFilePath(filePath);
        document.setHeaders(headers);
        document.setMetaData(userMetadata);

        var created = documentRepo.save(document);
        return new UploadedResponse().id(created.getId()).path(String.format("api/f/%s", filePath));
    }

    public Document getById(UUID documentId) {
        throw new NotImplementedException("this is not yet done");
    }
}
