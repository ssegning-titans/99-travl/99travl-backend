package com.ssegning.ntravl.server.service;

import com.ssegning.ntravl.server.domain.chat.ChatEntity;
import com.ssegning.ntravl.server.events.ChatCreatedEvent;
import com.ssegning.ntravl.server.exceptions.ChatNotFoundException;
import com.ssegning.ntravl.server.mapper.chat.ChatMapper;
import com.ssegning.ntravl.server.model.Chat;
import com.ssegning.ntravl.server.model.ChatStatus;
import com.ssegning.ntravl.server.repo.chat.ChatRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class ChatService {

    private final ChatRepo chatRepo;
    private final ChatMapper chatMapper;
    private final ApplicationEventPublisher eventPublisher;

    @Transactional
    public Chat createChat(Chat chat) {
        final var chatsByMembers = chatRepo.findByMembers(chat.getMembers().get(0), chat.getMembers().get(1));
        for (ChatEntity chatsByMember : chatsByMembers) {
            if (chatsByMember.getTravel().getId().equals(chat.getTravelId())) {
                return chatMapper.entityToModel(chatsByMember);
            }
        }

        var entity = chatMapper.toEntity(chat);
        var saved = chatRepo.save(entity);
        var created = chatMapper.entityToModel(saved);
        eventPublisher.publishEvent(new ChatCreatedEvent(created));
        return created;
    }

    public void markAsArchived(UUID chatId) {
        chatRepo.changeChatStatus(chatId, ChatStatus.ARCHIVED);
    }

    public Chat findById(UUID chatId) {
        var found = chatRepo.findById(chatId).orElseThrow(() -> new ChatNotFoundException(chatId));
        return chatMapper.entityToModel(found);
    }

    public void deleteOne(UUID chatId) {
        var found = chatRepo.findById(chatId).orElseThrow(() -> new ChatNotFoundException(chatId));
        chatRepo.delete(found);
    }

    public Page<Chat> getChatsByAccountId(UUID accountId, com.ssegning.ntravl.server.model.ChatStatus status, Pageable pageable) {
        var page = chatRepo.findByMembers_IdAndStatus(accountId, status, pageable);
        return page.map(chatMapper::entityToModel);
    }

}
