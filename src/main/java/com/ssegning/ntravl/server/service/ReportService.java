package com.ssegning.ntravl.server.service;

import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import com.ssegning.ntravl.server.domain.report.BaseReportEntity;
import com.ssegning.ntravl.server.domain.report.ReportAccountEntity;
import com.ssegning.ntravl.server.domain.report.ReportBugEntity;
import com.ssegning.ntravl.server.domain.report.ReportTravelEntity;
import com.ssegning.ntravl.server.domain.travel.TravelEntity;
import com.ssegning.ntravl.server.exceptions.ReportNotFoundException;
import com.ssegning.ntravl.server.mapper.report.ReportAccountMapper;
import com.ssegning.ntravl.server.mapper.report.ReportBugMapper;
import com.ssegning.ntravl.server.mapper.report.ReportTravelMapper;
import com.ssegning.ntravl.server.model.*;
import com.ssegning.ntravl.server.repo.report.BaseReportRepo;
import com.ssegning.ntravl.server.repo.report.ReportAccountRepo;
import com.ssegning.ntravl.server.repo.report.ReportBugRepo;
import com.ssegning.ntravl.server.repo.report.ReportTravelRepo;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@AllArgsConstructor
public class ReportService {

    private final ReportAccountMapper reportAccountMapper;
    private final ReportTravelMapper reportTravelMapper;
    private final ReportBugMapper reportBugMapper;

    private final BaseReportRepo baseReportRepo;
    private final ReportAccountRepo reportAccountRepo;
    private final ReportTravelRepo reportTravelRepo;
    private final ReportBugRepo reportBugRepo;

    public BaseReport findOneById(UUID reportId) {
        var found = baseReportRepo
                .findById(reportId)
                .map(this::map);
        if (found.isEmpty()) {
            throw new ReportNotFoundException(reportId);
        }
        return found.get();
    }

    public ReportTravel createTravelReport(CreateReportTravel input) {
        var toSave = new ReportTravelEntity();
        toSave.setTravel(new TravelEntity(input.getTravelId()));
        toSave.setAuthor(new AccountEntity(input.getAuthorId()));
        toSave.setDescription(input.getDescription());

        var saved = reportTravelRepo.save(toSave);
        return reportTravelMapper.entityToModel(saved);
    }

    public ReportBug createBugReport(CreateReportBug input, UUID reporterId) {
        var toSave = new ReportBugEntity();
        toSave.setDeviceInfo(input.getDeviceInfo());
        toSave.setAuthor(new AccountEntity(reporterId));
        toSave.setDescription(input.getDescription());
        toSave.setTitle(input.getTitle());
        toSave.setScreenshot(reportBugMapper.toEntity(input.getScreenshot()));
        var saved = reportBugRepo.save(toSave);
        return reportBugMapper.entityToModel(saved);
    }

    public ReportAccount createAccountReport(CreateReportAccount input) {
        var toSave = new ReportAccountEntity();
        toSave.setAccount(new AccountEntity(input.getAccountId()));
        toSave.setAuthor(new AccountEntity(input.getAuthorId()));
        toSave.setDescription(input.getDescription());
        var saved = reportAccountRepo.save(toSave);
        return reportAccountMapper.entityToModel(saved);
    }

    public Page<BaseReport> getOwnerReports(UUID ownerId, ReportStatus status, Pageable pageable) {
        var page = baseReportRepo.findAllByAuthorIdAndStatus(ownerId, status, pageable);
        return page.map(this::map);
    }

    public Page<BaseReport> getAllReports(ReportStatus status, Pageable pageable) {
        var page = baseReportRepo.findAllByStatus(status, pageable);
        return page.map(this::map);
    }

    public Page<ReportTravel> getAllReportForTravel(UUID travelId, Pageable pageable) {
        var page = reportTravelRepo.findAllByTravelId(travelId, pageable);
        return page.map(reportTravelMapper::entityToModel);
    }

    public Page<ReportAccount> getAllReportForAccount(UUID accountId, Pageable pageable) {
        var page = reportAccountRepo.findAllByAccountId(accountId, pageable);
        return page.map(reportAccountMapper::entityToModel);
    }

    public Page<ReportBug> getAllBugReports(UUID accountUd, Pageable pageable) {
        var page = accountUd == null ? reportBugRepo.findAll(pageable) : reportBugRepo.findAllByAuthorId(accountUd, pageable);
        return page.map(reportBugMapper::entityToModel);
    }

    private BaseReport map(BaseReportEntity entity) {
        if (entity instanceof ReportTravelEntity) {
            return reportTravelMapper.entityToModel((ReportTravelEntity) entity);
        } else if (entity instanceof ReportBugEntity) {
            return reportBugMapper.entityToModel((ReportBugEntity) entity);
        } else if (entity instanceof ReportAccountEntity) {
            return reportAccountMapper.entityToModel((ReportAccountEntity) entity);
        } else {
            throw new RuntimeException("This type is not supported");
        }
    }

}
