package com.ssegning.ntravl.server.handler;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.ssegning.ntravl.server.events.TravelCreatedEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@AllArgsConstructor
public class TravelEventListener {

    private final FirebaseMessaging firebaseMessaging;

    @EventListener
    public void handleCreate(TravelCreatedEvent event) throws FirebaseMessagingException {
        var travel = event.getTravel();
        var passagePoints = travel.getPassagePoints();
        var first = passagePoints.get(0);
        var last = passagePoints.get(passagePoints.size() - 1);

        var message = Message.builder()
                .putData("type", event.getName())
                .putData("title", "Your travel has been created")
                .putData("content", String.format("Your journey from %s to %s has been created", first.getFormattedName(), last.getFormattedName()))
                .putData("travelId", travel.getId().toString())
                .setCondition(String.format("'accounts-%s' in topics || 'travels-%s' in topics", travel.getProviderAccountId(), travel.getId()))
                .build();
        firebaseMessaging.send(message);
    }

}
