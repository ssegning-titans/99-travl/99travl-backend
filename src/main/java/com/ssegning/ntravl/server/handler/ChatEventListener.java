package com.ssegning.ntravl.server.handler;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.ssegning.ntravl.server.events.ChatCreatedEvent;
import com.ssegning.ntravl.server.events.ChatMessageCreatedEvent;
import com.ssegning.ntravl.server.service.ChatService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Slf4j
@Component
@AllArgsConstructor
public class ChatEventListener {

    private final ChatService chatService;
    private final FirebaseMessaging firebaseMessaging;

    @EventListener
    public void handleCreate(ChatMessageCreatedEvent event) throws FirebaseMessagingException {
        var chatMessage = event.getChatMessage();
        var chat = chatService.findById(chatMessage.getChatId());

        var chatId = chat.getId().toString();
        var chatMessageId = chatMessage.getId().toString();

        var message = Message.builder()
                .putData("type", event.getName())
                .putData("title", "Your have a new message")
                .putData("content", "Your have a new message")
                .putData("chatId", chatId)
                .putData("chatMessageId", chatMessageId)
                .setCondition(String.format("'chat-%s' in topics || 'chat-message-%s' in topics", chatId, chatMessageId))
                .build();
        firebaseMessaging.send(message);
    }

    @EventListener
    public void handleCreate(ChatCreatedEvent event) throws FirebaseMessagingException {
        var chat = event.getChat();
        StringBuilder strBld = new StringBuilder();
        for (UUID member : chat.getMembers()) {
            strBld.append(" || 'account-");
            strBld.append(member);
            strBld.append("' in topics");
        }

        var chatId = chat.getId().toString();
        var message = Message.builder()
                .putData("type", event.getName())
                .putData("title", "Your have a new message")
                .putData("content", "Your have a new message")
                .putData("chatId", chatId)
                .setCondition(String.format("'chat-%s' in topics%s", chatId, strBld))
                .build();
        firebaseMessaging.send(message);
    }

}
