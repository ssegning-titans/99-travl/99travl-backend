package com.ssegning.ntravl.server.annotation;

import com.ssegning.ntravl.server.validator.PhoneNumberValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = PhoneNumberValidator.class)
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AppPhoneNumber {
    String message() default "{app.message.constraint.PhoneNumber}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String region() default "{app.message.constraint.PhoneNumber}";
}
