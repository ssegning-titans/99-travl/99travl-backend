package com.ssegning.ntravl.server.repo.account;

import com.ssegning.ntravl.server.helper.annotation.WithDatabase;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

@WithDatabase
class AccountAddressRepoTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void findByIdAndAccountId() {
    }

    @Test
    void findFirstByAccountIdOrderByCreationDate() {
    }

    @Test
    void findAllByAccountIdOrderByCreationDate() {
    }
}
