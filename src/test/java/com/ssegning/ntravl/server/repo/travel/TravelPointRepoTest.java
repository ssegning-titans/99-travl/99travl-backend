package com.ssegning.ntravl.server.repo.travel;

import com.ssegning.ntravl.server.domain.embedded.Address;
import com.ssegning.ntravl.server.domain.travel.TravelEntity;
import com.ssegning.ntravl.server.domain.travel.TravelPointEntity;
import com.ssegning.ntravl.server.helper.annotation.WithDatabase;
import com.ssegning.ntravl.server.helper.common.AccountUtils;
import com.ssegning.ntravl.server.mapper.core.AddressMapper;
import com.ssegning.ntravl.server.repo.account.AccountRepo;
import com.ssegning.ntravl.server.util.AppPageRequest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.awt.geom.Point2D;
import java.sql.Date;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

@WithDatabase
public class TravelPointRepoTest {

    private final static AddressMapper addressMapper = new AddressMapper();

    @Autowired
    private TravelPointRepo repo;

    @Autowired
    private TravelRepo travelRepo;

    @Autowired
    private AccountRepo accountRepo;

    @Test
    void findByStartAndEnd() {
        var account = accountRepo.save(AccountUtils.randomAccount());

        var passagePoints = new HashSet<TravelPointEntity>(3);
        var toSave = new TravelEntity();
        toSave.setProvider(account);
        toSave.setPassagePoints(passagePoints);

        passagePoints.add(new TravelPointEntity(
                Address.builder().point(addressMapper.coordsToPoint(4.0, 6.0)).city("Point A").country("DE").build(),
                toSave,
                0,
                null
        ));

        passagePoints.add(new TravelPointEntity(
                Address.builder().point(addressMapper.coordsToPoint(5.0, 6.0)).city("Point B").country("DE").build(),
                toSave,
                1,
                null
        ));

        passagePoints.add(new TravelPointEntity(
                Address.builder().point(addressMapper.coordsToPoint(5.0, 7.0)).city("Point C").country("DE").build(),
                toSave,
                2,
                null
        ));

        passagePoints.add(new TravelPointEntity(
                Address.builder().point(addressMapper.coordsToPoint(6.0, 7.0)).city("Point D").country("DE").build(),
                toSave,
                3,
                null
        ));

        passagePoints.add(new TravelPointEntity(
                Address.builder().point(addressMapper.coordsToPoint(6.0, 8.0)).city("Point E").country("DE").build(),
                toSave,
                4,
                null
        ));


        passagePoints.add(new TravelPointEntity(
                Address.builder().point(addressMapper.coordsToPoint(7.0, 8.0)).city("Point F").country("DE").build(),
                toSave,
                5,
                null
        ));

        var travel = travelRepo.save(toSave);

        assertEquals(6, repo.count());
        assertEquals(6, travel.getPassagePoints().size());

        for (TravelPointEntity travelPointEntity : travel.getPassagePoints()) {
            assertNotNull(travelPointEntity.getId());
            assertNotNull(travelPointEntity.getTravel());
            assertEquals(travel.getId(), travelPointEntity.getTravel().getId());
            assertNotNull(travelPointEntity.getAddress());
            assertNotNull(travelPointEntity.getAddress().getPoint());
        }

        for (TravelPointEntity travelPointEntity : repo.findAll()) {
            assertNotNull(travelPointEntity.getTravel());
            assertEquals(travel.getId(), travelPointEntity.getTravel().getId());
            assertNotNull(travelPointEntity.getAddress());
            assertNotNull(travelPointEntity.getAddress().getPoint());
        }


        var radius = 0.8;

        var startX = 4.2;
        var startY = 6.0;

        var endX = 6.2;
        var endY = 7.0;

        var start = getPointAsString(startX, startY);
        var end = getPointAsString(endX, endY);

        var response = repo
                .findByStartAndEnd(start, end, radius, radius, Date.valueOf("2000-01-01"), Date.valueOf("2020-01-01"), AppPageRequest.of())
                .map(this::parseData);

        assertNotNull(response);
        assertEquals(2, response.getTotalElements());

        for (TravelPointEntity[] list : response) {
            var starting = list[0];
            var ending = list[1];

            /**
             * Be very cautious about: The "x" of the point and the normal "x" are not the same
             */
            var distanceToStart = calculateDistanceBetweenPointsWithPoint2D(
                    startX,
                    startY,
                    starting.getAddress().getPoint().getY(),
                    starting.getAddress().getPoint().getX()
            );
            var distanceToEnd = calculateDistanceBetweenPointsWithPoint2D(
                    endX,
                    endY,
                    ending.getAddress().getPoint().getY(),
                    ending.getAddress().getPoint().getX()
            );

            assertTrue(distanceToStart <= radius);
            assertTrue(distanceToEnd <= radius);
        }
    }

    @Test
    void findRandomlyNearPoint() {
        var account = accountRepo.save(AccountUtils.randomAccount());

        var passagePoints = new HashSet<TravelPointEntity>(3);
        var toSave = new TravelEntity();
        toSave.setProvider(account);
        toSave.setPassagePoints(passagePoints);

        passagePoints.add(new TravelPointEntity(
                Address.builder().point(addressMapper.coordsToPoint(4.0, 6.0)).city("Point A").country("DE").build(),
                toSave,
                0,
                null
        ));

        passagePoints.add(new TravelPointEntity(
                Address.builder().point(addressMapper.coordsToPoint(5.0, 6.0)).city("Point B").country("DE").build(),
                toSave,
                1,
                null
        ));

        passagePoints.add(new TravelPointEntity(
                Address.builder().point(addressMapper.coordsToPoint(5.0, 7.0)).city("Point C").country("DE").build(),
                toSave,
                2,
                null
        ));

        passagePoints.add(new TravelPointEntity(
                Address.builder().point(addressMapper.coordsToPoint(6.0, 7.0)).city("Point D").country("DE").build(),
                toSave,
                3,
                null
        ));

        passagePoints.add(new TravelPointEntity(
                Address.builder().point(addressMapper.coordsToPoint(6.0, 8.0)).city("Point E").country("DE").build(),
                toSave,
                4,
                null
        ));


        passagePoints.add(new TravelPointEntity(
                Address.builder().point(addressMapper.coordsToPoint(7.0, 8.0)).city("Point F").country("DE").build(),
                toSave,
                5,
                null
        ));

        var travel = travelRepo.save(toSave);

        assertEquals(6, repo.count());
        assertEquals(6, travel.getPassagePoints().size());

        for (TravelPointEntity travelPointEntity : travel.getPassagePoints()) {
            assertNotNull(travelPointEntity.getId());
            assertNotNull(travelPointEntity.getTravel());
            assertEquals(travel.getId(), travelPointEntity.getTravel().getId());
            assertNotNull(travelPointEntity.getAddress());
            assertNotNull(travelPointEntity.getAddress().getPoint());
        }

        for (TravelPointEntity travelPointEntity : repo.findAll()) {
            assertNotNull(travelPointEntity.getTravel());
            assertEquals(travel.getId(), travelPointEntity.getTravel().getId());
            assertNotNull(travelPointEntity.getAddress());
            assertNotNull(travelPointEntity.getAddress().getPoint());
        }


        var radius = 0.8;

        var startX = 4.2;
        var startY = 6.0;

        var endX = 6.2;
        var endY = 7.0;

        var start = getPointAsString(startX, startY);
        var end = getPointAsString(endX, endY);

        var response = repo
                .findRandomlyNearPoint(start, radius, AppPageRequest.of());

        assertNotNull(response);
        assertEquals(2, response.getTotalElements());

        for (TravelPointEntity point : response) {
            System.out.println("Point is " + point);
        }
    }

    Double calculateDistanceBetweenPointsWithPoint2D(
            Double x1,
            Double y1,
            Double x2,
            Double y2
    ) {
        return Point2D.distance(x1, y1, x2, y2);
    }

    private String getPointAsString(Double lat, Double lng) {
        return String.format("{\"type\":\"Point\",\"coordinates\":[%s, %s]}", lng, lat);
    }

    private TravelPointEntity[] parseData(Object[] arrayOfSelection) {
        return new TravelPointEntity[]{toPoint(arrayOfSelection[0]), toPoint(arrayOfSelection[1])};
    }

    private TravelPointEntity toPoint(Object any) {
        return (TravelPointEntity) any;
    }

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
        repo.deleteAll();
    }

    @Test
    void findAllByTravelId() {
    }
}
