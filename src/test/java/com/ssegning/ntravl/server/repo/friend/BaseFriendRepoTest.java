package com.ssegning.ntravl.server.repo.friend;

import com.ssegning.ntravl.server.helper.annotation.WithDatabase;
import com.ssegning.ntravl.server.helper.common.AccountUtils;
import com.ssegning.ntravl.server.helper.common.FriendUtils;
import com.ssegning.ntravl.server.repo.account.AccountRepo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@WithDatabase
class BaseFriendRepoTest {

    @Autowired
    private AccountRepo accountRepo;

    @Autowired
    private BaseFriendRepo baseFriendRepo;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
        accountRepo.deleteAll();
    }

    @Test
    void findAllByMembers_Id_for_OnlineFriendShipEntity() {
        var account_1 = accountRepo.save(AccountUtils.randomAccount());
        var account_2 = accountRepo.save(AccountUtils.randomAccount());
        var account_3 = accountRepo.save(AccountUtils.randomAccount());
        var account_4 = accountRepo.save(AccountUtils.randomAccount());
        var account_5 = accountRepo.save(AccountUtils.randomAccount());

        //
        // 1 asked 2
        var friendShip_12 = baseFriendRepo.save(FriendUtils.randomOnlineFriend(Set.of(account_1, account_2), account_1));

        // 3 asked 1
        var friendShip_13 = baseFriendRepo.save(FriendUtils.randomOnlineFriend(Set.of(account_1, account_3), account_3));

        // 1 asked 4
        var friendShip_14 = baseFriendRepo.save(FriendUtils.randomOnlineFriend(Set.of(account_1, account_4), account_1));

        // 2 asked 5
        var friendShip_25 = baseFriendRepo.save(FriendUtils.randomOnlineFriend(Set.of(account_2, account_5), account_2));

        //
        // All friendships of 1 = 3 (2,3,4)
        var friendShips_1 = baseFriendRepo.findAllByMembers_Id(account_1.getId(), PageRequest.of(0, 10));

        // All friendships of 2 = 2 (1,5)
        var friendShips_2 = baseFriendRepo.findAllByMembers_Id(account_2.getId(), PageRequest.of(0, 10));

        assertEquals(3, friendShips_1.getTotalElements());
        assertEquals(2, friendShips_2.getTotalElements());
    }

    @Test
    void findAllByMembers_Id_for_OfflineFriendShipEntity() {
        var account_1 = accountRepo.save(AccountUtils.randomAccount());
        var account_2 = accountRepo.save(AccountUtils.randomAccount());

        //
        // 1 proposed a
        var friendShip_1_a = baseFriendRepo.save(FriendUtils.randomOfflineFriend(account_1));

        // 1 proposed b
        var friendShip_1_b = baseFriendRepo.save(FriendUtils.randomOfflineFriend(account_1));

        // 2 proposed a (the same as 1_a)
        var friendShip_2_a = baseFriendRepo.save(FriendUtils.randomOfflineFriend(account_2, friendShip_1_a.getPhoneNumber()));

        // 2 proposed d
        var friendShip_2_d = baseFriendRepo.save(FriendUtils.randomOfflineFriend(account_2));

        //
        // All friendships of 1 = 3 (1_a,1_b)
        var friendShips_1 = baseFriendRepo.findAllByMembers_Id(account_1.getId(), PageRequest.of(0, 10));

        // All friendships of 2 = 2 (2_a,2_d)
        var friendShips_2 = baseFriendRepo.findAllByMembers_Id(account_2.getId(), PageRequest.of(0, 10));

        assertEquals(2, friendShips_1.getTotalElements());
        assertEquals(2, friendShips_2.getTotalElements());
    }
}
