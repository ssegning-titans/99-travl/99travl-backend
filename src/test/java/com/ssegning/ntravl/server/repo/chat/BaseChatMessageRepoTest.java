package com.ssegning.ntravl.server.repo.chat;

import com.ssegning.ntravl.server.helper.annotation.WithDatabase;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

@WithDatabase
class BaseChatMessageRepoTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void findAllByChatId() {
    }

    @Test
    void testFindAllByChatId() {
    }

    @Test
    void findAllByChatIdAndSeen() {
    }

    @Test
    void findAllByAuthorIdNotInAndSeen() {
    }

    @Test
    void markAsRead() {
    }
}
