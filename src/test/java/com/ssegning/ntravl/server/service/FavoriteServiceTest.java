package com.ssegning.ntravl.server.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FavoriteServiceTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void createFavoriteAccount() {
    }

    @Test
    void deleteFavoriteAccount() {
    }

    @Test
    void getFavoriteAccount() {
    }

    @Test
    void findPreviousFavoriteAccount() {
    }

    @Test
    void createFavoriteTravel() {
    }

    @Test
    void deleteFavoriteTravel() {
    }

    @Test
    void getFavoriteTravel() {
    }

    @Test
    void findPreviousFavoriteTravel() {
    }

    @Test
    void getAllByAccountId() {
    }
}
