package com.ssegning.ntravl.server.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ChatMessageServiceTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void addMessage() {
    }

    @Test
    void saveContactMessage() {
    }

    @Test
    void saveMediaChatMessage() {
    }

    @Test
    void savePropositionChatMessage() {
    }

    @Test
    void saveTextChatMessage() {
    }

    @Test
    void getAllMessages() {
    }

    @Test
    void getChatLastMessageStream() {
    }

    @Test
    void streamNewMessages() {
    }

    @Test
    void getChatMessages() {
    }

    @Test
    void getAllUnreadChatMessages() {
    }

    @Test
    void getUnreadChatMessages() {
    }

    @Test
    void deleteById() {
    }

    @Test
    void markAsRead() {
    }

    @Test
    void findOneById() {
    }
}
