package com.ssegning.ntravl.server.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PasswordServiceTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void createUserPassword() {
    }

    @Test
    void deactivatePreviousUserPassword() {
    }

    @Test
    void getUserLastPassword() {
    }

    @Test
    void hasPassword() {
    }

    @Test
    void validatePassword() {
    }
}
