package com.ssegning.ntravl.server.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ChatServiceTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void createChat() {
    }

    @Test
    void markAsArchived() {
    }

    @Test
    void findById() {
    }

    @Test
    void deleteOne() {
    }

    @Test
    void getChatsByAccountId() {
    }
}
