package com.ssegning.ntravl.server.helper;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

@Getter
@RequiredArgsConstructor
public class CustomUserDetails {

    private final String name;
    private final String username;
    private final Collection<? extends GrantedAuthority> authorities;

    public CustomUserDetails(String name, String username, String[] roles) {
        this.name = name;
        this.username = username;
        authorities = Arrays.stream(roles)
                .map(GrantedAuthorityImpl::new)
                .collect(Collectors.toList());
    }

    @Getter
    @Builder
    @RequiredArgsConstructor
    public static class GrantedAuthorityImpl implements GrantedAuthority {
        private final String authority;
    }
}
