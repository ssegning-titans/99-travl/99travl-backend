package com.ssegning.ntravl.server.helper.common;

import com.github.javafaker.Faker;
import com.github.javafaker.Internet;
import com.github.javafaker.Name;
import com.github.javafaker.PhoneNumber;
import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import com.ssegning.ntravl.server.domain.embedded.AccountContact;
import com.ssegning.ntravl.server.domain.embedded.AccountName;

import java.util.UUID;

public class AccountUtils {
    private final static Faker faker = new Faker();

    private AccountUtils() {
    }

    public static AccountEntity randomAccountWithId() {
        return randomAccount(UUID.randomUUID());
    }

    public static AccountEntity randomAccount() {
        return randomAccount(null);
    }

    public static AccountEntity randomAccount(UUID accountId) {
        Name name = faker.name();
        Internet internet = faker.internet();
        PhoneNumber phoneNumber = faker.phoneNumber();

        var account = new AccountEntity();
        account.setId(accountId);
        account.setName(new AccountName(name.firstName(), name.lastName()));
        account.setContact(new AccountContact(internet.emailAddress(), true, phoneNumber.phoneNumber(), null));
        return account;
    }
}
