package com.ssegning.ntravl.server.helper.common;

import com.github.javafaker.Faker;
import com.github.javafaker.PhoneNumber;
import com.ssegning.ntravl.server.domain.accounts.AccountEntity;
import com.ssegning.ntravl.server.domain.friend.OfflineFriendShipEntity;
import com.ssegning.ntravl.server.domain.friend.OnlineFriendShipEntity;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class FriendUtils {

    private final static Faker faker = new Faker();

    private FriendUtils() {
    }

    public static OnlineFriendShipEntity randomOnlineFriend() {
        return randomOnlineFriend(null, null, null);
    }

    public static OnlineFriendShipEntity randomOnlineFriend(UUID friendsShipId) {
        return randomOnlineFriend(friendsShipId, null, null);
    }

    public static OnlineFriendShipEntity randomOnlineFriend(Set<AccountEntity> providedMembers) {
        return randomOnlineFriend(providedMembers, null);
    }

    public static OnlineFriendShipEntity randomOnlineFriend(Set<AccountEntity> providedMembers, AccountEntity author) {
        return randomOnlineFriend(null, author, providedMembers);
    }

    public static OnlineFriendShipEntity randomOnlineFriend(UUID friendsShipId, AccountEntity author, Set<AccountEntity> providedMembers) {
        var members = new HashSet<AccountEntity>();
        if (providedMembers != null && providedMembers.size() > 0) {
            members.addAll(providedMembers);
        }

        if (author != null) {
            if (!members.contains(author)) {
                throw new RuntimeException("An author is also always member");
            }
        }

        var friend = new OnlineFriendShipEntity();
        friend.setAuthor(author);
        friend.setId(friendsShipId);
        friend.setMembers(members);

        return friend;
    }

    public static OfflineFriendShipEntity randomOfflineFriend(AccountEntity member, UUID friendsShipId, String phoneNumber, String code) {
        var set = new HashSet<AccountEntity>();
        if (member != null) {
            set.add(member);
        }

        var friendShip = new OfflineFriendShipEntity();
        friendShip.setId(friendsShipId);
        friendShip.setMembers(set);
        friendShip.setPhoneNumber(phoneNumber);
        FriendCodeUtils.randomOfflineFriend(friendShip, code);

        return friendShip;
    }

    public static OfflineFriendShipEntity randomOfflineFriend(AccountEntity member, String phoneNumber, String code) {
        return randomOfflineFriend(member, null, phoneNumber, code);
    }

    public static OfflineFriendShipEntity randomOfflineFriend(AccountEntity member, String phoneNumber) {
        return randomOfflineFriend(member, null, phoneNumber, RandomStringUtils.randomNumeric(6));
    }

    public static OfflineFriendShipEntity randomOfflineFriend(AccountEntity member) {
        PhoneNumber phoneNumber = faker.phoneNumber();
        return randomOfflineFriend(member, phoneNumber.phoneNumber());
    }
}
