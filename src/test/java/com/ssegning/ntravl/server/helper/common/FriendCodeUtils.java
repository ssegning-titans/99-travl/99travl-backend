package com.ssegning.ntravl.server.helper.common;

import com.ssegning.ntravl.server.domain.friend.OfflineFriendShipEntity;
import com.ssegning.ntravl.server.domain.friend.OfflineFriendShipRequestEntity;
import com.ssegning.ntravl.server.types.FriendShipRequestStatus;

public class FriendCodeUtils {
    private FriendCodeUtils() {
    }

    public static OfflineFriendShipRequestEntity randomOfflineFriend(OfflineFriendShipEntity owner, String code) {
        var request = new OfflineFriendShipRequestEntity(code, owner, FriendShipRequestStatus.PENDING);
        if (owner != null) {
            owner.setRequest(request);
        }
        return request;
    }
}
