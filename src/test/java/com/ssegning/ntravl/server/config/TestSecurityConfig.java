package com.ssegning.ntravl.server.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtDecoders;

@TestConfiguration
public class TestSecurityConfig {

    @Value("${keycloak.issuer-uri}")
    private String keycloakIssuerUri;

    @Bean
    JwtDecoder jwtDecoder() {
        return JwtDecoders.fromIssuerLocation(keycloakIssuerUri);
    }
}
