package com.ssegning.ntravl.server.api;

import com.ssegning.ntravl.server.mapper.account.AccountMapper;
import com.ssegning.ntravl.server.service.AccountService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@WebMvcTest(controllers = AccountsApiController.class)
public class AccountApiTest {

    @MockBean
    private AccountService service;

    @MockBean
    private AccountMapper mapper;

    @Test
    public void shouldCreateAccount() {

    }

}
