FROM adoptopenjdk/openjdk11:alpine-slim

LABEL label=<stephane.segning@ssegning.com>

WORKDIR /app

COPY ./target/*.jar ./

RUN ls -la

ENTRYPOINT java -jar *.jar
